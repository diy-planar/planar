import math
import sys

import geometry_2d
from geometry_2d import P
import membrane_functions
import shading_calculator

from hacked_cq_cache import cq_cache

# cbt_radius = 250.0
cbt_radius = 1500.0
magnet_length = 12.5
corrugation_shortening_comp_ratio = 1.15

membrane_width = 55

max_v = shading_calculator.Values(db=-12)
membrane_length_mm = geometry_2d.distance_across_circle(max_v.degree, cbt_radius) * corrugation_shortening_comp_ratio
membrane_top_length_mm = 7.5
membrane_bot_length_mm = 7.5

magnet_rows = 5

magnet_width = 5
magnet_to_magnet_gap = 5

magnet_row_ctc = magnet_width + magnet_to_magnet_gap

num_traces_rows = 4

magnet_width = 5
magnet_to_magnet_gap = 5
traces_width = 3
trace_width = 3
trace_gap_width = 0.5

# resistor_width = 0.95
# resistor_length_factor = 0.87

aluminum_um = 20


print(f"membrane_length_mm = {round(membrane_length_mm, 2)}")

#
class MembraneTraces:
    def __init__(self, length_mm, offset_mm, lower_shading_db, upper_shading_db):
        self.length_mm = length_mm
        self.offset_mm = offset_mm
        self.lower_shading_db = lower_shading_db
        self.upper_shading_db = upper_shading_db

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        v = {
            "length_mm": round(self.length_mm, 2),
            "offset_mm": round(self.offset_mm, 2),
            "lower_shading_db": round(self.lower_shading_db, 2),
            "upper_shading_db": (round(self.upper_shading_db, 2)) if self.upper_shading_db else None,
        }
        return f"{v}"

print()
membrane_segments = []

##############################

# # 1 dB steps
# shading_step = 1.0
# shading_list_calculated = [
#     0,
#     shading_calculator.Values(db=-1).degree - 6.2,
#     shading_calculator.Values(db=-2).degree - 3.8,
#     shading_calculator.Values(db=-3).degree - 2.8,
#     shading_calculator.Values(db=-4).degree - 2.2,
#     shading_calculator.Values(db=-5).degree - 1.8,
#     shading_calculator.Values(db=-6).degree - 1.55,
#     shading_calculator.Values(db=-7).degree - 1.3,
#     shading_calculator.Values(db=-8).degree - 1.15,
#     shading_calculator.Values(db=-9).degree - 1.0,
#     shading_calculator.Values(db=-10).degree - 0.85,
#     shading_calculator.Values(db=-11).degree - 0.75,
#     shading_calculator.Values(db=-12).degree - 0.65,
#     shading_calculator.Values(db=-12).degree,
# ]

# # 1.5 dB steps
# shading_step = 1.5
# shading_list_calculated = [
#     0,
#     shading_calculator.Values(db=-1.5).degree - 7.5,
#     shading_calculator.Values(db=-3).degree - 4.5,
#     shading_calculator.Values(db=-4.5).degree - 3.0,
#     shading_calculator.Values(db=-6).degree - 2.2,
#     shading_calculator.Values(db=-7.5).degree - 1.8,
#     shading_calculator.Values(db=-9).degree - 1.5,
#     shading_calculator.Values(db=-10.5).degree - 1.3,
#     shading_calculator.Values(db=-12).degree - 1.1,
#     shading_calculator.Values(db=-12).degree,
# ]

# 3 dB steps
shading_step = 3
shading_list_calculated = [
    0,
    shading_calculator.Values(db=-3).degree - 10.5,
    shading_calculator.Values(db=-6).degree - 5.5,
    shading_calculator.Values(db=-9).degree - 3.5,
    shading_calculator.Values(db=-12).degree - 2.4,
    shading_calculator.Values(db=-12).degree,
]

shading_calculated = []

for i in range(0, len(shading_list_calculated) - 1):
    shading_end = (-i - 1) * shading_step if len(shading_list_calculated) > i else None
    shading_calculated.append((shading_list_calculated[i], shading_list_calculated[i + 1], -i * shading_step, shading_end))

###

print(f'shading_list_calculated')
for i in range(0, len(shading_list_calculated)):
    r = shading_list_calculated[i]
    length_mm = geometry_2d.distance_across_circle(r, cbt_radius) * corrugation_shortening_comp_ratio
    print(f"{i}: {r}, length_mm = {round(length_mm, 2)}")

####################################################

for (lower_r, upper_r, lower_shading_db, upper_shading_db) in shading_calculated:

    membrane_segments.append(MembraneTraces(
        length_mm=geometry_2d.distance_across_circle(upper_r - lower_r,
                                                     cbt_radius) * corrugation_shortening_comp_ratio,
        offset_mm=geometry_2d.distance_across_circle(lower_r, cbt_radius) * corrugation_shortening_comp_ratio,
        lower_shading_db=lower_shading_db,
        upper_shading_db=upper_shading_db,
    ))
    # print(values)

####################################################

print()
print('front')
for it in membrane_segments:
    print(it)

def print_length_left(name, membrane_segments):
    for i in range(0, len(membrane_segments)):
        length_left = 0
        for j in range(i, len(membrane_segments)):
            length_left += membrane_segments[j].length_mm

        r = membrane_functions.calc_alu_trace_resistance(width_mm=3, length_mm=length_left, thickness_um=aluminum_um) * 0.707
        print(f"{name}-{i}: {round(length_left, 2)}, r ~ {round(r, 2)}")

print()
print_length_left('front', membrane_segments)

print()

import os
if os.environ.get('SKIP_CQ') == 'true':
    sys.exit()

print("Start of CQ")
import cadquery as cq
import wg_functions
import wg_path


####################################################

def calc_slices(list):
    slices = []
    diffs = {}
    for (r1, r2, shading, end_shading) in list:
        # print(f"r1 = {r1}, r2 = {r2}, shading = {shading}")
        p1 = P(angle=r1, length=cbt_radius)
        p2 = P(angle=r2, length=cbt_radius)
        p_shading = P(angle=shading_calculator.Values(db=shading).degree, length=cbt_radius)


        # diffs[f"{shading}"] = abs()
        s = (
            cq.Workplane("YX")
            .moveTo(0, p1.y)
            .lineTo(p_shading.x, p1.y)
            .lineTo(p_shading.x, p2.y)
            .lineTo(0, p2.y)

            # .lineTo(p1.x, p1.y)
            # .lineTo(p2.x, p2.y)
            # .lineTo(0, 0)
            .close()
            .extrude(1)
        )
        slices.append(s)

    return wg_functions.union_shapes_list(slices)


c = (
    cq.Workplane("XY")
    .circle(cbt_radius)
    .extrude(-1)
    .intersect((
        cq.Workplane("XY")
        .rect(cbt_radius, cbt_radius)
        .extrude(-1)
        .translate((cbt_radius / 2, cbt_radius / 2, 0))
    ))
)

p_shaded_max = P(angle=shading_calculator.Values(db=-12).degree, length=cbt_radius)
cutoff_12_db = (
    cq.Workplane("XY")
    .rect(p_shaded_max.y, cbt_radius)
    .extrude(-1)
    .translate((p_shaded_max.y / 2, cbt_radius / 2, 0))
)

c = (
    cq.Workplane("XY")
    .circle(cbt_radius)
    .extrude(-1)
    .intersect((
        cq.Workplane("XY")
        .rect(cbt_radius, cbt_radius)
        .extrude(-1)
        .translate((cbt_radius / 2, cbt_radius / 2, 0))
    ))
)

p_shaded_max = P(angle=shading_calculator.Values(db=-12).degree, length=cbt_radius)
cutoff_12_db = (
    cq.Workplane("XY")
    .rect(p_shaded_max.y, cbt_radius)
    .extrude(-1)
    .translate((p_shaded_max.y / 2, cbt_radius / 2, 0))
)

shading_aid_y_offset = - cbt_radius * 0.3

shading_aid_cut = (
    cq.Workplane("YX")
    .rect(cbt_radius, cbt_radius * 2)
    .extrude(1)
)

calculated_shaded_circle = (
    calc_slices(shading_calculated)
    .cut(shading_aid_cut)
)

diff_add = (
    calculated_shaded_circle
    .cut(c)
    .intersect(cutoff_12_db)
    .cut(shading_aid_cut)
    .translate((0, shading_aid_y_offset, 0))
)
diff_sub = (
    c
    .cut(calculated_shaded_circle)
    .intersect(cutoff_12_db)
    .cut(shading_aid_cut)
    .translate((0, shading_aid_y_offset, 0))
)

calculated_shaded_circle = (
    calculated_shaded_circle
    .translate((0, shading_aid_y_offset, 0))
)

diff_add_vol = diff_add.val().Volume()
diff_sub_vol = diff_sub.val().Volume()
diff_error_vol = diff_add_vol - diff_sub_vol
print(f"diff_add_vol = {diff_add_vol}")
print(f"diff_sub_vol = {diff_sub_vol}")
print(f"diff_error_vol = {diff_error_vol}")

####################################################


@cq_cache()
def calc_shading_visual(shading_list, shading_step, return_lines=False, return_text=False):

    shading_visual_lines = []
    shading_visual_text = []

    for i in range(0, len(shading_list)):
        r = shading_list[i]
        length_mm = geometry_2d.distance_across_circle(r, cbt_radius) * corrugation_shortening_comp_ratio
        print(f"{i}: {r}, length_mm = {round(length_mm, 2)}")

        w = 10
        h = 100
        bp = (
            cq.Workplane("XY")
            .rect(w, h)
            .extrude(1)
            .translate((length_mm, 0, 0))
            .translate((0, membrane_width/2 + h / 2 + 5, 0))
        )
        shading_visual_lines.append(bp)

        if i > 0:
            r_prev = shading_list[i-1]
            r_middle = wg_functions.middle_value(r, r_prev)

            length_mm = geometry_2d.distance_across_circle(r_middle, cbt_radius) * corrugation_shortening_comp_ratio

            s = (i-1) * shading_step

            fontsize = 40.0
            if s == 5:
                fontsize *= 0.75
            elif s == 6:
                fontsize *= 0.6
            elif s >= 11:
                fontsize *= 0.25
            elif s >= 10:
                fontsize *= 0.3
            elif s >= 7:
                fontsize *= 0.4
            y = 0
            # if s == 11:
            #     y += fontsize

            shading_text = (
                cq.Workplane("XY")
                    .text(txt=str(f"-{s} dB"), fontsize=fontsize, distance=4, cut=True, clean=True)
                    .translate((length_mm, 0, 0))
                    .translate((0, membrane_width / 2 + h / 2 + 5 + y, 0))
            )
            shading_visual_text.append(shading_text)


    shading_visual_lines = wg_functions.union_shapes_list(shading_visual_lines)
    shading_visual_text = wg_functions.union_shapes_list(shading_visual_text)

    if return_lines:
        return shading_visual_lines
    elif return_text:
        return shading_visual_text
    else:
        return None


shading_visual_lines = calc_shading_visual(shading_list_calculated, shading_step, return_lines=True)
shading_visual_text = calc_shading_visual(shading_list_calculated, shading_step, return_text=True)

t = 0.1

membrane = (
    cq.Workplane("XY")
    .rect(membrane_length_mm, membrane_width)
    .extrude(t)
    .translate((membrane_length_mm / 2, 0, 0))
)
# top
membrane = membrane.union(
    cq.Workplane("XY")
    .rect(membrane_top_length_mm, membrane_width)
    .extrude(t)
    .translate((membrane_top_length_mm / 2 + membrane_length_mm, 0, 0))
)
# bot
membrane = membrane.union(
    cq.Workplane("XY")
    .rect(membrane_bot_length_mm, membrane_width)
    .extrude(t)
    .translate((-membrane_bot_length_mm / 2, 0, 0))
)
membrane = (
    membrane
    .edges("|Z")
    .fillet(4)
)

magnet_row = (
    cq.Workplane("XY")
    .rect(membrane_length_mm, magnet_width)
    .extrude(3)
    .translate((membrane_length_mm / 2, 0, -1.5))
)

magnets = []
for i in range(-math.floor(magnet_rows / 2), math.ceil(magnet_rows / 2)):
    magnets.append(
        magnet_row.translate((0, i * magnet_row_ctc, 0))
    )

magnets = wg_functions.union_shapes_list(magnets)


#

def mirror_traces(shape):
    return (
        shape
        .translate((0, -magnet_width, 0))
        .mirror(mirrorPlane='ZX', union=True)
        .translate((0, -2 * magnet_width, 0))
        .mirror(mirrorPlane='ZX', union=True)
    )


def calc_traces(margin=0):

    all_traces = []

    for it in membrane_segments:
        w = (
            wg_path.WgPath(width=trace_width + margin, current_p=P(0, 0))
            .walk_straight(it.length_mm)
        )

        b = (
            w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
            .close()
            .extrude(t)
            .translate((it.offset_mm, 0, 0))
            .translate((0, traces_width / 2 - trace_width / 2, 0))
            .translate((0, -magnet_row_ctc/2, 0))
        )

        all_traces.append(b)

    traces = wg_functions.union_shapes_list(all_traces)

    traces = (
        traces
        .mirror(mirrorPlane='XZ', union=True)
    )

    return traces


#

traces = calc_traces(0)

# traces_fill, a, b = calc_traces(2 * trace_gap_width)

# traces_fill = (
#     traces_fill
#     .edges("|Z")
#     .fillet(trace_gap_width * 0.)
# )

# fill = membrane.cut(traces_fill)


# traces = traces.union(fill)

# traces = traces.intersect((
#     cq.Workplane("XY")
#     .rect(membrane_length_mm, membrane_width)
#     .extrude(t)
#     .translate((membrane_length_mm / 2 + membrane_length_mm - 260, 0, 0))
# ))


show_object(shading_visual_lines, name='shading_visual_help', options={"color": (255, 40, 40), "alpha": 0.8})
show_object(shading_visual_text, name='shading_visual_text', options={"color": (0, 0, 0), "alpha": 0})

show_object(calculated_shaded_circle, name='shading_visual_calculated_shaded_circle', options={"color": (40, 200, 40), "alpha": 0.5})

show_object(diff_add, name='shading_visual_diff_add', options={"color": (30, 150, 30), "alpha": 0.5})
show_object(diff_sub, name='shading_visual_diff_sub', options={"color": (20, 100, 20), "alpha": 0.5})

show_object(magnets, name='magnets', options={"color": (40, 40, 40), "alpha": 0.8})
show_object(membrane, name='membrane', options={"color": (255, 255, 255), "alpha": 0.9})

show_object(traces, name='traces', options={"alpha": 0.0})
# show_object(traces_fill, name='traces_fill', options={"alpha": 0.75})
# show_object(fill, name='fill', options={"alpha": 0.75})
# show_object(transition_traces, name='transition_traces', options={"alpha": 0.75})
# show_object(transition_cuts, name='transition_cuts', options={"alpha": 0.75})

EXPORT_DXF = False
if EXPORT_DXF:
    workplane = traces.faces("-Z")

    from datetime import date
    import os


    today = str(date.today())

    target_dir = f"{os.getcwd()}\\full_membrane_traces_single_trace\\{today}"
    os.makedirs(target_dir, exist_ok=True)

    cq.exporters.export(
        workplane.section(),
        f"{target_dir}/full_membrane_traces.dxf"
    )