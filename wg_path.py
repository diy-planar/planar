import wg_functions
from geometry_2d import P
import math


class TraceSegment:
    def __init__(self, start_width, end_width, length):
        self.start_width = start_width
        self.end_width = end_width
        self.length = length

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()

class WgPath:
    def __init__(self, width, angle=0, current_p=P(0, 0), right_walked_points=None, left_walked_points=None, length_segments=None):
        self.width = width
        self.angle = angle
        self.current_p = current_p
        if right_walked_points is None:
            right_walked_points = [current_p + P(length=width / 2, angle=angle + 90)]
        self.right_walked_points = right_walked_points
        if left_walked_points is None:
            left_walked_points = [current_p + P(length=width / 2, angle=angle - 90)]
        self.left_walked_points = left_walked_points
        self.length_segments = length_segments if length_segments else []

    def walk_straight(self, length, end_width=None, left_offset=0.0, right_offset=0.0):
        next_p = self.current_p + P(length=length, angle=self.angle) + P(length=left_offset - right_offset, angle=self.angle + 90)

        end_width = end_width if end_width else self.width

        next_right_p = next_p + P(length=end_width / 2, angle=self.angle + 90)
        next_left_p = next_p + P(length=end_width / 2, angle=self.angle - 90)

        next_right_walked_points = self.right_walked_points + [next_right_p]
        next_left_walked_points = [next_left_p] + self.left_walked_points
        return WgPath(
            width=end_width,
            angle=self.angle,
            current_p=next_p,
            right_walked_points=next_right_walked_points,
            left_walked_points=next_left_walked_points,
            length_segments=self.length_segments + [TraceSegment(
                start_width=self.width,
                end_width=end_width,
                length=length,
            )],
        )

    def walk_arc(self, relative_angle, radius, end_width=None):

        end_width = end_width if end_width else self.width
        middle_width = wg_functions.middle_value(self.width, end_width)
        length_segments = self.length_segments + [TraceSegment(
                start_width=self.width,
                end_width=end_width,
                length=(relative_angle / 360) * radius * 2 * math.pi,
            )]

        if relative_angle == 0:
            return self.walk_straight(length=radius)
        elif relative_angle > 0:  # arc to right
            arc_center_p = self.current_p + P(length=radius, angle=(self.angle + 90))

            arc_rotating_p_none = P(length=radius, angle=self.angle + 90 + 180 + 0)
            arc_rotating_p_full = P(length=radius, angle=self.angle + 90 + 180 + relative_angle)

            if self.current_p.round(10) != (arc_center_p + arc_rotating_p_none).round(10):
                # noinspection PyStringException
                raise 'should not happen!'

            right_p1 = arc_center_p + P(length=radius - middle_width / 2, angle=self.angle + 90 + 180 + relative_angle / 2)
            right_p2 = arc_center_p + P(length=radius - end_width / 2, angle=self.angle + 90 + 180 + relative_angle)

            left_p1 = arc_center_p + P(length=radius + middle_width / 2, angle=self.angle + 90 + 180 + relative_angle / 2)
            left_p2 = arc_center_p + P(length=radius + end_width / 2, angle=self.angle + 90 + 180 + relative_angle)

            next_p = arc_center_p + arc_rotating_p_full
            next_right_walked_points = self.right_walked_points + [(right_p1, right_p2)]

            next_left_walked_points = [left_p2] + [(left_p1, self.left_walked_points[0])] + self.left_walked_points[1::]
            return WgPath(
                width=end_width,
                angle=self.angle + relative_angle,
                current_p=next_p,
                right_walked_points=next_right_walked_points,
                left_walked_points=next_left_walked_points,
                length_segments=length_segments,
            )
        else:  # arc to left
            arc_center_angle = self.angle - 90
            arc_center_p = self.current_p + P(length=radius, angle=arc_center_angle)
            arc_rotating_p_none = P(length=radius, angle=self.angle + 90)
            arc_rotating_p_full = P(length=radius, angle=self.angle + 90 + relative_angle)

            if self.current_p.round(10) != (arc_center_p + arc_rotating_p_none).round(10):
                # noinspection PyStringException
                raise 'should not happen!'

            right_p1 = arc_center_p + P(length=radius + self.width / 2, angle=self.angle + 90 + relative_angle/2)
            right_p2 = arc_center_p + P(length=radius + self.width / 2, angle=self.angle + 90 + relative_angle)

            left_p1 = arc_center_p + P(length=radius - self.width / 2, angle=self.angle + 90 + relative_angle/2)
            left_p2 = arc_center_p + P(length=radius - self.width / 2, angle=self.angle + 90 + relative_angle)

            next_p = arc_center_p + arc_rotating_p_full
            next_right_walked_points = self.right_walked_points + [(right_p1, right_p2)]

            next_left_walked_points = [left_p2] + [(left_p1, self.left_walked_points[0])] + self.left_walked_points[1::]
            return WgPath(
                width=self.width,
                angle=self.angle + relative_angle,
                current_p=next_p,
                right_walked_points=next_right_walked_points,
                left_walked_points=next_left_walked_points,
                length_segments=length_segments,
            )

    def draw_lines_on_workplane(self, workplane):
        w = workplane
        p_first = self.right_walked_points[0]
        w = w.moveTo(p_first.x, p_first.y)
        for p in self.right_walked_points[1::] + self.left_walked_points:
            if type(p) is tuple:
                p0, p1 = p
                w = w.lineTo(p0.x, p0.y)
                w = w.lineTo(p1.x, p1.y)
            else:
                w = w.lineTo(p.x, p.y)
        return w

    def draw_lines_and_arc_on_workplane(self, workplane, invert_x=False, invert_y=False, walked_points=None):
        w = workplane
        p_first = self.right_walked_points[0]
        if invert_x:
            p_first = p_first.invert_x()
        if invert_y:
            p_first = p_first.invert_y()
        w = w.moveTo(p_first.x, p_first.y)

        if walked_points is None:
            walked_points = self.right_walked_points[1::] + self.left_walked_points

        for p in walked_points:
            if type(p) is tuple:
                p0, p1 = p
                if invert_x:
                    p0, p1 = p0.invert_x(), p1.invert_x()
                if invert_y:
                    p0, p1 = p0.invert_y(), p1.invert_y()
                w = w.threePointArc(p0.values(), p1.values())
            else:
                if invert_x:
                    p = p.invert_x()
                if invert_y:
                    p = p.invert_y()
                w = w.lineTo(p.x, p.y)
        return w

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()

