class Screw:
    def __init__(self, m_gauge):
        self.m_gauge = m_gauge
        self.screwdriver_shaft_diameter = 0.8
        if m_gauge == 'm3':
            self.screw_diameter_tight = 0.325
            self.screw_diameter_medium_fit = 0.35
            self.screw_diameter_loose = 0.37
            self.screw_head_diameter = 0.6 + 0.2
            self.screw_head_thickness = 0.2
            self.heatsert_short_depth = 0.4
            self.heatsert_long_depth = 0.91
            self.heatsert_diameter = 0.4
            self.heatsert_diameter_reinforced = self.heatsert_diameter * 2
        if m_gauge == 'm4':
            self.screw_diameter_tight = 0.425
            self.screw_diameter_medium_fit = 0.45
            self.screw_diameter_loose = 0.47
            self.screw_head_diameter = 0.7 + 0.2
            self.screw_head_thickness = 0.25
            self.heatsert_short_depth = 0.5
            self.heatsert_long_depth = 1.05
            self.heatsert_diameter = 0.56
            self.heatsert_diameter_reinforced = self.heatsert_diameter * 2
        if m_gauge == 'm5':
            self.screw_diameter_tight = 0.53
            self.screw_diameter_medium_fit = 0.55
            self.screw_diameter_loose = 0.57
            self.screw_head_diameter = 0.91 + 0.3
            self.screw_head_thickness = 0.3
            self.heatsert_short_depth = 0.68
            self.heatsert_long_depth = 1.37
            self.heatsert_diameter = 0.64
            self.heatsert_diameter_reinforced = self.heatsert_diameter * 2
        if m_gauge == 'm6':
            self.screw_diameter_tight = 0.63
            self.screw_diameter_medium_fit = 0.65
            self.screw_diameter_loose = 0.67
            self.screw_head_diameter = 1.1 + 0.4
            self.screw_head_thickness = 0.4
            self.heatsert_depth = 1.37
            self.heatsert_diameter = 0.80
            self.heatsert_diameter_reinforced = self.heatsert_diameter * 2

    def __repr__(self):
        d = {key: value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(value)}
        return d.items().__repr__()
