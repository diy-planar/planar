from geometry_2d import P
import wg_path

t = 3.5
r = 6

w = (
    wg_path.WgPath(width=t, current_p=P(0, 0), angle=-45)
        .walk_straight(r + 1)
        .walk_arc(180 - 45, r)
        .walk_straight(2)
        .walk_arc(90 + 22.5, r)
)

import cadquery as cq

b = (
    w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
        .close()
        .extrude(t)
)

b = b.union((
    b
    .mirror(mirrorPlane='YZ', union=False)
    .mirror(mirrorPlane='XZ', union=False)
))

b = b.fillet(t*0.49)

show_object(b, name=f'b')

cq.exporters.export(
    b,
    'm4_support_hook.step'
)