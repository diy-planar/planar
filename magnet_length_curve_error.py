import geometry_2d
from geometry_2d import P

cbt_radius = 75.0


def calc_curve_error(magnet_length):
    r = geometry_2d.degrees_across_circle(magnet_length / 2, cbt_radius)

    p0 = P(angle=0, length=cbt_radius)

    p1 = p0.rotate(r)

    # print(p0)
    # print(p1)

    diff = p0.x - p1.x

    cm_to_mm = 10
    cm_to_um = 10000

    print()
    # print(diff)

    print(f"magnet_length = {cm_to_mm * magnet_length} mm")
    print(f"diff_mm = {round(cm_to_mm * diff, 3)} mm")
    print(f"diff_um = {round(cm_to_um * diff, 3)} um")


calc_curve_error(magnet_length=0.6)
calc_curve_error(magnet_length=1.2)
calc_curve_error(magnet_length=2.5)
calc_curve_error(magnet_length=5)
calc_curve_error(magnet_length=10)
