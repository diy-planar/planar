import math

import cadquery as cq

import geometry_2d
import wg_functions
import planar_gasket
from geometry_2d import P
from wg_classes import Screw

print()

printer_wall_thickness = 0.05
printer_wall_thickness_gasket = 0.06
printer_margin = 0.01

# num_magnets_lengthwise = 3
# num_magnets_lengthwise = 4
# num_magnets_lengthwise = 7
num_magnets_lengthwise_top = 7
num_magnets_lengthwise_bot = num_magnets_lengthwise_top * 3
num_magnets_lengthwise_bot_sup = 4
num_magnets_lengthwise = num_magnets_lengthwise_bot
# num_magnets_lengthwise = 125

num_magnets_in_length = 1
magnets_support_gap = printer_wall_thickness

fractory_laser_cut_margin = 0.0075

planar_front_rear_steel_thickness = 0.2
# planar_front_rear_steel_thickness = 0.15
# steel_plate_magnet_gap_fillet = 0.2
steel_plate_magnet_gap_fillet = 0.15
edge_screw_diameter_multi = 2.0


# if printed parts
# steel_edge_cut_margin = 0
# steel_length_margin = - 2 * printer_wall_thickness

# if laser cut parts
steel_edge_cut_margin = -2 * fractory_laser_cut_margin
steel_length_margin = -printer_wall_thickness

RENDER_STEEL_PLATES = False
RENDER_CURVED_STEEL_PLATE = True
RENDER_STEEL_PLATE_SUPPORTS = False
RENDER_STEEL_PLATE_ROLL_REFERENCE = False
RENDER_MAGNETS = False
RENDER_MAGNET_GLUE_JIG = False
RENDER_MAGNET_GLUE_JIG_2 = False
RENDER_MAGNET_GLUE_JIG_SUPPORT = False
RENDER_MAGNET_GLUE_JIG_EVEN = False
RENDER_EDGE = False
RENDER_EDGE_SHIM = False
RENDER_GASKET_SOLID_TOP = False
RENDER_GASKET_SOLID_BOT = False
RENDER_FOAM_SUSPENSION_TOP = False
RENDER_FOAM_SUSPENSION_BOT = False
RENDER_MEMBRANE = False
RENDER_MIDDLE_MEMBRANE_XMAX_COMPARISON = False
RENDER_TEST_HOOK = False
RENDER_MID_FLARE = False
RENDER_EDGE_CYLINDER_PEGS = False
RENDER_PLANAR_SHAPE = True
RENDER_PLANAR_WG = True

STEEL_GAP_OUTSIDE_OUTER_MAGNET = False
SUSPENSION_IN_EDGE_ROW = False
EDGE_CYLINDER_PEGS = True
EDGE_MEMBRANE_CHAMFER_MARGIN = True

GASKET_SOLID_NEW_STYLE = True
EDGE_TOP_BOT_SPLIT = True
EDGE_CAPS = True
RENDER_EDGE_EXTRA_WITH_FILLET = False
SPLIT_EDGE_LEFT_RIGHT = True
RENDER_STEEL_EDGE_SUPPORT = False
RENDER_MIDDLE_MOUNT_SCREW = False
RENDER_THREADED_INSERT = False
RENDER_OUTER_SCREWS_AND_NUTS = False
RENDER_EDGE_MARGINS_OVER_UNDER_GASKET = False
RENDER_HORIZONTAL_GASKET_TABS = False
RENDER_VERTICAL_GASKET_TABS = False
RENDER_EDGE_STEEL_INDEX_PEGS = False
RENDER_OUTER_HOLE_CHEAPSKATE_GRID_FIX = False

EXPORT_GASKET = False
EXPORT_EDGE = False
EXPORT_STEEL_PLATE = False
EXPORT_STEEL_PLATE_SUPPORTS = False
EXPORT_STEEL_PLATE_ROLL_REF = False
EXPORT_MAGNET_GLUE_JIG = False
EXPORT_OUTER_HOLE_CHEAPSKATE_GRID_FIX = False
EXPORT_TEST_HOOK = False
EXPORT_MID_FLARE = False
EXPORT_PLANAR_WG = False

horizontal_magnets = False
#
# magnet_middle_supports_offset = None
magnet_middle_supports = 0
magnets_per_middle_support = 1
steel_plate_magnet_gap_fillet = 0.1
# magnet_middle_supports_offset = 0
magnet_middle_supports_offset = 1 / (magnet_middle_supports + 1) / 2

steel_plate_magnet_support_outer_row_cross_brace = False
steel_plate_magnet_support_odd_row_offset = 0.25


# 1x 12x5x3
num_magnets_rows = 5
num_magnet_gap_rows = 6
num_magnets_in_length = 1
actual_magnet_length = 1.2
magnet_width = 0.5
magnet_height = 0.3
steel_plate_magnet_support_width = min(0.2, (0.2 / (magnet_middle_supports + 1))) + 2 * fractory_laser_cut_margin
# steel_plate_magnet_support_outer_row_cross_brace = True
# magnets_support_gap = magnets_support_gap / 2
magnets_support_gap = 0.00

# # # 2x 12x5x3
# num_magnets_rows = 5
# num_magnets_in_length = 2
# actual_magnet_length = 1.2
# magnet_width = 0.5
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.2

# # 1x 24x5x3
# num_magnets_rows = 5
# actual_magnet_length = 1.2 * 2
# magnet_width = 0.5
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.2

# 2x 12x4x3
# num_magnets_rows = 5
# num_magnets_in_length = 2
# actual_magnet_length = 1.2
# magnet_width = 0.4
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.2

# 2x 12x4x3
# num_magnets_rows = 7
# num_magnets_in_length = 2
# actual_magnet_length = 1.2
# magnet_width = 0.4
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.2
# radiating_area_width = 5.2
# planar_width = 8.3
# planar_extra_edge_width = 9.0

# 50x3x3x
# num_magnets_rows = 7
# actual_magnet_length = 5
# magnet_width = 0.3
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.4
# steel_plate_magnet_gap_fillet = 0.125

# 48x2.6x2.6x
# num_magnets_rows = 9
# actual_magnet_length = 4.8
# magnet_width = 0.261
# magnet_height = 0.261
# steel_plate_magnet_support_width = 0.4
# steel_plate_magnet_gap_fillet = 0.1
# edge_screw_diameter_multi = 1.5

# 40x5x5x
# num_magnets_lengthwise = 1
# num_magnets_rows = 3
# actual_magnet_length = 4
# magnet_width = 0.5
# magnet_height = 0.5
# steel_plate_magnet_support_width = 0.4
# planar_front_rear_steel_thickness = 0.2

# 40x5x5x
# num_magnets_lengthwise = 1
# num_magnets_rows = 5
# actual_magnet_length = 4
# magnet_width = 0.5
# magnet_height = 0.5
# steel_plate_magnet_support_width = 0.4
# planar_front_rear_steel_thickness = 0.2

# 50x3x3x horizontal
# num_magnets_lengthwise = 5
# num_magnets_rows = 1
# actual_magnet_length = 5
# magnet_width = 0.3
# magnet_height = 0.3
# steel_plate_magnet_support_width = 0.4
# steel_plate_magnet_gap_fillet = 0.125
# horizontal_magnets = True
# RENDER_EDGE_STEEL_INDEX_PEGS = False


magnet_middles_lengthwise_list = []
for i in range(0, math.ceil(num_magnets_lengthwise / 2)):
    if num_magnets_lengthwise % 2 == 0:
        magnet_middles_lengthwise_list.append(i + 0.5)
    else:
        magnet_middles_lengthwise_list.append(i)


print(f"steel_plate_magnet_support_width={steel_plate_magnet_support_width}")

# radiating_area_width = 4.0
radiating_area_width = (num_magnets_rows * 2 - 1 - 2) * magnet_width
# planar_width = 7.3
# planar_width = radiating_area_width + 4 * magnet_width + 3 * magnet_width
planar_width = 7.0
planar_extra_edge_width = 8.0

magnets_glue_jig_thickness = 0.6


magnet_length = actual_magnet_length * num_magnets_in_length + (1 + num_magnets_in_length) * magnets_support_gap
magnet_length_gap_margin = 0.05


num_screws_per_magnet = 1

xmax_mech_one_way = 0.2
magnet_z_offset = xmax_mech_one_way
steel_screw_elongation = None
# steel_screw_elongation = 0.1

edge_shim_thickness = 0.1


magnet_glue_jig_h_above_magnets = 0.15

cbt_radius = 150.0
# cbt_radius = 100000.0

middle_cut_margin = 0.002


screw_hole_middles_list = []
for i in magnet_middles_lengthwise_list:
    if i in magnet_middles_lengthwise_list[:-2] and num_screws_per_magnet == 0.5 and i % 2 == (0 if num_magnets_lengthwise == 7 else 1):
        continue
    screw_hole_middles_list.append(i)


xmax_str = f"{round(2*magnet_z_offset*10, 2)}mm-xmax"

cbt_curve_str = 'straight' if cbt_radius > 1000 else f"{cbt_radius}r"

edge_shim_thickness_str = f"{round(edge_shim_thickness * 10, 2)}mm"
magnet_size_str = f"{num_magnets_lengthwise}_{int(magnet_length * 10)}x{int(magnet_width * 10)}x{int(magnet_height * 10)}"
steel_plate_size_str = '_'.join(filter(None, [
    f"w{round(planar_width, 2)}",
    f"t{round(planar_front_rear_steel_thickness, 3)}",
    f"{num_magnets_lengthwise}x{num_magnets_rows}",
    f"{magnet_size_str}",
    f"{round(2*magnet_z_offset*10, 2)}mm-xmax",
    f"{'elongated-holes' if steel_screw_elongation else ''}",
    f"{'open-outer-row' if STEEL_GAP_OUTSIDE_OUTER_MAGNET else ''}",
    f"{'suspension-in-outer-row' if SUSPENSION_IN_EDGE_ROW else ''}",
]))

if horizontal_magnets:
    radiating_area_width = magnet_length
    radiating_area_width_including_edge_magnets = magnet_length
    gap_width_between_magnets = magnet_width * 1.25
    magnet_ctc_length_degrees = geometry_2d.degrees_across_circle(magnet_width + gap_width_between_magnets, cbt_radius)
    num_screws_per_magnet = 1
else:
    radiating_area_width_including_edge_magnets = radiating_area_width + 2 * magnet_width
    gap_width_between_magnets = (radiating_area_width - (num_magnets_rows - 2) * magnet_width) / (num_magnets_rows - 1)
    magnet_ctc_length_degrees = geometry_2d.degrees_across_circle(
        magnet_length + magnet_length_gap_margin,
        cbt_radius)
    magnet_ctc_length_for_screw_holes_degrees = geometry_2d.degrees_across_circle(
        magnet_length + magnet_length_gap_margin,
        cbt_radius)

planar_height = num_magnets_lengthwise * magnet_length
planar_height_degrees = num_magnets_lengthwise * magnet_ctc_length_degrees
planar_extra_edge_fillet = (planar_extra_edge_width - planar_width) * 0.45

MICRON_TO_CM = 0.0001

membrane_xmax_one_way = 0.04

membrane_corrugation_length = 0.5 / 4
membrane_corrugation_fillet_radius = 0.03
membrane_corrugation_depth_peak_to_peak = 2 * membrane_xmax_one_way * 1.2
membrane_thickness = MICRON_TO_CM * 42
middle_membrane_xmax_comparison_height = membrane_xmax_one_way * 2

membrane_corrugation_peaks_per_squiggly = 3
gasket_squiggly_at_peak = False

membrane_gasket_edge_margin = 0.05
membrane_gasket_overlap_total = 0.6

planar_edge_inner_cavity_edge_height = 0.10

gasket_margin = 0.005

gasket_magnet_membrane_thickness_intersect = magnet_z_offset * 2
# gasket_magnet_membrane_thickness_intersect = membrane_corrugation_depth_peak_to_peak + 0.06

gasket_edge_margin = printer_wall_thickness
# gasket_tabs_radius = magnet_height - 2 * gasket_margin
gasket_tabs_radius = planar_edge_inner_cavity_edge_height * 1.25

print(f"num_screws_per_magnet={num_screws_per_magnet}")
print(f"gasket_tabs_radius={gasket_tabs_radius}")

gasket_solid_to_top_steel = True

edge_screw_diameter = Screw('m3').screw_diameter_tight
edge_screw_diameter_medium_fit = Screw('m3').screw_diameter_medium_fit

# membrane_gasket_magnet_overlap = magnet_width / 2
membrane_gasket_magnet_overlap = 0
membrane_gasket_overlap = membrane_gasket_overlap_total - membrane_gasket_magnet_overlap

edge_screw_margin = 2 * printer_wall_thickness

# gasket_width = planar_width - 2 * (edge_screw_diameter + 1.5 * edge_screw_margin)
gasket_width = radiating_area_width_including_edge_magnets + 2.5 * magnet_width

planar_thickness_inner = (magnet_z_offset + magnet_height) * 2
planar_thickness = planar_thickness_inner + 2 * planar_front_rear_steel_thickness

# planar_edge_inner_cavity_height = (magnet_z_offset + magnet_height / 2) * 2
planar_edge_inner_cavity_height = planar_thickness_inner

# cavity_width = membrane_width + 2 * membrane_gasket_edge_margin + 2 * gasket_edge_margin
cavity_width = gasket_width
gasket_width = gasket_width - 2 * gasket_edge_margin
gasket_actual_inner_width = radiating_area_width_including_edge_magnets
gasket_actual_width = cavity_width - 2 * gasket_margin

# membrane_width = radiating_area_width + 2 * magnet_width + 2 * membrane_gasket_overlap
# membrane_width = gasket_width - 2 * membrane_gasket_edge_margin - 2 * gasket_edge_margin
membrane_width = radiating_area_width_including_edge_magnets + 2 * magnet_width
# membrane_width = wg_functions.middle_value(cavity_width, radiating_area_width_including_edge_magnets)

membrane_gasket_overlap_mm = membrane_width - radiating_area_width_including_edge_magnets

edge_inner_width = radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness

if SUSPENSION_IN_EDGE_ROW:
    gasket_actual_inner_width = radiating_area_width - 2 * gap_width_between_magnets
    gasket_actual_width = radiating_area_width
    membrane_width = radiating_area_width + 2 * magnet_width / 3
    STEEL_GAP_OUTSIDE_OUTER_MAGNET = False
    planar_width -= 2.5 * magnet_width
    cavity_width = radiating_area_width_including_edge_magnets
    edge_inner_width = radiating_area_width_including_edge_magnets + 2 * printer_margin


print(f"planar_width={round(planar_width, 2)}")
print(f"radiating_area_width={round(radiating_area_width, 2)}")
print(f"cavity_width={round(cavity_width, 2)}")
print(f"gasket_width={round(gasket_width, 2)}")
print(f"gasket_actual_width={round(gasket_actual_width, 2)}")

print(f"membrane_width={round(membrane_width, 2)}")
print(f"membrane_gasket_overlap_mm={round(membrane_gasket_overlap_mm / 2, 2)}")

# edge_screw_width = cavity_width + edge_screw_diameter + 3 * printer_wall_thickness
# edge_screw_width = planar_width - 1.5 * (edge_screw_diameter + 2 * edge_screw_margin)
edge_screw_width = cavity_width + edge_screw_diameter + 4 * printer_wall_thickness

print(f"edge_screw_width={round(edge_screw_width, 2)}")

edge_index_peg_diameter = planar_width - cavity_width - edge_screw_diameter * edge_screw_diameter_multi - printer_wall_thickness * 2
edge_index_peg_diameter_margin = 0.02

print(f"planar_edge_inner_cavity_height={planar_edge_inner_cavity_height}")
print(f"edge_index_peg_diameter={edge_index_peg_diameter}")

screw_offset_from_edge = (planar_width - edge_screw_width) * (2 / 3)
index_peg_step = (magnet_length - 2 * screw_offset_from_edge) / max((num_screws_per_magnet - 1), 1)
# screw_offset_from_edge = (planar_width - edge_screw_width) * (1/2)


print(f"planar_thickness={planar_thickness}")

num_magnet_gaps = num_magnets_rows - 1

opening_area = num_magnet_gaps * gap_width_between_magnets * (magnet_length - 3 * steel_plate_magnet_support_width)
cavity_volume = opening_area * (magnet_height + planar_front_rear_steel_thickness) + cavity_width * magnet_z_offset
length_of_opening = magnet_height + planar_front_rear_steel_thickness

print(f"cavity_volume={round(cavity_volume, 2)}cm3")
print(f"opening_area={round(opening_area, 2)}cm2")
print(f"length_of_opening={round(length_of_opening, 2)}cm")


def calc_magnets_row(outer=True, length_margin=0.0, width_margin=0.0, height_margin=0.0, magnet=None, magnet_rows=None):
    if magnet is None:
        magnet = (
            cq.Workplane("XY")
                .rect((actual_magnet_length + length_margin), (magnet_width + width_margin))
                .extrude((magnet_height + height_margin) if outer else -(magnet_height - height_margin))
                .translate((0, 0, -height_margin/2 if outer else height_margin/2))
        )

    if num_magnets_in_length == 2:
        magnet = (
            magnet
                .translate(((actual_magnet_length + magnets_support_gap) / 2, 0, 0))
                .mirror(mirrorPlane='YZ', union=True)
        )

    if horizontal_magnets:
        magnet_row = (
            magnet
                .rotate((0, 0, 0), (0, 0, 1), 90)
                .translate((0, 0, magnet_z_offset if outer else -magnet_z_offset))
        )
    else:
        magnet_row_list = []

        if magnet_rows is None:
            magnet_rows = range(0, math.ceil(num_magnets_rows / 2))
        for i in magnet_rows:
            magnet_row_list.append((
                magnet
                    .translate((0, i * (gap_width_between_magnets + magnet_width), 0))
                    .translate((0, 0, magnet_z_offset if outer else -magnet_z_offset))
            ))

        magnet_row = wg_functions.union_shapes_list(magnet_row_list)

    magnet_row = magnet_row.mirror(mirrorPlane='XZ', union=True)
    return magnet_row


def calc_magnets(outer=True):
    i_list = None
    if outer:
        i_list = magnet_middles_lengthwise_list[:math.ceil(num_magnets_lengthwise_top / 2)]
    return mirror_over_each_magnet(calc_magnets_row(outer=outer), i_list=i_list)


def calc_membrane_corrugation_gear(radius, width):
    outer_r = radius + (membrane_corrugation_depth_peak_to_peak / 2)
    inner_r = radius - (membrane_corrugation_depth_peak_to_peak / 2)

    pc = P(-cbt_radius, 0)

    degrees_per_corrugation = geometry_2d.degrees_across_circle(membrane_corrugation_length, radius)
    num_corugations_half = (planar_height_degrees / 2) / degrees_per_corrugation
    # print(f"num_corugations_half={num_corugations_half}")

    p0 = pc + P(outer_r, 0).rotate(0)

    b = (
        cq.Workplane("XY")
            # .lineTo(pc.x, pc.y)
            .lineTo(p0.x, p0.y)
    )

    for i in range(1, math.ceil(num_corugations_half) + 1):
        odd = (i % 2) == 1
        corr_radius = inner_r if odd else outer_r
        corr_r = i * degrees_per_corrugation
        pn = pc + P(corr_radius, 0).rotate(corr_r)
        b = (
            b
                .lineTo(pn.x, pn.y)
        )

    b = (
        b
            .lineTo(pc.x, pc.y)
            .close()
            .extrude(width)
            .translate((0, 0, -width / 2))
            .mirror(mirrorPlane='XZ', union=True)
            .edges("|Z")
            .fillet(membrane_corrugation_fillet_radius)
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
    )

    return b


def calc_edge_part_2(width, degrees=planar_height_degrees, degrees_offset=0.0, top_r=cbt_radius + planar_thickness / 2,
                     bot_r=cbt_radius - planar_thickness / 2):
    pc_t = P(-cbt_radius, 0)

    p0_t = pc_t + P(top_r, 0).rotate(-degrees / 2 + degrees_offset)
    p1_t = pc_t + P(top_r, 0).rotate(degrees_offset)
    p2_t = pc_t + P(top_r, 0).rotate(degrees / 2 + degrees_offset)

    pc_b = P(-cbt_radius, 0)

    p0_b = pc_b + P(bot_r, 0).rotate(-degrees / 2 + degrees_offset)
    p1_b = pc_b + P(bot_r, 0).rotate(degrees_offset)
    p2_b = pc_b + P(bot_r, 0).rotate(degrees / 2 + degrees_offset)

    b = (
        cq.Workplane("XY")
            .moveTo(p0_t.x, p0_t.y)
            .threePointArc((p1_t.x, p1_t.y), (p2_t.x, p2_t.y))

            .lineTo(p2_b.x, p2_b.y)
            .threePointArc((p1_b.x, p1_b.y), (p0_b.x, p0_b.y))
            .close()
    )

    b = (
        b
            .extrude(width)
            .translate((0, 0, -width / 2))
            # .mirror(mirrorPlane='XZ', union=True)
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
    )

    return b


def calc_edge_part(radius, width, degrees=planar_height_degrees, degrees_offset=0.0):
    return calc_edge_part_2(width=width, degrees=degrees, degrees_offset=degrees_offset, top_r=radius, bot_r=radius - planar_thickness * 2)


def calc_edge_part_square(radius, width, degrees=magnet_ctc_length_degrees, mirror_over_magnets=True):
    pc = P(-cbt_radius, 0)

    p0 = pc + P(x=radius, angle=-degrees / 2)
    p2 = pc + P(x=radius, angle=degrees / 2)

    b = (
        cq.Workplane("XY")
            .moveTo(p0.x, p0.y)
            .lineTo(p2.x, p2.y)
            .lineTo(pc.x, pc.y)
            .close()
            .extrude(width)
            .translate((0, 0, -width / 2))
            # .mirror(mirrorPlane='XZ', union=True)
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
    )

    if mirror_over_magnets:
        b = mirror_over_each_magnet(b)

    return b


def calc_gasket_tab(radius, width, z_offset, magnet_ctc_length_degrees_multi=1.5):
    b = (
        cq.Workplane("XY")
            .circle(radius / 2)
            .extrude(width)
            .translate((0, 0, -width / 2))
            .rotate((0, 0, 0), (0, 1, 0), -90)
            .rotate((0, 0, 0), (0, 0, 1), -90)
            .translate((0, 0, z_offset))
            .translate((0, 0, cbt_radius))
            .rotate((0, 0, 0), (0, 1, 0),
                    magnet_ctc_length_degrees_multi * magnet_ctc_length_degrees)
            .translate((0, 0, -cbt_radius))
    )

    return b


def calc_vertical_gasket_tabs(radius):
    return None


def calc_gasket_tabs(radius, width, top=None):
    gasket_tabs = []

    def append_gasket(multi, z=0.0):
        for i in [1] if top else [-1]:
            gasket_tabs.append(
                calc_gasket_tab(radius=radius, width=width, z_offset=i * (planar_edge_inner_cavity_height / 2 - z),
                                magnet_ctc_length_degrees_multi=multi)
            )

    for i in magnet_middles_lengthwise_list:
        append_gasket((0.5 if top else 0.0) + i, z=planar_edge_inner_cavity_edge_height)

    return wg_functions.union_shapes_list(gasket_tabs).mirror(mirrorPlane='YZ', union=True)


def gasket_top_cavity_solid(width=cavity_width):
    b = (
        calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2, width=width)
            .cut(calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2,
                                       width=radiating_area_width_including_edge_magnets))
            .cut(calc_edge_part_square(radius=cbt_radius, width=width))
    )

    return b


def calc_membrane_only(width, thickness):
    membrane, ignored, ignored = calc_membrane(width, thickness)
    return membrane


def calc_membrane(width, thickness):
    membrane_corrugation_outer = calc_membrane_corrugation_gear(radius=cbt_radius + thickness / 2, width=width)
    membrane_corrugation_inner = calc_membrane_corrugation_gear(radius=cbt_radius - thickness / 2, width=width)

    inner_removed = calc_edge_part_square(cbt_radius - planar_thickness / 2, width=width)
    membrane_corrugation_outer = membrane_corrugation_outer.cut(inner_removed)
    membrane_corrugation_inner = membrane_corrugation_inner.cut(inner_removed)

    membrane = membrane_corrugation_outer.cut(membrane_corrugation_inner)

    return membrane, membrane_corrugation_outer, membrane_corrugation_inner


def mirror_over_each_magnet(shape, ignore_odd=False, ignore_even=False, i_offset=0.0, i_list=None):
    shapes = []
    for i in i_list if i_list else magnet_middles_lengthwise_list:
        if ignore_odd and i % 2 == 1:
            continue
        elif ignore_even and i % 2 == 0:
            continue
        shapes.append(
            shape
                .translate((0, 0, cbt_radius))
                .rotate((0, 0, 0), (0, 1, 0),
                        (i + i_offset) * magnet_ctc_length_degrees)
                .translate((0, 0, -cbt_radius))
        )
    shapes = wg_functions.union_shapes_list(shapes)
    shapes = shapes.mirror(mirrorPlane='YZ', union=True)

    return shapes


def calc_edge_screw_holes(screw_diameter=edge_screw_diameter_medium_fit, extra_inner_solid_length=0.0, screw_h = 10):

    screw = (
        cq.Workplane("XY")
            .circle((screw_diameter + steel_edge_cut_margin) / 2)
            .extrude(screw_h)
    )

    if extra_inner_solid_length:
        screw = screw.union((
            cq.Workplane("XY")
                .rect(screw_diameter + steel_edge_cut_margin, extra_inner_solid_length)
                .extrude(screw_h)
                .translate((0, -extra_inner_solid_length / 2, 0))
        ))

    screw = (
        screw
            .translate((0, 0, -screw_h / 2))
            .translate((0, edge_screw_width / 2, 0))
            .mirror(mirrorPlane='XZ', union=True)
    )

    screw_holes = []

    for i in screw_hole_middles_list:
        for sign in [-1, 1]:
            if i == 0 and sign == -1:
                continue
            screw_holes.append((
                screw
                    .translate((0, 0, cbt_radius))
                    .rotate((0, 0, 0), (0, 1, 0),
                            i * magnet_ctc_length_for_screw_holes_degrees)
                    .translate((0, 0, -cbt_radius))
            ))
    screw_holes = wg_functions.union_shapes_list(screw_holes)

    screw_holes = screw_holes.mirror(mirrorPlane='YZ', union=True)

    return screw_holes


def calc_edge_index_pegs(screw_diameter=edge_index_peg_diameter, width=planar_width / 2, squared=True, chamfer=False):
    peg = (
        cq.Workplane("XY")
            .circle(screw_diameter / 2)
            .extrude(planar_thickness)
            .translate((0, width, 0))
    )

    if squared:
        peg = (
            peg
                .intersect((
                cq.Workplane("XY")
                    .rect(screw_diameter, width * 2)
                    .extrude(planar_thickness)
            ))
        )

    peg = (
        peg
            .translate((0, 0, -planar_thickness / 2))
            .translate((index_peg_step / 2, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
            .mirror(mirrorPlane='XZ', union=True)
    )

    # pegs = calc_edge_screw_holes_for_plate(screw_diameter=screw_diameter, screws_per_magnet=1, screw_height=planar_thickness)
    if chamfer:
        peg = peg.chamfer(planar_front_rear_steel_thickness / 4)

    return mirror_over_each_magnet(peg)


def calc_edge_screw_holes_for_plate(screw_diameter=edge_screw_diameter, screws_per_magnet=num_screws_per_magnet,
                                    screw_height=None):
    if screw_height is None:
        screw_height = planar_thickness + 0.5

    screw = (
        cq.Workplane("XY")
            .circle(screw_diameter / 2)
            .extrude(screw_height)
            .translate((0, 0, -screw_height / 2))
    )

    if screws_per_magnet == 1:
        return (
            screw
                .translate((0, edge_screw_width / 2, 0))
                .mirror(mirrorPlane='XZ', union=True)
        )

    screw_step = (magnet_length - 2 * screw_offset_from_edge) / (screws_per_magnet - 1)

    screw_holes = []

    for i in range(0, max(1, screws_per_magnet)):
        screw_holes.append(
            screw
                .translate((0, edge_screw_width / 2, 0))
                .translate((-magnet_length / 2, 0, 0))
                .translate((screw_offset_from_edge, 0, 0))
                .translate((i * screw_step, 0, 0))
        )

    screw_holes = wg_functions.union_shapes_list(screw_holes)

    # screw_holes = screw_holes.mirror(mirrorPlane='YZ', union=True)
    screw_holes = screw_holes.mirror(mirrorPlane='XZ', union=True)

    return screw_holes


def calc_magnet_supports_for_plate(top=True):
    b0 = (
        cq.Workplane("XY")
            .rect(steel_plate_magnet_support_width, radiating_area_width_including_edge_magnets)
            .extrude(magnet_height)
            .edges(">Z and |Y")
            .chamfer(steel_plate_magnet_support_width * 0.5 - magnets_support_gap / 2)
    )

    b1 = (
        cq.Workplane("XY")
            .rect(steel_plate_magnet_support_width, radiating_area_width_including_edge_magnets)
            .extrude(magnet_height)
            .edges(">Z and |Y and <X")
            .chamfer(steel_plate_magnet_support_width - magnets_support_gap)
            .translate((((magnet_length - steel_plate_magnet_support_width) / 2), 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
    )

    b = b0.union(b1)

    z = magnet_z_offset + magnet_height
    b = b.translate((0, 0, -z))

    b = b.cut(calc_magnets_row(outer=False))

    if not top:
        b = b.rotate((0, 0, 0), (0, 1, 0), 180)

    return b


def calc_steel_plate_curved_magnet_gap_rect_single_row(
        screw_ctc_l,
        relative_magnet_length,
        magnet_middle_supports=magnet_middle_supports,
        magnet_middle_supports_offset=magnet_middle_supports_offset,
        cross_brace=False,
):
    screw_ctc_l *= magnets_per_middle_support
    relative_magnet_length *= magnets_per_middle_support

    t = 10
    w = max(0.075 + 2 * fractory_laser_cut_margin, steel_plate_magnet_support_width / (1 + magnet_middle_supports))

    if horizontal_magnets:
        magnet_gap = (
            cq.Workplane("XY")
                .rect(gap_width_between_magnets + magnet_width * 1.1, actual_magnet_length)
                .extrude(t)
                .cut((
                cq.Workplane("XY")
                    .rect(magnet_width, actual_magnet_length)
                    .extrude(t)
            ))
                .translate((0, 0, -t / 2))
        )
    else:
        gap = (
            cq.Workplane("XY")
                .rect(relative_magnet_length - 1 * w, gap_width_between_magnets)
                .extrude(t)
        )

        y_step = relative_magnet_length / (magnet_middle_supports + 1)

        for i in range(0, magnet_middle_supports):
            gap = (
                gap
                    .cut((
                    cq.Workplane("XY")
                        .rect(w, gap_width_between_magnets)
                        .extrude(t)
                        .translate(((1 + i) * y_step - relative_magnet_length / 2, 0, 0))
                ))
            )

        gap = (
            gap
                # .edges("|Z")
                # .fillet(steel_plate_magnet_gap_fillet)
                .translate((0, 0, -t / 2))
        )

        magnet_gap = gap

    magnet_holes = []

    for i in magnet_middles_lengthwise_list + ([magnet_middles_lengthwise_list[-1]+1] if magnet_middle_supports_offset else []):
        for sign in [-1, 1]:
            if i == 0 and sign == -1:
                continue
            magnet_holes.append((
                magnet_gap
                    .translate((sign * i * screw_ctc_l, 0, 0))
            ))
    magnet_holes = wg_functions.union_shapes_list(magnet_holes)

    if magnet_middle_supports_offset:
        magnet_holes = magnet_holes.translate((-magnet_middle_supports_offset * relative_magnet_length, 0, 0))

    if cross_brace:
        magnet_holes = magnet_holes.cut((
            cq.Workplane("XY")
                .rect(relative_magnet_length * (num_magnets_lengthwise - 2 * magnet_middle_supports_offset), w)
                .extrude(t)
                .translate((0, 0, -t/2))
        ))

    return magnet_holes


def calc_steel_plate_curved_magnet_gap_rect(screw_ctc_l, relative_magnet_length):
    magnet_holes_inner = calc_steel_plate_curved_magnet_gap_rect_single_row(screw_ctc_l=screw_ctc_l, relative_magnet_length=relative_magnet_length)

    gaps = []
    i_list = range(-math.floor(num_magnet_gap_rows / 2), math.floor(num_magnet_gap_rows / 2))
    for i in i_list:
        if not STEEL_GAP_OUTSIDE_OUTER_MAGNET and i in [i_list[0], i_list[-1]]:
            continue
        if steel_plate_magnet_support_odd_row_offset:
            row_offset = magnet_length * steel_plate_magnet_support_odd_row_offset * (1 if i % 2 == 0 else -1)
        else:
            row_offset = 0
        gaps.append(
            (
                calc_steel_plate_curved_magnet_gap_rect_single_row(
                    screw_ctc_l=screw_ctc_l,
                    relative_magnet_length=relative_magnet_length,
                    magnet_middle_supports=3,
                    magnet_middle_supports_offset=1/8,
                    cross_brace=True
                )
                if i == i_list[-1] and steel_plate_magnet_support_outer_row_cross_brace
                else
                magnet_holes_inner
            )
                .translate((0, (0.5 + i) * magnet_width + (0.5 + i) * gap_width_between_magnets, 0))
                .translate((row_offset, 0, 0))
        )

    magnet_holes = wg_functions.union_shapes_list(gaps)

    return magnet_holes


def calc_steel_plate_curved(
        top=True,
        screw_diameter=edge_screw_diameter,
        z=None,
        z_offset=None,
        magnet_lengths=num_magnets_lengthwise,
        normal_magnet_holes=True,
        bot_supports_mid_holes_cut=False,
        x_offset_magnet_lengths=0.0,
        sup_chamfer_fillet=False,
        sup_m4_screw_hole_offset=None,
):
    t = planar_front_rear_steel_thickness

    if z is None:
        z = magnet_z_offset + magnet_height + t / 2
        if not top:
            z = -z

    if z_offset:
        z += z_offset

    r = cbt_radius + z

    plate_l = geometry_2d.distance_across_circle(magnet_ctc_length_degrees * magnet_lengths, r) + steel_length_margin
    screw_ctc_l = geometry_2d.distance_across_circle(magnet_ctc_length_for_screw_holes_degrees, r)
    relative_magnet_length = geometry_2d.distance_across_circle(magnet_ctc_length_degrees, r)

    x_offset_normalized_at_z = geometry_2d.distance_across_circle(magnet_ctc_length_degrees * x_offset_magnet_lengths, r)

    c = 0.6

    b = (
        cq.Workplane("XY")
            .rect(plate_l, planar_width - (2 * edge_index_peg_diameter_margin if RENDER_EDGE_STEEL_INDEX_PEGS else 0))
            .pushPoints([(-(plate_l / 2 - screw_ctc_l - steel_length_margin/2), (-1 if top else 1) * (edge_screw_width / 2))])
            .circle(0.15 / 2)
            .extrude(t)
    )

    if sup_m4_screw_hole_offset is not None:
        b = b.union((
            cq.Workplane("XY")
                .rect(1.2, planar_width + 2)
                .pushPoints([(0, (1 if top else -1) * (planar_width / 2 + 0.5))])
                .circle(Screw('m4').screw_diameter_tight / 2)
                .extrude(t)
                .edges("|Z and <Y")
                .fillet(c * 0.99)
                .translate((sup_m4_screw_hole_offset * screw_ctc_l, 0, 0))
        ))

    b = (
        b
            .translate((0, 0, -t / 2))
            .translate((x_offset_normalized_at_z, 0, 0))
    )

    #
    screw_h = 10
    if steel_screw_elongation:
        screw = (
            (
                cq.Workplane("XY")
                    .circle((screw_diameter + steel_edge_cut_margin) / 2)
                    .extrude(screw_h)
                    .translate((steel_screw_elongation / 2, 0, 0))
            )
            .union((
                cq.Workplane("XY")
                    .rect(steel_screw_elongation, screw_diameter + steel_edge_cut_margin)
                    .extrude(screw_h)
            ))
            .union((
                cq.Workplane("XY")
                    .circle((screw_diameter + steel_edge_cut_margin) / 2)
                    .extrude(screw_h)
                    .translate((-steel_screw_elongation / 2, 0, 0))
            ))
        )
    else:
        screw = (
            cq.Workplane("XY")
                .circle((screw_diameter + steel_edge_cut_margin) / 2)
                .extrude(screw_h)
        )

    screw = (
        screw
            .translate((0, 0, -screw_h / 2))
            .translate((0, edge_screw_width / 2, 0))
            .mirror(mirrorPlane='XZ', union=True)
    )

    screw_holes = []

    for i in screw_hole_middles_list:
        for sign in [-1, 1]:
            if i == 0 and sign == -1:
                continue
            screw_holes.append((
                screw
                    .translate((sign * i * screw_ctc_l, 0, 0))
            ))
    screw_holes = wg_functions.union_shapes_list(screw_holes)

    if normal_magnet_holes:
        b = b.cut(calc_steel_plate_curved_magnet_gap_rect(screw_ctc_l=screw_ctc_l, relative_magnet_length=relative_magnet_length))

    if bot_supports_mid_holes_cut:
        b = b.cut((
            cq.Workplane("XY")
                .rect(plate_l * 2, radiating_area_width_including_edge_magnets)
                .extrude(100)
                .translate((0, 0, -50))
        ))
        b = b.cut((
            cq.Workplane("XY")
                .rect(plate_l * 2, 100)
                .extrude(100)
                .translate((0, 50, -50))
        ))

    if sup_chamfer_fillet:
        b = (
            b
                .edges("|Z and >Y")
                .chamfer(c)
                .edges("|Z")
                .fillet(c/2)
        )
    else:
        b = (
            b
                .edges("|Z")
                .fillet(steel_plate_magnet_gap_fillet * 0.5)
        )

    b = b.cut(screw_holes)

    b = (
        b
            .translate((0, 0, z))
    )

    return b


def calc_outer_hole_cheapskate_grid_fix():
    t = 0.06

    l = 2 * magnet_length - steel_plate_magnet_support_width * 2.6
    w = magnet_width - 2 * printer_margin

    num_supports = 6

    support_ctc = (l-printer_wall_thickness) / num_supports

    edge_overap = (
        cq.Workplane("XY")
            .rect(l, w * 2)
            .extrude(t)
            .cut((
                cq.Workplane("XY")
                    .circle(w * 0.4)
                    .extrude(t)
                    .translate((0, w, 0))
                    .mirror(mirrorPlane='XZ', union=True)
                    .translate((l * 0.5, 0, 0))
                    .mirror(mirrorPlane='YZ', union=True)
            ))
            .cut((
                cq.Workplane("XY")
                    .rect(w * 0.8, w * 0.8)
                    .extrude(t)
                    .translate((0, w * 0.4, 0))
                    .rotate((0, 0, 0), (0, 0, 1), 45)
                    .translate((0, w, 0))
                    .translate((l * 0.5, 0, 0))
                    .mirror(mirrorPlane='YZ', union=True)
                    .mirror(mirrorPlane='XZ', union=True)
            ))
            .cut((
                cq.Workplane("XY")
                    .rect(l, w)
                    .extrude(t)
            ))
            .translate((0, 0, xmax_mech_one_way + magnet_height + planar_front_rear_steel_thickness))
            .translate((0, 3 * magnet_width, 0))
            .translate((magnet_length * 0.8, 0, 0))
    )

    supp = (
        cq.Workplane("XY")
            .rect(printer_wall_thickness, w)
            .extrude(planar_front_rear_steel_thickness + t)
            .translate((0, 0, xmax_mech_one_way + magnet_height))
            .translate((0, 3 * magnet_width, 0))
            .translate((magnet_length * 0.8, 0, 0))
            .translate((l / 2 - printer_wall_thickness/2, 0, 0))
    )

    b = (
        cq.Workplane("XY")
            .rect(l + 1.3 * support_ctc, printer_wall_thickness)
            .extrude(planar_front_rear_steel_thickness + t)
            # .faces("|Z")
            # .shell(-printer_wall_thickness)
            .translate((0, 0, xmax_mech_one_way + magnet_height))
            .translate((0, 3 * magnet_width, 0))
            .translate((magnet_length * 0.8, 0, 0))
    )

    b = (
        b
            .union(edge_overap)
    )

    # b = edge_overap

    for i in range(0, num_supports + 1):
        b = b.union((
            supp
            .translate((-i * support_ctc, 0, 0))
        ))

    return b


def calc_test_hook():

    r = magnet_middles_lengthwise_list[-1] * magnet_ctc_length_for_screw_holes_degrees
    t = 1.0

    import wg_path

    w = (
        wg_path.WgPath(width=0.5)
            .walk_straight(3)
            .walk_arc(r, 3)
            .walk_arc(45, 1)
            .walk_straight(0.37 if (cbt_radius > 1000) else 0.9)
            .walk_arc(90, 0.5)
            .walk_straight(0.6)
            .walk_arc(45, 0.5)
            .walk_straight(0.6)
    )

    b = (
        w.draw_lines_and_arc_on_workplane(cq.Workplane("XZ"))
            # w.draw_lines_on_workplane(cq.Workplane("XY"))
            .close()
            .extrude(t)
    )

    # b = (
    #     cq.Workplane("XY")
    #         .rect(10, 10)
    #         .extrude(-0.5)
    #         .translate((0, 0, -xmax_mech_one_way - magnet_height - planar_front_rear_steel_thickness))
    # )

    b = (
        b
            .translate((-0.6, t/2, -0.5 / 2))
            .translate((0, edge_screw_width / 2, 0))
            .mirror(mirrorPlane='XZ', union=True)

            .translate((0, 0, -xmax_mech_one_way - magnet_height - planar_front_rear_steel_thickness))

            .translate((0, 0, cbt_radius))
            .rotate((0, 0, 0), (0, 1, 0), r)
            .translate((0, 0, -cbt_radius))
    )

    b = (
        b
            .fillet(0.2)
            .cut(calc_edge_screw_holes())
    )

    return b


def revolve_lengthwise(fn):
    return (
        planar_gasket.gasket_revolve(fn=fn, angle=planar_height_degrees, radius=cbt_radius)
            .rotate((0, 0, 0), (0, 1, 0), 90)
            .rotate((0, 0, 0), (1, 0, 0), 90)
    )


def calc_mid_flare():

    split_part_mid_margin = 0.01

    w = magnet_width / 2

    y0 = planar_thickness / 2
    # y0 = 0

    rx0 = planar_width/2

    mid_cut = (
        cq.Workplane("XY")
            .box(100, split_part_mid_margin, 100)
    )

    # r = 4 # 13.4 mm
    r = 4.9  # 15 mm

    w = r * 3

    flare_fillet = (
        calc_edge_part_2(
            width=w,
            top_r=cbt_radius + y0 + r,
            bot_r=cbt_radius - y0 - r,
            degrees=planar_height_degrees * 3
        )
        .fillet(r)
    )

    b = (
        flare_fillet.translate((0, w / 2 - magnet_width / 2, 0))
        .intersect(flare_fillet.translate((0, -w / 2 + magnet_width / 2, 0)))
        .intersect(calc_edge_part_2(
            width=planar_width,
            top_r=cbt_radius + 10,
            bot_r=cbt_radius - 10,
            degrees=planar_height_degrees
        ))
        .cut(mid_cut)
        .cut(calc_edge_part_2(
            width=planar_width,
            top_r=cbt_radius + y0,
            bot_r=cbt_radius - y0,
            degrees=planar_height_degrees + 2
        ))
    )

    b = (
        b
        .intersect((
            calc_edge_part(radius=cbt_radius + 10, width=planar_width)
        ))
    )

    import wg_shapes
    peg = wg_shapes.filament_peg(height=0.39)

    peg_z = 0.2

    b = b.cut(
        mirror_over_each_magnet((
            peg
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .translate((0, 0, planar_thickness / 2 + peg_z))
            .mirror(mirrorPlane='XY', union=True)
        ), ignore_even=True)
    )

    b = b.cut(
        mirror_over_each_magnet((
            wg_shapes.filament_peg()
                .rotate((0, 0, 0), (1, 0, 0), 90)
                .translate((0, 0, planar_thickness / 2 + peg_z))
                .translate((0, rx0 - r, 0))
                .mirror(mirrorPlane='XY', union=True)
        ), ignore_even=True)
    )

    b = b.mirror(mirrorPlane='XZ', union=True)

    return b


def calc_planar_wg():
    depth = 2.45

    w = depth * 3
    # total_w = 8.2
    total_w = planar_width

    d_sub = 0.3

    t = 0.2

    slot_w = 0.5

    total_degrees = num_magnets_lengthwise_top * magnet_ctc_length_degrees - degree_printer_wall_thickness

    # b = (
    #     calc_edge_part_2(
    #         width=w,
    #         top_r=cbt_radius + planar_thickness / 2 + depth,
    #         bot_r=cbt_radius - planar_thickness / 2 - depth,
    #         degrees=planar_height_degrees * 2
    #     )
    #     .chamfer(depth - d_sub)
    #     .fillet(d_sub * 2.41)
    #     .translate((0, w / 2, 0))
    #     .translate((0, radiating_area_width / 2, 0))
    # )
    #


    def calc_slot(offset=0.0):
        return (
            calc_edge_part_2(
                width=total_w - 2 * t,
                top_r=cbt_radius + 10,
                bot_r=cbt_radius + planar_thickness / 2 + d_sub,
                degrees=geometry_2d.degrees_across_circle(slot_w, cbt_radius),
                degrees_offset=offset
            )
            .fillet(slot_w * 0.45)
        )
    #
    # b = (
    #     b
    #     .shell(-t)
    #     .intersect((
    #         calc_edge_part_2(
    #             width=total_w,
    #             top_r=cbt_radius + planar_thickness / 2 + depth,
    #             bot_r=cbt_radius - planar_thickness / 2 - depth,
    #             # degrees=planar_height_degrees
    #             degrees=total_degrees
    #         )
    #     ))
    #     .union(g)
    #     .cut(
    #         calc_edge_part_2(
    #             width=planar_width + 10,
    #             top_r=cbt_radius + planar_thickness / 2,
    #             # bot_r=cbt_radius - planar_thickness / 2,
    #             bot_r=cbt_radius - planar_thickness / 2 - 10,
    #             degrees=planar_height_degrees
    #         )
    #     )
    #     # .cut(calc_edge_screw_holes(screw_diameter=edge_screw_diameter, screw_h=planar_thickness + 3 * t))
    #     # .cut(calc_edge_screw_holes(screw_diameter=edge_screw_diameter, screw_h=planar_thickness + 2 * 2.5))
    #     # .cut(calc_slot(offset=0))
    #     # .cut(calc_slot(offset=-total_degrees/2).mirror(mirrorPlane='YZ', union=True))
    #     # .cut(calc_slot(offset=total_degrees/2))
    #     # .fillet(t / 3)
    #     # .mirror(mirrorPlane='XZ', union=True)
    # )

    d = depth/2

    p = P(angle=60, x=d-d_sub)

    # print(f"p={p}")

    y = planar_thickness/2

    bb = (
        cq.Workplane("YZ")
        .center(0, 0)
        # .moveTo(radiating_area_width / 2, 0 + y)
        .moveTo(radiating_area_width / 2, 0 - y)
        .lineTo(radiating_area_width / 2, d_sub + y)
        .lineTo(radiating_area_width / 2 + p.y, d + y)
        .lineTo(planar_width, d + y)
        # .lineTo(planar_width, 0 + y)
        .lineTo(planar_width, 0 - y)
        # .lineTo(radiating_area_width / 2, 0)
        .close()
        # .extrude(1)
        .revolve(planar_height_degrees / 2, [0, -cbt_radius, 0], [-1000, -cbt_radius, 0], clean=False)
        .mirror(mirrorPlane='YZ', union=True)
        .fillet(d_sub * 1.72) # 60 degrees
        # .translate((0, 0, ))
    )

    g = (
        calc_edge_part_2(
            width=planar_width,
            top_r=cbt_radius + planar_thickness / 2 + t,
            bot_r=cbt_radius + planar_thickness / 2,
            degrees=total_degrees
        )
        .intersect(bb)
    )

    bb = (
        bb
        .shell(-t)
        .intersect((
            calc_edge_part_2(
                width=total_w,
                top_r=cbt_radius + planar_thickness / 2 + depth,
                bot_r=cbt_radius + planar_thickness / 2,
                # degrees=planar_height_degrees
                degrees=total_degrees
            )
        ))
        .union(g)
        .cut(calc_edge_screw_holes(screw_diameter=edge_screw_diameter_medium_fit, screw_h=planar_thickness + 3 * t))
        .cut(calc_edge_screw_holes(screw_diameter=edge_screw_diameter_medium_fit, screw_h=planar_thickness + 2 * 2.5))
        .cut(calc_slot(offset=0))
        .cut(calc_slot(offset=-total_degrees/2))
        .cut(calc_slot(offset=total_degrees/2))
        # .chamfer(t / 10)
        .cut((
            calc_edge_part_2(
                width=total_w,
                top_r=cbt_radius + planar_thickness / 2 + t,
                bot_r=cbt_radius + planar_thickness / 2,
                # degrees=planar_height_degrees
                degrees=2 * magnet_ctc_length_degrees,
                degrees_offset=1.5 * magnet_ctc_length_degrees,
            )
            .cut((
                calc_edge_part_2(
                    width=total_w - 1.75,
                    top_r=cbt_radius + planar_thickness / 2 + t,
                    bot_r=cbt_radius + planar_thickness / 2,
                    degrees=total_degrees
                )
            ))
            .mirror(mirrorPlane='YZ', union=True)
        ))
    )

    # b = b.union(bb)
    b = bb

    return b


def calc_steel_plate_roll_ref():

    w = planar_width + 2
    h = 4
    r = (num_magnets_lengthwise + 2) * magnet_ctc_length_degrees

    b = (
        cq.Workplane("XY")
            .box((num_magnets_lengthwise * 2) * magnet_length, w, h)
            .translate((0, 0, -0.2))
    )

    mid_cut = (
        calc_edge_part_2(
            width=planar_width + 4 * printer_margin,
            degrees=r,
            top_r=cbt_radius + planar_front_rear_steel_thickness / 2,
            bot_r=cbt_radius - planar_front_rear_steel_thickness / 2,
        )
        .union(
            calc_edge_part_2(
                width=w,
                degrees=r,
                top_r=cbt_radius + printer_margin / 2,
                bot_r=cbt_radius - printer_margin / 2,
            )
        )
    )

    edge_cut = (
        cq.Workplane("XY")
            .box(100, 100, 100)
            .cut(
            calc_edge_part_2(
                width=w,
                degrees=r,
                top_r=cbt_radius + h,
                bot_r=cbt_radius - h,
            )
        )
    )

    b = (
        b
            .cut(mid_cut)
            .cut(edge_cut)
    )

    return b


def calc_steel_plate_flat(top=True, screw_diameter=edge_screw_diameter):
    t = planar_front_rear_steel_thickness
    if not top:
        t = -t

    b = (
        cq.Workplane("XY")
            .rect(magnet_length, planar_width - 2 * edge_index_peg_diameter_margin)
            .extrude(t)
    )

    gap = (
        cq.Workplane("XY")
            .rect(magnet_length - 2 * steel_plate_magnet_support_width, gap_width_between_magnets)
            .extrude(t)
            .cut((
            cq.Workplane("XY")
                .rect(steel_plate_magnet_support_width, gap_width_between_magnets)
                .extrude(t)
        ))
            .edges("|Z")
            .fillet(steel_plate_magnet_gap_fillet)
    )

    gaps = []
    for i in range(0, math.floor(num_magnets_rows / 2)):
        gaps.append(
            gap.translate((0, (0.5 + i) * magnet_width + (0.5 + i) * gap_width_between_magnets, 0))
        )

    gaps = wg_functions.union_shapes_list(gaps)

    gaps = gaps.mirror(mirrorPlane='XZ', union=True)

    b = b.cut(gaps)

    b = b.cut(calc_edge_screw_holes_for_plate(screw_diameter=screw_diameter))
    if RENDER_EDGE_STEEL_INDEX_PEGS:
        b = b.cut(calc_edge_index_pegs())

    z = magnet_z_offset + magnet_height
    b = b.translate((0, 0, z if top else -z))

    return b


def calc_steel_edge_filament_glue_pegs():
    w = planar_width + 2 * planar_front_rear_steel_thickness
    peg_depth = planar_front_rear_steel_thickness + edge_screw_diameter_medium_fit

    filament_glue_pegs = (
        cq.Workplane("XY")
            .circle(0.175 / 2)
            .extrude(peg_depth)
            .translate((0, 0, w / 2 - peg_depth))
            .translate((index_peg_step / 2, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .mirror(mirrorPlane='XZ', union=True)
    )

    return mirror_over_each_magnet(filament_glue_pegs)


def calc_steel_edge_curved_support():
    delta = planar_thickness_inner / 2 + planar_front_rear_steel_thickness
    w = planar_width + 2 * planar_front_rear_steel_thickness
    b = (
        calc_edge_part_square(radius=cbt_radius + delta, width=w)
            .cut(
            calc_edge_part_square(radius=cbt_radius - delta, width=w)
        )
            .cut(
            calc_edge_part_square(radius=cbt_radius + delta, width=planar_width)
        )
    )

    if RENDER_STEEL_EDGE_SUPPORT:
        b = b.cut(calc_steel_edge_filament_glue_pegs())

    return b


def calc_threaded_insert():
    screw = (
        cq.Workplane("XY")
            .circle(Screw('m3').heatsert_diameter / 2)
            .extrude(-Screw('m3').heatsert_short_depth)
            .translate((0, 0, planar_thickness / 2 - planar_front_rear_steel_thickness))
            .translate((0, edge_screw_width / 2, 0))
    )

    return screw


def calc_middle_screw():
    screw = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 0.5)
            .extrude(Screw('m3').heatsert_long_depth)
            .translate((0, 0, -planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    head = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 1.0)
            .extrude(-Screw('m3').screw_head_thickness)
            .translate((0, 0, -planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    screw = screw.union(head)

    return screw


def calc_outer_screws_and_nuts():
    screw = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 0.5)
            .extrude(-planar_thickness - 0.4)
            .translate((0, 0, planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    head = (
        cq.Workplane("XY")
            .circle(Screw('m3').screw_diameter_tight * 1.0)
            .extrude(Screw('m3').screw_head_thickness)
            .translate((0, 0, planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    nut_l = Screw('m3').screw_diameter_tight * 1.0

    p0 = P(length=nut_l, angle=0)
    p1 = P(length=nut_l, angle=60)
    p2 = P(length=nut_l, angle=120)
    p3 = P(length=nut_l, angle=180)
    p4 = P(length=nut_l, angle=240)
    p5 = P(length=nut_l, angle=300)

    nut = (
        cq.Workplane("XY")
            .moveTo(p0.x, p0.y)
            .lineTo(p1.x, p1.y)
            .lineTo(p2.x, p2.y)
            .lineTo(p3.x, p3.y)
            .lineTo(p4.x, p4.y)
            .lineTo(p5.x, p5.y)
            .close()
            .extrude(-Screw('m3').screw_head_thickness)
            .translate((0, 0, -planar_thickness / 2))
            .translate((0, edge_screw_width / 2, 0))
    )

    screw_step = (magnet_length - 2 * screw_offset_from_edge) / (num_screws_per_magnet - 1)

    screw = (
        screw
            .union(head)
            .union(nut)
            .translate((screw_step, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
    )

    return screw


def calc_gasked_solid_squiggly(t, outer_r_curved):
    suspension_big_r = 10 * t

    s2_r = membrane_corrugation_length * 2 - t

    x_offset = 0
    if membrane_corrugation_peaks_per_squiggly == 2:
        x = 1.1
    elif membrane_corrugation_peaks_per_squiggly == 3:
        x = 1.70
    y = 0.8
    s0 = True

    # x_offset = membrane_corrugation_length
    # x = 1.1
    # y = 1.2
    # s0 = False

    if s0:
        s0 = (
            cq.Workplane("XY")
                .rect(2 * t, 3.5 * t)
                .extrude(gasket_actual_width)
                .translate((0, 1.25 * t, 0))
                .translate((0, 0, -gasket_actual_width / 2))
        )

    s2 = (
        cq.Workplane("XY")
            .ellipse(s2_r * x + t / 2, s2_r * y + t / 2, 0)
            .ellipse(s2_r * x - t / 2, s2_r * y - t / 2, 0)
            # .circle(s2_r + t / 2)
            # .circle(s2_r - t / 2)
            .extrude(gasket_actual_width)
            .intersect((
            cq.Workplane("XY")
                .polarLineTo(suspension_big_r, 5)
                .polarLineTo(suspension_big_r, 0)
                .polarLineTo(suspension_big_r, -45)
                .polarLineTo(suspension_big_r, -90)
                .polarLineTo(suspension_big_r, -135)
                .polarLineTo(suspension_big_r, -180)
                .polarLineTo(suspension_big_r, -185)
                .close()
                .extrude(gasket_actual_width)
        ))
            .translate((x_offset, 0, 0))
            # .translate((-suspension_small_r, 0, 0))
            .translate((0, outer_r_curved, 0))
            .translate((0, 0, -gasket_actual_width / 2))
    )

    if s0:
        s = s2.union(s0)
    else:
        s = s2

    s = (
        s
            .rotate((0, 0, 0), (1, 0, 0), 90)
    )

    return s


def calc_gasket_solid_new(top=True):
    outer_r_curved = planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height

    x_left = radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness
    x_right = cavity_width

    width = (x_right - x_left) / 2

    print(f"boop_x_left={x_left}")
    print(f"boop_x_right={x_right}")
    print(f"boop_width={width}")

    x_center = x_left / 2 + width / 2

    # r = inner_r_curved - membrane_xmax_one_way - t / 2

    height = outer_r_curved - membrane_xmax_one_way

    walk_width = printer_wall_thickness_gasket
    r = height * 0.75
    walk_middle = width - 2 * r - walk_width * 2

    edge_overlap = planar_width - gasket_actual_width

    import wg_path
    # w = ( # walk from middle
    #     wg_path.WgPath(width=walk_width, angle=0, current_p=P(-width / 2 - edge_overlap, -walk_width / 2))
    #         .walk_straight(edge_overlap)
    #         .walk_arc(-75, height / 4)
    #         .walk_arc(75, r)
    #         .walk_straight(walk_middle + r * 0.25)
    #         .walk_arc(90, r * 0.75)
    #         .walk_straight(r * 0.25)
    # )


    # w = ( # walk from bot
    #     wg_path.WgPath(width=walk_width, angle=0, current_p=P(-width / 2 - edge_overlap, -planar_thickness_inner / 2 + walk_width / 2))
    #         .walk_straight(edge_overlap)
    #         .walk_arc(75, height / 4)
    #         .walk_arc(-90, r * 0.5)
    #         .walk_straight(walk_middle + r * 0.15)
    #         .walk_arc(105, r * 1.0)
    #         .walk_straight(r * 0.25)
    # )

    r = height / 6
    d = width - 2 * r - 2 * printer_wall_thickness_gasket

    # w = ( # walk from bot squiggly
    #     wg_path.WgPath(width=walk_width, angle=90, current_p=P(-width / 2 + walk_width + d / 2 + r / 2, -planar_thickness_inner / 2 + walk_width / 2))
    #         .walk_arc(-90, r)
    #         .walk_straight(d / 2 - r / 2)
    #         .walk_arc(180, r)
    #         .walk_straight(d)
    #         .walk_arc(-180, r)
    #         .walk_straight(d / 2)
    # )

    offset_from_edge = walk_width * 1.5 + d / 2 + r / 2
    w = ( # walk from bot squiggly
        wg_path.WgPath(width=walk_width, angle=90, current_p=P(-width / 2 + offset_from_edge, -planar_thickness_inner / 2 - walk_width/2))
            .walk_straight(walk_width * 1.5)
            .walk_arc(-85, r)
            .walk_straight(d / 2 - r / 2)
            .walk_arc(170, r)
            .walk_straight(d)
            .walk_arc(-170, r)
            .walk_straight(d / 2)
            .walk_arc(85, r)
    )

    # print('w_walk:')
    # print(w.right_walked_points)
    # print(w.left_walked_points)

    def gasket_fn(workplane):
        return (
            w.draw_lines_and_arc_on_workplane(workplane, invert_y=top)
                .close()
        )

    b = (
        planar_gasket.gasket_revolve(fn=gasket_fn, angle=planar_height_degrees, radius=cbt_radius)
            .rotate((0, 0, 0), (0, 1, 0), 90)
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .mirror(mirrorPlane='XZ', union=False)
            .translate((0, x_center, 0))
    )

    intersect_box = (
        cq.Workplane("XY")
            .box(100, 100, 100)
            .cut((
            calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=planar_width)
        ))
    )

    b = (
        b
            .cut(intersect_box)
    )

    membrane = calc_membrane_only(
        width=membrane_width + 2 * printer_wall_thickness_gasket,
        thickness=membrane_thickness + 2 * printer_wall_thickness_gasket
    )

    membrane_edge = (
        membrane
            .cut(calc_edge_part(radius=cbt_radius + planar_thickness, width=membrane_width))
            .intersect(calc_edge_part(radius=cbt_radius, width=membrane_width + 2 * printer_wall_thickness_gasket))
    )

    membrane_cut, membrane_cut_outer, membrane_cut_inner = calc_membrane(
        width=membrane_width,
        thickness=membrane_thickness
    )

    b = b.cut(membrane)

    if top:
        membrane_cut_box = membrane_cut_outer
    else:
        membrane_cut_box = (
            cq.Workplane("XY")
                .box(100, 100, 100)
                .cut(membrane_cut_outer)
        )

    membrane_platform = (
        membrane
            .cut(membrane_cut_box)
            .cut(membrane_cut)
            .cut(calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=radiating_area_width_including_edge_magnets + 3 * printer_wall_thickness_gasket))
            .cut(intersect_box)
    )

    if top:
        bot_platform_to_edge = (
            (
                calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2, width=planar_width)
            )
                .cut(
                calc_edge_part_square(
                    radius=cbt_radius + planar_thickness_inner / 2,
                    width=wg_functions.middle_value(radiating_area_width_including_edge_magnets, gasket_width) + walk_width
                )
            )
                .cut(
                calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2 - 2 * printer_wall_thickness_gasket, width=planar_width)
            )
        )
    else:
        bot_platform_to_edge = (
            (
                calc_edge_part_square(radius=cbt_radius - planar_thickness_inner / 2 + 2 * printer_wall_thickness_gasket, width=planar_width)
            )
                .cut(
                calc_edge_part_square(
                    radius=cbt_radius - planar_thickness_inner / 2 + 2 * printer_wall_thickness_gasket,
                    width=wg_functions.middle_value(radiating_area_width_including_edge_magnets, gasket_width) + walk_width
                )
            )
                .cut(
                calc_edge_part_square(radius=cbt_radius - planar_thickness_inner / 2, width=planar_width)
            )
        )

    if top:
        membrane_platform = membrane_platform.cut(membrane_edge)
    else:
        b = b.union(membrane_edge)

    b = (
        b
            .union(membrane_platform)
            .union(bot_platform_to_edge)
            .mirror(mirrorPlane='XZ', union=True)
    )

    gasket_intersect_cut_box = (
        cq.Workplane("XY")
            .box(100, 100, 100)
            .cut((
            (
                calc_edge_part_square(radius=cbt_radius + planar_thickness_inner / 2, width=planar_width)
            )
                .cut(
                calc_edge_part_square(radius=cbt_radius - planar_thickness_inner / 2, width=planar_width)
            )
        ))
    )

    b = b.cut(gasket_intersect_cut_box)

    return b


def calc_gasket_solid(top=True):
    if GASKET_SOLID_NEW_STYLE:
        return calc_gasket_solid_new(top=top)

    top_bot_walls = 1

    t = printer_wall_thickness_gasket

    outer_r_curved = planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height - t * top_bot_walls

    inner_r_curved = planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height - t * top_bot_walls

    b = (
        calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height, width=gasket_actual_width)
            .cut(
            calc_edge_part(radius=cbt_radius + outer_r_curved
                           , width=gasket_actual_width))
    )

    b_down = (
        calc_edge_part(radius=cbt_radius - inner_r_curved
                       , width=gasket_actual_width)
            .cut(
            calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2 + planar_edge_inner_cavity_edge_height, width=gasket_actual_width)
        )
    )

    b = b.union(b_down)

    membrane_shell = (
        calc_membrane_only(
            width=gasket_actual_width,
            # thickness=membrane_thickness + 2 * gasket_margin + 2 * t
            thickness=3 * t
        )
    )

    top_bot_z_offset = (-1 if top else 1) * (membrane_xmax_one_way - membrane_thickness)
    membrane_shell = membrane_shell.translate((0, 0, top_bot_z_offset))

    b = b.union(membrane_shell)

    s = calc_gasked_solid_squiggly(t, outer_r_curved)
    s_down = (
        calc_gasked_solid_squiggly(t, outer_r_curved)
            .mirror(mirrorPlane='XY', union=False)
    )

    squigglys = []

    degrees_per_corrugation = geometry_2d.degrees_across_circle(membrane_corrugation_length * 2, cbt_radius)
    num_corugations_half = (planar_height_degrees / 2) / degrees_per_corrugation

    for i in range(0, math.ceil(num_corugations_half) + 1):
        if i % membrane_corrugation_peaks_per_squiggly != 0:
            continue
        for sign in [-1, 1]:
            if i == 0 and sign == -1:
                continue
            if membrane_corrugation_peaks_per_squiggly == 2:
                offset_top = 0.5 if gasket_squiggly_at_peak else 0.25
                offset_bot = 0 if gasket_squiggly_at_peak else 0.25
            if membrane_corrugation_peaks_per_squiggly == 3:
                offset_top = 0 if gasket_squiggly_at_peak else 0.25
                offset_bot = 0.5 if gasket_squiggly_at_peak else 0.25
            offset = offset_top if top else offset_bot
            offset = 0
            squigglys.append((
                (s if top else s_down)
                    .translate((0, 0, cbt_radius))
                    .rotate((0, 0, 0), (0, 1, 0), (offset + sign * i) * degrees_per_corrugation)
                    .translate((0, 0, -cbt_radius))
            ))
            # squigglys.append((
            #     s_down
            #         .translate((0, 0, cbt_radius))
            #         .rotate((0, 0, 0), (0, 1, 0), (0.5 + sign * i) * degrees_per_corrugation)
            #         .translate((0, 0, -cbt_radius))
            # ))

    b = b.union(wg_functions.union_shapes_list(squigglys))

    b = (
        b.cut(calc_edge_part(radius=cbt_radius + planar_thickness, width=radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness))
            #     .cut(
            #     membrane_inner
            # )
            .intersect(calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=gasket_actual_width))
    )

    bot_solid = (
        calc_membrane_corrugation_gear(radius=cbt_radius - 0 / 2, width=gasket_actual_width)
            .translate((0, 0, top_bot_z_offset))
    )
    if top:
        b = b.cut(bot_solid)
    else:
        b = b.intersect(bot_solid)

    return b


if RENDER_MEMBRANE:
    membrane, membrane_outer, membrane_inner = calc_membrane(width=membrane_width, thickness=membrane_thickness)
    membrane = membrane.intersect(calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=planar_width))

magnets_top = calc_magnets(outer=True) if RENDER_MAGNETS else None
magnets_bot = calc_magnets(outer=False) if RENDER_MAGNETS else None


def calc_magnet_glue_jig_curved_index_pegs(diameter=0.3, length=2 * xmax_mech_one_way):
    peg = (
        cq.Workplane("XY")
            .circle(diameter / 2)
            .extrude(length)
            .translate((0, 0, -length / 2))
            .chamfer(diameter / 6)
    )

    pegs = []

    for i in range(-math.floor(num_magnet_gap_rows / 2), math.floor(num_magnet_gap_rows / 2)):
        pegs.append((
            peg.translate((0, (0.5 + i) * magnet_width + (0.5 + i) * gap_width_between_magnets, 0))
        ))

    pegs = wg_functions.union_shapes_list(pegs)

    return pegs


def calc_magnets_glue_jig_support():
    t = 0.3
    r = cbt_radius + xmax_mech_one_way - magnet_glue_jig_h_above_magnets
    degrees = geometry_2d.degrees_across_circle(0.8, cbt_radius)
    b = (
        (
            calc_edge_part_square(radius=r, width=radiating_area_width_including_edge_magnets + 2.5 * magnet_width, degrees=degrees, mirror_over_magnets=False)
        )
    )


    pegs = calc_magnet_glue_jig_curved_index_pegs(diameter=0.3 - 0.5 * printer_margin, length= 2 * xmax_mech_one_way - 6 * printer_margin)

    b = (
        b
            .union(pegs)
            .cut((
            calc_edge_part_square(radius=r - t, width=planar_width, degrees=degrees, mirror_over_magnets=False)
        ))
    )

    return b


def calc_magnets_glue_jig_even():

    ignore_odd_magnet = True
    t_top = 0.25
    t = magnet_height + t_top
    bot_offset = magnet_height / 3

    w = magnet_width + 1.75 * gap_width_between_magnets
    r = planar_height_degrees + geometry_2d.degrees_across_circle(distance=3 * magnets_glue_jig_thickness, radius=cbt_radius)
    if ignore_odd_magnet:
        r = magnet_ctc_length_degrees * 2 - geometry_2d.degrees_across_circle(0.1, cbt_radius)

    z_offset = xmax_mech_one_way + magnet_height
    b = (
        calc_edge_part_square(radius=cbt_radius + z_offset - bot_offset, width=w, degrees=r, mirror_over_magnets=False)
            .cut(calc_edge_part_square(radius=cbt_radius + z_offset - t, width=w, degrees=r, mirror_over_magnets=False))
    )

    if ignore_odd_magnet:
        b = mirror_over_each_magnet(b, ignore_odd=True, i_list=[0])

    length_margin = 3 * printer_margin
    width_margin_outer = 4 * printer_margin
    width_margin_inner = 4 * printer_margin

    magnet_top_fillet_negative = (
        (
            cq.Workplane("XY")
                .rect(10, 10)
                .extrude(-(magnet_height + t_top + t_top))
                .translate((0, 0, t_top))
                .translate((0, 0, xmax_mech_one_way + magnet_height))

        )
            .cut((
            cq.Workplane("XY")
                .rect((actual_magnet_length + length_margin), (magnet_width + width_margin_inner))
                .extrude(-10)
                .translate((0, 0, 5))
        ))
            .edges("|Y or |X")
            .fillet(t_top)
    )

    magnet_top_fillet = (
        (
            cq.Workplane("XY")
                .rect(8, 8)
                .extrude(-(magnet_height + t_top))
                .translate((0, 0, xmax_mech_one_way + magnet_height))

        )
        .cut(magnet_top_fillet_negative)
        .translate((0, 0, -xmax_mech_one_way - 0.01))
    )

    # magnet = (
    #     cq.Workplane("XY")
    #         .rect((actual_magnet_length + length_margin), (magnet_width + width_margin))
    #         .extrude(magnet_height)
    # )

    # return magnet_top_fillet
    # return magnet

    b = (
        b
            # .cut(mirror_over_each_magnet(calc_magnets_row(outer=True, magnet=magnet_cut), ignore_odd=ignore_odd_magnet))
            .cut(mirror_over_each_magnet(calc_magnets_row(outer=True, length_margin=length_margin, width_margin=width_margin_outer)))
            .cut(mirror_over_each_magnet(calc_magnets_row(outer=True, magnet=magnet_top_fillet, magnet_rows=[0]),
                                         ignore_odd=True))
            .cut(calc_edge_screw_holes())
    )

    return b


def calc_magnets_glue_jig(top, ignore_odd_magnet=True):

    t_top = magnet_height * 0.5
    t = magnet_height * 0.5 + t_top

    # t = magnet_height * 0.99

    length_margin = 3 * printer_margin
    width_margin_inner = 4 * printer_margin

    magnet_top_fillet_negative = (
        (
            cq.Workplane("XY")
                .rect(10, 10)
                .extrude(-(magnet_height + t_top + t_top))
                .translate((0, 0, t_top))
                .translate((0, 0, xmax_mech_one_way + magnet_height))

        )
            .cut((
            cq.Workplane("XY")
                .rect((actual_magnet_length + length_margin), (magnet_width + width_margin_inner))
                .extrude(-10)
                .translate((0, 0, 5))
        ))
            .edges("|Y or |X")
            .fillet(t_top)
    )

    magnet_cut = (
        cq.Workplane("XY")
            .rect((actual_magnet_length + length_margin + 2 * t_top), (magnet_width + width_margin_inner + 2 * t_top))
            .extrude(magnet_height if top else -magnet_height)
        .cut(magnet_top_fillet_negative)
    )

    # magnet_cut = (
    #     (
    #         cq.Workplane("XY")
    #             .rect((actual_magnet_length + length_margin), (magnet_width + width_margin_inner))
    #             .extrude(magnet_height * 2)
    #             .translate((0, 0, -magnet_height))
    #     )
    #         .union((
    #         cq.Workplane("XY")
    #             .rect((actual_magnet_length + length_margin + t * 1.5), (magnet_width + width_margin_inner + t * 1.5))
    #             .extrude(t if top else -t)
    #             .translate((0, 0, t if top else -t))
    #     ))
    # )

    # return magnet_cut

    w = planar_width + 2 * magnets_glue_jig_thickness
    r = planar_height_degrees + geometry_2d.degrees_across_circle(distance=3 * magnets_glue_jig_thickness, radius=cbt_radius)
    if ignore_odd_magnet:
        r = magnet_ctc_length_degrees * 2 - geometry_2d.degrees_across_circle(0.1, cbt_radius)

    z_offset = xmax_mech_one_way + magnet_height
    if top:
        b = (
            calc_edge_part_square(radius=cbt_radius + z_offset, width=w, degrees=r, mirror_over_magnets=False)
                .cut(calc_edge_part_square(radius=cbt_radius + z_offset - t, width=w, degrees=r, mirror_over_magnets=False))
        )
    else:
        b = (
            calc_edge_part_square(radius=cbt_radius - z_offset + t, width=w, degrees=r, mirror_over_magnets=False)
                .cut(calc_edge_part_square(radius=cbt_radius - z_offset, width=w, degrees=r, mirror_over_magnets=False))
        )

    if ignore_odd_magnet:
        b = mirror_over_each_magnet(b, ignore_odd=True, i_list=[0])

    cbt_radius_text = 'flat' if cbt_radius > 1000 else round(cbt_radius, 1)
    # text = (
    #     cq.Workplane("XY")
    #         .text(txt=str(f"{'' if top else ' '}{' top' if top else 'bot'}   r-{cbt_radius_text}"), fontsize=0.8, distance=-(xmax_mech_one_way + t), cut=True,
    #               clean=True)
    #         .rotate((0, 0, 0), (0, 1, 0), (180 if top else 0))
    #         .translate((0, edge_screw_width / 2 + 0.4, 0))
    #         .rotate((0, 0, 0), (0, 0, 1), (180 if top else 0))
    # )

    b = (
        b
            .cut(mirror_over_each_magnet(calc_magnets_row(outer=top, magnet=magnet_cut), ignore_odd=ignore_odd_magnet))
            .cut(calc_edge_screw_holes())
            # .cut(text)
    )

    return b


magnets_glue_jig_top = calc_magnets_glue_jig(top=True) if RENDER_MAGNET_GLUE_JIG else None
magnets_glue_jig_top_2 = calc_magnets_glue_jig(top=True, ignore_odd_magnet=False) if RENDER_MAGNET_GLUE_JIG_2 else None
magnets_glue_jig_bot, magnets_glue_jig_bot_2 = None, None
# magnets_glue_jig_bot = calc_magnets_glue_jig(top=False) if RENDER_MAGNET_GLUE_JIG else None
# magnets_glue_jig_bot_2 = calc_magnets_glue_jig(top=False, ignore_odd_magnet=False) if RENDER_MAGNET_GLUE_JIG_2 else None
magnets_glue_jig_support = calc_magnets_glue_jig_support() if RENDER_MAGNET_GLUE_JIG_SUPPORT else None
magnets_glue_jig_even = calc_magnets_glue_jig_even() if RENDER_MAGNET_GLUE_JIG_EVEN else None


edge_cavity = (
    calc_edge_part_2(
        width=cavity_width,
        top_r=cbt_radius + planar_edge_inner_cavity_height / 2,
        bot_r=cbt_radius - planar_edge_inner_cavity_height / 2,
    )
)

edge = None
edge_top = None
edge_top_floor = None
edge_top_ceil = None
edge_bot = None
edge_top_cap = None
edge_bot_cap = None
edge_shim = None

if RENDER_EDGE:
    edge = (
        (
            calc_edge_part_2(
                width=planar_width,
                top_r=cbt_radius + planar_thickness_inner / 2,
                bot_r=cbt_radius - planar_thickness_inner / 2,
            )
        )
        .cut((
            calc_edge_part_2(
                width=edge_inner_width,
                top_r=cbt_radius + planar_thickness_inner / 2,
                bot_r=cbt_radius - planar_thickness_inner / 2,
            )
        ))
        .cut(edge_cavity)
    )

    screw_holes = calc_edge_screw_holes()
    if RENDER_EDGE_STEEL_INDEX_PEGS:
        edge_screw_index_pegs = calc_edge_index_pegs(screw_diameter=edge_index_peg_diameter - edge_index_peg_diameter_margin)
        edge = edge.union(edge_screw_index_pegs)
    edge = edge.cut(screw_holes)

if RENDER_EDGE_SHIM:
    edge_shim = (
        calc_edge_part(radius=cbt_radius + edge_shim_thickness / 2, width=planar_width)
            .cut(calc_edge_part(radius=cbt_radius - edge_shim_thickness / 2, width=planar_width))
            .cut(edge_cavity)
            .cut(calc_edge_screw_holes(extra_inner_solid_length=2))
    )

if RENDER_EDGE_EXTRA_WITH_FILLET and RENDER_EDGE:
    edge_extra = (
        calc_edge_part_square(radius=cbt_radius + planar_thickness / 2, width=planar_extra_edge_width)
            .cut(calc_edge_part_square(radius=cbt_radius - planar_thickness / 2, width=planar_extra_edge_width))
            .cut(calc_edge_part_square(radius=cbt_radius + planar_thickness / 2, width=planar_width))
            .edges(">Y and |X or <Y and |X")
            .fillet(planar_extra_edge_fillet)
    )
    edge = edge.union(edge_extra)


if RENDER_EDGE and gasket_solid_to_top_steel:
    edge = edge.cut(gasket_top_cavity_solid(width=cavity_width))


def calc_foam_suspension(top):
    if top:
        return (
            (
                calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=gasket_actual_width)
            )
            .cut((
                calc_edge_part(radius=cbt_radius + membrane_corrugation_depth_peak_to_peak / 2, width=gasket_actual_width)
            ))
            .cut((
                calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=gasket_actual_inner_width)
            ))
        )
    else:
        return (
            (
                calc_edge_part(radius=cbt_radius - membrane_corrugation_depth_peak_to_peak / 2, width=gasket_actual_width)
            )
                .cut((
                calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2, width=gasket_actual_width)
            ))
                .cut((
                calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=gasket_actual_inner_width)
            ))
        )


def calc_edge_cylinder_pegs(diameter=0.3):

    t0 = 0.1
    t_max = (planar_width - cavity_width) / 2
    t_angle = (t_max - 3 * t0) / 2 - printer_wall_thickness

    inner_r = diameter / 3
    outer_r = diameter / 2

    # b = (
    #     cq.Workplane("XZ")
    #         .moveTo(0, 0)
    #         .lineTo(outer_r, 0)
    #
    #         .lineTo(outer_r, t0)
    #
    #         .lineTo(inner_r, t0 + t_angle)
    #         .lineTo(inner_r, t0 + t_angle + t0)
    #
    #         .lineTo(outer_r, t0 + t_angle + t0 + t_angle)
    #
    #         .lineTo(outer_r, t_max)
    #         .lineTo(0, t_max)
    #         .close()
    #         .revolve(360, (0, 1, 0), (0, 0, 0))
    # )

    b = (
        (
            cq.Workplane("XY")
                .circle(inner_r)
                .extrude(t_max)
        )
            .union((
            cq.Workplane("XY")
                .circle(outer_r)
                .extrude(t0 + 3 * t_angle)
                .translate((0, 0, -2 * t_angle))
                .fillet(t_angle * 1.1)
                .mirror(mirrorPlane='XY', union=True, basePointVector=(0, 0, t_max / 2))
        ))

    )

    b = (
            b
            .rotate((0, 0, 0), (1, 0, 0), 90)
            .translate((0, planar_width/2, 0))
                .mirror(mirrorPlane='XZ', union=True)
    )

    return mirror_over_each_magnet(b, i_offset=0.5)


foam_suspension_top = calc_foam_suspension(top=True) if RENDER_FOAM_SUSPENSION_TOP else None
foam_suspension_bot = calc_foam_suspension(top=False) if RENDER_FOAM_SUSPENSION_TOP else None

gasket_solid_top = calc_gasket_solid(top=True) if RENDER_GASKET_SOLID_TOP else None
gasket_solid_bot = calc_gasket_solid(top=False) if RENDER_GASKET_SOLID_BOT else None

if (RENDER_GASKET_SOLID_TOP or RENDER_GASKET_SOLID_BOT) and RENDER_HORIZONTAL_GASKET_TABS:
    tabs_intersect = (
        calc_edge_part_square(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=gasket_actual_width)
            .cut(
            calc_edge_part_square(radius=cbt_radius - planar_edge_inner_cavity_height / 2, width=gasket_actual_width)
        )
            .cut(
            calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height * 1.5, width=gasket_actual_width)
                .cut(
                calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2 + planar_edge_inner_cavity_edge_height * 1.5, width=gasket_actual_width)
            )
        )
            .cut(
            calc_edge_part_square(radius=cbt_radius + planar_thickness / 2, width=radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness)
        )
    )
    if RENDER_GASKET_SOLID_TOP:
        gasket_solid_top = gasket_solid_top.union(
            calc_gasket_tabs(radius=gasket_tabs_radius, width=gasket_actual_width, top=True).intersect(tabs_intersect)
        )
    if RENDER_GASKET_SOLID_BOT:
        gasket_solid_bot = gasket_solid_bot.union(
            calc_gasket_tabs(radius=gasket_tabs_radius, width=gasket_actual_width, top=False).intersect(tabs_intersect)
        )

if RENDER_EDGE and edge:
    if RENDER_EDGE_MARGINS_OVER_UNDER_GASKET:
        ttop = (
            calc_edge_part_square(radius=cbt_radius + planar_edge_inner_cavity_height / 2, width=cavity_width)
                .cut(
                calc_edge_part(radius=cbt_radius + planar_edge_inner_cavity_height / 2 - planar_edge_inner_cavity_edge_height, width=cavity_width)
            )
        )
        bbot = (
            calc_edge_part(radius=cbt_radius - planar_edge_inner_cavity_height / 2 + planar_edge_inner_cavity_edge_height, width=cavity_width)
                .cut(
                calc_edge_part_square(radius=cbt_radius - planar_edge_inner_cavity_height / 2, width=cavity_width)
            )
        )
        mmid = (
            calc_edge_part_square(radius=cbt_radius + planar_edge_inner_cavity_height / 2,
                                  width=radiating_area_width_including_edge_magnets + 2 * printer_wall_thickness)
        )
        tb = (
            ttop
                .union(bbot)
                .cut(mmid)
        )
        edge = edge.union(tb)

    chamfer_r = 2 * printer_wall_thickness
    # chamfer_cut_thickness = membrane_thickness + 2 * 1.5 * membrane_xmax_one_way + 2 * printer_wall_thickness + 2 * chamfer_r
    # chamfer_cut_thickness = planar_edge_inner_cavity_height - 2 * (planar_edge_inner_cavity_edge_height + printer_wall_thickness_gasket)
    chamfer_cut_thickness = planar_edge_inner_cavity_height - 2 * planar_edge_inner_cavity_edge_height

    # mmembrane_chamfer_cut = (
    #     calc_edge_part(radius=cbt_radius + chamfer_cut_thickness / 2, width=cavity_width + 2 * printer_wall_thickness, degrees=180)
    #         .cut(
    #         calc_edge_part(radius=cbt_radius - chamfer_cut_thickness / 2, width=cavity_width + 2 * printer_wall_thickness, degrees=180)
    #     )
    #         .edges(">Y or <Y")
    #         .chamfer(chamfer_r)
    # )

    # mmembrane = calc_membrane_only(width=cavity_width + 2 * printer_wall_thickness, thickness=membrane_thickness + 2 * 1.5 * membrane_xmax_one_way + 2 * printer_wall_thickness)

    # mmembrane = mmembrane.chamfer(membrane_xmax_one_way)
    # edge = edge.cut(mmembrane_chamfer_cut)

    edge_cavity_maybe_tabs = None

    if RENDER_HORIZONTAL_GASKET_TABS:
        edge_cavity_maybe_tabs = (
            calc_gasket_tabs(radius=gasket_tabs_radius + 2 * gasket_margin, width=cavity_width, top=True)
                .union(
                calc_gasket_tabs(radius=gasket_tabs_radius + 2 * gasket_margin, width=cavity_width, top=False)
            )
        )
    if RENDER_VERTICAL_GASKET_TABS:
        edge_cavity_maybe_tabs = calc_edge_index_pegs(screw_diameter=gasket_tabs_radius + 2 * gasket_margin, width=cavity_width / 2, squared=False)

    if edge_cavity_maybe_tabs:
        edge = edge.cut(edge_cavity_maybe_tabs)

if RENDER_STEEL_EDGE_SUPPORT:
    edge = edge.cut(calc_steel_edge_filament_glue_pegs())
# edge = edge.union(edge_screw_index_pegs)

steel_plates_single = None
steel_plates_top = None
steel_plates_bot = None
steel_plates_bot_sups = []

if RENDER_STEEL_PLATES:
    if horizontal_magnets or RENDER_CURVED_STEEL_PLATE:
        steel_plates_top = calc_steel_plate_curved(top=True, magnet_lengths=num_magnets_lengthwise_top)
        steel_plates_top_floor = calc_steel_plate_curved(top=True, magnet_lengths=math.floor(num_magnets_lengthwise_top / 2),
                                                         x_offset_magnet_lengths=-2
                                                         )
        steel_plates_top_ceil = calc_steel_plate_curved(top=True, magnet_lengths=math.ceil(num_magnets_lengthwise_top / 2),
                                                        x_offset_magnet_lengths=1.5
                                                        )

        steel_plates_bot = calc_steel_plate_curved(top=False, magnet_lengths=num_magnets_lengthwise_bot)
        steel_plates_bot_sups.append(calc_steel_plate_curved(top=False, z_offset=-planar_front_rear_steel_thickness, magnet_lengths=num_magnets_lengthwise_bot_sup,
                                                             normal_magnet_holes=False,
                                                             bot_supports_mid_holes_cut=True,
                                                             x_offset_magnet_lengths=0.5,
                                                             sup_chamfer_fillet=True,
                                                             ))
        steel_plates_bot_sups.append(calc_steel_plate_curved(top=False, z_offset=-planar_front_rear_steel_thickness, magnet_lengths=num_magnets_lengthwise_bot_sup-1,
                                                             normal_magnet_holes=False,
                                                             bot_supports_mid_holes_cut=True,
                                                             x_offset_magnet_lengths=0.0,
                                                             sup_chamfer_fillet=True,
                                                             ))

        steel_plates_bot_sups.append(calc_steel_plate_curved(top=False, z_offset=-planar_front_rear_steel_thickness, magnet_lengths=num_magnets_lengthwise_bot_sup,
                                                             normal_magnet_holes=False,
                                                             bot_supports_mid_holes_cut=True,
                                                             x_offset_magnet_lengths=0.5,
                                                             sup_chamfer_fillet=True,
                                                             sup_m4_screw_hole_offset=0.0
                                                             ))
        steel_plates_bot_sups.append(calc_steel_plate_curved(top=False, z_offset=-planar_front_rear_steel_thickness, magnet_lengths=num_magnets_lengthwise_bot_sup-1,
                                                             normal_magnet_holes=False,
                                                             bot_supports_mid_holes_cut=True,
                                                             x_offset_magnet_lengths=0.0,
                                                             sup_chamfer_fillet=True,
                                                             sup_m4_screw_hole_offset=0.5
                                                             ))
        steel_plates_bot_sups.append(calc_steel_plate_curved(top=False, z_offset=-planar_front_rear_steel_thickness, magnet_lengths=num_magnets_lengthwise_bot_sup-1,
                                                             normal_magnet_holes=False,
                                                             bot_supports_mid_holes_cut=True,
                                                             x_offset_magnet_lengths=0.0,
                                                             sup_chamfer_fillet=True,
                                                             sup_m4_screw_hole_offset=0.0
                                                             ))
    else:
        steel_plates_single = calc_steel_plate_flat(top=True)
        steel_plates_top = mirror_over_each_magnet(calc_steel_plate_flat(top=True))
        steel_plates_bot = mirror_over_each_magnet(calc_steel_plate_flat(top=False))

steel_edge_curved_support = None
if RENDER_STEEL_PLATE_SUPPORTS:
    steel_plate_support_top = mirror_over_each_magnet(
        calc_magnet_supports_for_plate(top=True))
    steel_plate_support_bot = mirror_over_each_magnet(
        calc_magnet_supports_for_plate(top=False))
    steel_edge_curved_support = calc_steel_edge_curved_support() if RENDER_STEEL_EDGE_SUPPORT else None

# middle_cut = (
#     calc_edge_part(radius=cbt_radius + middle_cut_margin / 2, width=planar_width)
#         .cut(calc_edge_part(radius=cbt_radius - middle_cut_margin / 2, width=planar_width))
# )

edge_cylinder_pegs = calc_edge_cylinder_pegs() if RENDER_EDGE_CYLINDER_PEGS else None

if RENDER_EDGE and EDGE_MEMBRANE_CHAMFER_MARGIN:

    # width = 2 * (edge_screw_width - edge_screw_diameter_medium_fit / 2 - printer_wall_thickness)

    # chamfer = (width - cavity_width) / 2
    chamfer = 1.0 * printer_wall_thickness

    edge = edge.cut((
        calc_edge_part_2(
            width=cavity_width + 2 * chamfer,
            top_r=cbt_radius + (xmax_mech_one_way * 1.2 + chamfer),
            bot_r=cbt_radius - (xmax_mech_one_way * 1.2 + chamfer),
            degrees=planar_height_degrees + 2
        )
        .chamfer(chamfer)
    ))

    edge = edge.cut((
        calc_edge_part_2(
            width=cavity_width + 2 * chamfer,
            top_r=cbt_radius + planar_thickness_inner / 2 + 10,
            bot_r=cbt_radius + planar_thickness_inner / 2 - 0.05,
            degrees=planar_height_degrees + 2
        )
        # .chamfer(chamfer)
    ))

    edge = edge.cut((
        calc_edge_part_2(
            width=cavity_width + 2 * chamfer,
            top_r=cbt_radius - planar_thickness_inner / 2 + 0.05,
            bot_r=cbt_radius - planar_thickness_inner / 2 - 10,
            degrees=planar_height_degrees + 2
        )
        # .chamfer(chamfer)
    ))


degree_printer_wall_thickness = geometry_2d.degrees_across_circle(printer_wall_thickness, cbt_radius)

if RENDER_EDGE and EDGE_TOP_BOT_SPLIT:

    edge_cylinder_peg_diameter = 0.45

    edge_top_cut_box = (
        cq.Workplane("XY")
            .box(100, 100, 100)
            .cut((
            calc_edge_part_2(
                width=planar_width,
                bot_r=cbt_radius + middle_cut_margin / 2,
            )
        ))
    )

    edge_bot_cut_box = (
        cq.Workplane("XY")
            .box(100, 100, 100)
            .cut((
            calc_edge_part_2(
                width=planar_width,
                top_r=cbt_radius - middle_cut_margin / 2,
                degrees=num_magnets_lengthwise_bot * magnet_ctc_length_degrees - degree_printer_wall_thickness,
            )
        ))
    )

    edge_top_raw = (
        edge
        .cut(edge_top_cut_box)
    )

    if EDGE_CYLINDER_PEGS:
        edge_top_raw = (
            edge_top_raw
                .cut((
                calc_edge_cylinder_pegs(diameter=edge_cylinder_peg_diameter + printer_margin)
            ))
        )

    edge_top = (
        edge_top_raw
        .intersect((
            calc_edge_part_2(
                width=planar_width,
                degrees=num_magnets_lengthwise_top * magnet_ctc_length_degrees - degree_printer_wall_thickness,
            )
        ))
    )

    edge_top_floor_degrees = math.floor(num_magnets_lengthwise_top / 2) * magnet_ctc_length_degrees
    edge_top_floor = (
        edge_top_raw
            .intersect((
            calc_edge_part_2(
                width=planar_width,
                degrees=edge_top_floor_degrees - degree_printer_wall_thickness,
                degrees_offset=-edge_top_floor_degrees / 2 - magnet_ctc_length_degrees / 2,
            )
        ))
    )

    edge_top_ceil_degrees = math.ceil(num_magnets_lengthwise_top / 2) * magnet_ctc_length_degrees
    edge_top_ceil = (
        edge_top_raw
            .intersect((
            calc_edge_part_2(
                width=planar_width,
                degrees=edge_top_ceil_degrees - degree_printer_wall_thickness,
                degrees_offset=edge_top_ceil_degrees / 2 - magnet_ctc_length_degrees / 2,
            )
        ))
    )

    edge_bot = edge.cut(edge_bot_cut_box)

    if EDGE_CYLINDER_PEGS:
        edge_bot = edge_bot.union((
            calc_edge_cylinder_pegs(diameter=edge_cylinder_peg_diameter)
                .cut((
                cq.Workplane("XY")
                    .box(100, 100, 100)
                    .cut(edge)
            ))
            .cut((
                cq.Workplane("XY")
                    .box(100, 100, 100)
                    .cut((
                    calc_edge_part_2(
                        width=planar_width,
                        degrees=num_magnets_lengthwise_bot * magnet_ctc_length_degrees - degree_printer_wall_thickness,
                    )
                ))
            ))
        ))


    edge = None

if RENDER_EDGE and EDGE_CAPS:
    screw_holes = calc_edge_screw_holes()

    edge_top_cap = (
        edge_top
        .intersect((
            calc_edge_part_2(
                width=planar_width,
                degrees=magnet_ctc_length_degrees - degree_printer_wall_thickness,
                degrees_offset=0,
            )
        ))
        .union((
            calc_edge_part_2(
                width=planar_width - 0.6,
                degrees=magnet_ctc_length_degrees / 2 - degree_printer_wall_thickness,
                degrees_offset=magnet_ctc_length_degrees / 4,

                top_r=cbt_radius + planar_thickness_inner / 2 + planar_front_rear_steel_thickness,
                bot_r=cbt_radius + xmax_mech_one_way,
            )
            # .fillet(0.05)
        ))
        .union((
            calc_edge_part_2(
                width=planar_width,
                degrees=magnet_ctc_length_degrees - degree_printer_wall_thickness,

                top_r=cbt_radius + planar_thickness_inner / 2 + planar_front_rear_steel_thickness,
                bot_r=cbt_radius + planar_thickness_inner / 2,
            )
        ))
        .cut(screw_holes)
    )

    edge_bot_cap = (
        edge_bot
        .intersect((
            calc_edge_part_2(
                width=planar_width,
                degrees=magnet_ctc_length_degrees - degree_printer_wall_thickness,
                degrees_offset=0,
            )
        ))
        .union((
            calc_edge_part_2(
                width=planar_width - 0.6,
                degrees=magnet_ctc_length_degrees / 2 - degree_printer_wall_thickness,
                degrees_offset=magnet_ctc_length_degrees / 4,

                top_r=cbt_radius - xmax_mech_one_way,
                bot_r=cbt_radius - planar_thickness_inner / 2 - planar_front_rear_steel_thickness,
            )
            # .fillet(0.05)
        ))
        .cut((
            calc_edge_part_2(
                width=planar_width - 0.6,
                degrees=magnet_ctc_length_degrees / 2 - degree_printer_wall_thickness,
                degrees_offset=magnet_ctc_length_degrees / 4,

                top_r=cbt_radius + planar_thickness_inner / 2,
                bot_r=cbt_radius + xmax_mech_one_way - printer_margin,
            )
        ))
        .union((
            calc_edge_part_2(
                width=planar_width,
                degrees=magnet_ctc_length_degrees - degree_printer_wall_thickness,

                top_r=cbt_radius - planar_thickness_inner / 2,
                bot_r=cbt_radius - planar_thickness_inner / 2 - planar_front_rear_steel_thickness,
            )
        ))
        .cut(screw_holes)
    )


if RENDER_MIDDLE_MEMBRANE_XMAX_COMPARISON:
    middle_membrane_xmax_comparison_squiggly = (
        calc_membrane_only(width=planar_width, thickness=middle_membrane_xmax_comparison_height)
            .intersect(calc_edge_part(radius=cbt_radius + planar_thickness / 2, width=planar_width))
    )
    middle_membrane_xmax_comparison_flat = (
        calc_edge_part(radius=cbt_radius + membrane_xmax_one_way, width=membrane_width)
            .cut(calc_edge_part(radius=cbt_radius - membrane_xmax_one_way, width=membrane_width))
    )

threaded_insert = mirror_over_each_magnet(calc_threaded_insert()) if RENDER_THREADED_INSERT else None

middle_screw = mirror_over_each_magnet(calc_middle_screw()) if RENDER_MIDDLE_MOUNT_SCREW else None
outer_screws_and_nuts = mirror_over_each_magnet(calc_outer_screws_and_nuts()) if RENDER_OUTER_SCREWS_AND_NUTS else None

edge_right, edge_left = None, None

if SPLIT_EDGE_LEFT_RIGHT and edge:
    right = (
        cq.Workplane("XY")
            .box(100, 100, 100)
            .translate((0, 50, 0))
    )
    edge_right = edge.intersect(right)
    edge_left = edge.cut(right)
    edge = None


outer_hole_cheapskate_grid_fix = calc_outer_hole_cheapskate_grid_fix() if RENDER_OUTER_HOLE_CHEAPSKATE_GRID_FIX else None

test_hook = calc_test_hook() if RENDER_TEST_HOOK else None
mid_flare = calc_mid_flare() if RENDER_MID_FLARE else None
steel_plate_roll_ref = calc_steel_plate_roll_ref() if RENDER_STEEL_PLATE_ROLL_REFERENCE else None
planar_wg = calc_planar_wg() if RENDER_PLANAR_WG else None


planar_shape = None
if RENDER_PLANAR_SHAPE:
    planar_shape = (
        calc_edge_part_2(
            width=planar_width,
            top_r=cbt_radius + planar_thickness / 2,
            bot_r=cbt_radius - planar_thickness / 2,
            degrees=planar_height_degrees
        )
    )

# show_object(membrane_corrugation_outer, name=f'membrane_corrugation_outer', options={"color": (125, 45, 45)})
# show_object(membrane_corrugation_inner, name=f'membrane_corrugation_inner', options={"color": (125, 45, 45)})
# show_object(gasket_tabs, name=f'gasket_tabs', options={"color": (45, 45, 125)})
if RENDER_MIDDLE_MEMBRANE_XMAX_COMPARISON:
    show_object(middle_membrane_xmax_comparison_squiggly, name=f'middle_membrane_xmax_comparison_squiggly', options={"color": (125, 45, 45), "alpha": 0.5})
    show_object(middle_membrane_xmax_comparison_flat, name=f'middle_membrane_xmax_comparison_flat', options={"color": (125, 45, 45), "alpha": 0.5})
if RENDER_MEMBRANE:
    show_object(membrane, name=f'membrane', options={"color": (125, 45, 45)})
# show_object(screw_holes, name=f'screw_holes', options={"color": (120, 120, 120)})
if RENDER_MAGNETS:
    show_object(magnets_top, name=f'magnets_top', options={"color": (40, 40, 40)})
    show_object(magnets_bot, name=f'magnets_bot', options={"color": (40, 40, 40)})
# show_object(magnet_plate_gaps(), name=f'magnet_plate_gaps', options={"color": (40, 40, 40)})

if magnets_glue_jig_top:
    show_object(magnets_glue_jig_top, name=f'magnets_glue_jig_top', options={"color": (40, 40, 125), "alpha": 0.5})
if magnets_glue_jig_top_2:
    show_object(magnets_glue_jig_top_2, name=f'magnets_glue_jig_top_2', options={"color": (40, 40, 125), "alpha": 0.5})
if magnets_glue_jig_bot:
    show_object(magnets_glue_jig_bot, name=f'magnets_glue_jig_bot', options={"color": (40, 40, 125), "alpha": 0.5})
if magnets_glue_jig_bot_2:
    show_object(magnets_glue_jig_bot_2, name=f'magnets_glue_jig_bot_2', options={"color": (40, 40, 125), "alpha": 0.5})
if magnets_glue_jig_support:
    show_object(magnets_glue_jig_support, name=f'magnets_glue_jig_support', options={"color": (40, 125, 40), "alpha": 0.5})
if magnets_glue_jig_even:
    show_object(magnets_glue_jig_even, name=f'magnets_glue_jig_even', options={"color": (40, 125, 40), "alpha": 0.5})

if outer_hole_cheapskate_grid_fix:
    show_object(outer_hole_cheapskate_grid_fix, name=f'outer_hole_cheapskate_grid_fix', options={"color": (200, 60, 0), "alpha": 0.5})

if test_hook:
    show_object(test_hook, name=f'test_hook', options={"color": (200, 60, 0), "alpha": 0.5})

if mid_flare:
    show_object(mid_flare, name=f'mid_flare', options={"color": (200, 60, 0), "alpha": 0.5})

if steel_plate_roll_ref:
    show_object(steel_plate_roll_ref, name=f'steel_plate_roll_ref', options={"color": (200, 60, 0), "alpha": 0.5})

if edge_cylinder_pegs:
    show_object(edge_cylinder_pegs, name=f'edge_cylinder_pegs', options={"color": (200, 60, 0), "alpha": 0.5})

if foam_suspension_top:
    show_object(foam_suspension_top, name=f'foam_suspension_top', options={"color": (200, 60, 0), "alpha": 0.5})
if foam_suspension_bot:
    show_object(foam_suspension_bot, name=f'foam_suspension_bot', options={"color": (200, 60, 0), "alpha": 0.5})

if gasket_solid_top:
    show_object(gasket_solid_top, name=f'gasket_solid_top', options={"color": (200, 60, 0)})
if gasket_solid_bot:
    show_object(gasket_solid_bot, name=f'gasket_solid_bot', options={"color": (200, 60, 0)})
# show_object(steel_plates, name=f'steel_plates', options={"color": (15, 15, 15)})

# show_object(edge_screw_index_pegs, name=f'edge_screw_index_pegs', options={"color": (125, 90, 0)})

if threaded_insert:
    show_object(threaded_insert, name=f'threaded_insert', options={"color": (200, 90, 0)})
if RENDER_EDGE:
    if edge_left and edge_right:
        show_object(edge_left, name=f'edge_left', options={"color": (125, 90, 0), "alpha": 0.5})
        show_object(edge_right, name=f'edge_right', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge:
        show_object(edge, name=f'edge', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_top:
        show_object(edge_top, name=f'edge_top', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_top_floor:
        show_object(edge_top_floor, name=f'edge_top_floor', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_top_ceil:
        show_object(edge_top_ceil, name=f'edge_top_ceil', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_top_cap:
        show_object(edge_top_cap, name=f'edge_top_cap', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_bot_cap:
        show_object(edge_bot_cap, name=f'edge_bot_cap', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_bot:
        show_object(edge_bot, name=f'edge_bot', options={"color": (125, 90, 0), "alpha": 0.5})
    if edge_shim:
        show_object(edge_shim, name=f'edge_shim', options={"color": (125, 90, 0), "alpha": 0.5})
if planar_shape:
    show_object(planar_shape, name=f'planar_shape', options={"color": (125, 90, 0), "alpha": 0.5})
if planar_wg:
    show_object(planar_wg, name=f'planar_wg', options={"color": (200, 60, 0), "alpha": 0.5})
# show_object(edge_top, name=f'edge_top', options={"color": (125, 90, 0)})
# show_object(edge_bot, name=f'edge_bot', options={"color": (125, 90, 0)})
if RENDER_STEEL_PLATE_SUPPORTS:
    if steel_plate_support_top:
        show_object(steel_plate_support_top, name=f'steel_plate_support_top', options={"color": (125, 90, 0)})
    if steel_plate_support_bot:
        show_object(steel_plate_support_bot, name=f'steel_plate_support_bot', options={"color": (125, 90, 0)})
# show_object(gasket_top, name=f'gasket_top', options={"color": (15, 15, 15)})
# show_object(gasket_bot, name=f'gasket_bot', options={"color": (15, 15, 15)})
if RENDER_STEEL_PLATES:
    if steel_plates_top:
        show_object(steel_plates_top, name=f'steel_plates_top', options={"color": (120, 120, 120)})
    if steel_plates_top_floor:
        show_object(steel_plates_top_floor, name=f'steel_plates_top_floor', options={"color": (120, 120, 120)})
    if steel_plates_top_ceil:
        show_object(steel_plates_top_ceil, name=f'steel_plates_top_ceil', options={"color": (120, 120, 120)})
    if steel_plates_bot:
        show_object(steel_plates_bot, name=f'steel_plates_bot', options={"color": (120, 120, 120)})
    for i in range(0, len(steel_plates_bot_sups)):
        steel_plates_bot_sup = steel_plates_bot_sups[i]
        show_object(steel_plates_bot_sup, name=f'steel_plates_bot_sup_{i}', options={"color": (120, 120, 120)})

if RENDER_STEEL_PLATE_SUPPORTS:
    if steel_edge_curved_support:
        show_object(steel_edge_curved_support, name=f'steel_edge_curved_support', options={"color": (120, 120, 120), "alpha": 0.5})
# show_object(steel_plates_single, name=f'steel_plates_single', options={"color": (120, 120, 120)})

if middle_screw:
    show_object(middle_screw, name=f'middle_screw', options={"color": (0, 0, 120)})
if outer_screws_and_nuts:
    show_object(outer_screws_and_nuts, name=f'outer_screws_and_nuts', options={"color": (120, 0, 0)})

if EXPORT_GASKET:
    if RENDER_GASKET_SOLID_TOP and gasket_solid_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(gasket_solid_top), f"gasket_solid_top_{magnet_size_str}.step")
    if RENDER_GASKET_SOLID_BOT and gasket_solid_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(gasket_solid_bot), f"gasket_solid_bot_{magnet_size_str}.step")

if EXPORT_EDGE:
    if edge_left and edge_right:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_right), f"edge_right_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_left), f"edge_left_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge), f"edge_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_top), f"edge_top_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge_top_floor:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_top_floor), f"edge_top_floor_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge_top_ceil:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_top_ceil), f"edge_top_ceil_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge_top_cap:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_top_cap), f"edge_top_cap_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge_bot_cap:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_bot_cap), f"edge_bot_cap{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_bot), f"edge_bot_{cbt_curve_str}_{magnet_size_str}_{xmax_str}.step")
    if edge_shim:
        cq.exporters.export(wg_functions.scale_cm_to_mm(edge_shim), f"edge_shim_{cbt_curve_str}_{edge_shim_thickness_str}.step")

if EXPORT_STEEL_PLATE:
    if steel_plates_single:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_single), f"steel_plates_single_{steel_plate_size_str}.step")
    if steel_plates_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_top), f"steel_plates_top_curved_{steel_plate_size_str}.step")
    if steel_plates_top_floor:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_top_floor), f"steel_plates_top_floor_curved_{steel_plate_size_str}.step")
    if steel_plates_top_ceil:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_top_ceil), f"steel_plates_top_ceil_curved_{steel_plate_size_str}.step")
    if steel_plates_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_bot), f"steel_plates_bot_curved_{steel_plate_size_str}.step")
    for i in range(0, len(steel_plates_bot_sups)):
        steel_plates_bot_sup = steel_plates_bot_sups[i]
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plates_bot_sup), f"steel_plates_bot_sup_{i}_curved_{steel_plate_size_str}.step")
    if steel_edge_curved_support:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_edge_curved_support),
                            f"steel_edge_curved_support_{num_magnets_lengthwise}_lengthwise.step")

if EXPORT_STEEL_PLATE_SUPPORTS:
    if steel_plate_support_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plate_support_top), f"steel_plate_support_top_{steel_plate_size_str}.step")
    if steel_plate_support_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plate_support_bot), f"steel_plate_support_bot_{steel_plate_size_str}.step")

if EXPORT_MAGNET_GLUE_JIG:
    if magnets_glue_jig_top:
        cq.exporters.export(wg_functions.scale_cm_to_mm(magnets_glue_jig_top), f"magnets_glue_jig_top_{steel_plate_size_str}.step")
    if magnets_glue_jig_top_2:
        cq.exporters.export(wg_functions.scale_cm_to_mm(magnets_glue_jig_top_2), f"magnets_glue_jig_top_2_{steel_plate_size_str}.step")
    if magnets_glue_jig_bot:
        cq.exporters.export(wg_functions.scale_cm_to_mm(magnets_glue_jig_bot), f"magnets_glue_jig_bot_{steel_plate_size_str}.step")
    if magnets_glue_jig_bot_2:
        cq.exporters.export(wg_functions.scale_cm_to_mm(magnets_glue_jig_bot_2), f"magnets_glue_jig_bot_2_{steel_plate_size_str}.step")
    if magnets_glue_jig_support:
        cq.exporters.export(wg_functions.scale_cm_to_mm(magnets_glue_jig_support), f"magnets_glue_jig_support_{steel_plate_size_str}.step")
    if magnets_glue_jig_even:
        cq.exporters.export(wg_functions.scale_cm_to_mm(magnets_glue_jig_even), f"magnets_glue_jig_even_{steel_plate_size_str}.step")

if EXPORT_OUTER_HOLE_CHEAPSKATE_GRID_FIX:
        if outer_hole_cheapskate_grid_fix:
            cq.exporters.export(wg_functions.scale_cm_to_mm(outer_hole_cheapskate_grid_fix), "outer_hole_cheapskate_grid_fix.step")
if EXPORT_TEST_HOOK:
        if test_hook:
            cq.exporters.export(wg_functions.scale_cm_to_mm(test_hook), f"test_hook_{cbt_curve_str}.step")

if EXPORT_MID_FLARE:
        if mid_flare:
            cq.exporters.export(wg_functions.scale_cm_to_mm(mid_flare), f"mid_flare_{cbt_curve_str}.step")

if EXPORT_PLANAR_WG:
        if planar_wg:
            cq.exporters.export(wg_functions.scale_cm_to_mm(planar_wg), f"planar_wg_{cbt_curve_str}.step")

if EXPORT_STEEL_PLATE_ROLL_REF:
        if steel_plate_roll_ref:
            cq.exporters.export(wg_functions.scale_cm_to_mm(steel_plate_roll_ref), f"steel_plate_roll_ref_{cbt_curve_str}.step")