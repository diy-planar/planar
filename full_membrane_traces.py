import math
import sys

import geometry_2d
from geometry_2d import P
import membrane_functions
import shading_calculator
import wg_path

cbt_radius = 200.0
# cbt_radius = 1500.0
magnet_length = 12.5
corrugation_shortening_comp_ratio = 1.15

membrane_width = 55

max_v = shading_calculator.Values(db=-12)
membrane_length_mm = geometry_2d.distance_across_circle(max_v.degree, cbt_radius) * corrugation_shortening_comp_ratio
membrane_top_length_mm = 7.5
membrane_bot_length_mm = 7.5

traces_top_rounding_offset = 0.8

magnet_rows = 5

magnet_width = 5
magnet_to_magnet_gap = 5

magnet_row_ctc = magnet_width + magnet_to_magnet_gap

num_traces_rows = 4

magnet_width = 5
magnet_to_magnet_gap = 5
traces_width = 3
trace_gap_width = 0.5


resistor_width = 0.95
resistor_length_factor = 0.87

aluminum_um = 20


def calc_trace_width(num_traces):
    return (traces_width - (num_traces - 1) * trace_gap_width) / num_traces


def calc_trace_width_ctc(num_traces):
    return calc_trace_width(num_traces) + trace_gap_width


print(f"calc_trace_width(4) = {calc_trace_width(4)}")
print(f"calc_trace_width(3) = {calc_trace_width(3)}")
print(f"calc_trace_width(2) = {calc_trace_width(2)}")
print(f"calc_trace_width(1) = {calc_trace_width(1)}")


#
class MembraneTraces:
    def __init__(self, length_mm, offset_mm, num_traces, last, trace_loop_around_top_length_mm, resistor_length_mm, resistor_width_mm):
        self.length_mm = length_mm
        self.offset_mm = offset_mm
        self.num_traces = num_traces
        self.trace_width = calc_trace_width(num_traces)
        self.last = last
        self.trace_loop_around_top_length_mm = trace_loop_around_top_length_mm
        self.resistor_length_mm = resistor_length_mm
        self.resistor_width_mm = resistor_width_mm

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        v = {
            "length_mm": round(self.length_mm, 2),
            "offset_mm": round(self.offset_mm, 2),
            "num_traces": self.num_traces,
            "trace_width": self.trace_width,
        }
        if self.trace_loop_around_top_length_mm:
            v['trace_loop_around_top_length_mm'] = round(self.trace_loop_around_top_length_mm, 2)
        if self.resistor_length_mm:
            v['resistor_length_mm'] = round(self.resistor_length_mm, 2)
        if self.resistor_width_mm:
            v['resistor_width_mm'] = round(self.resistor_width_mm, 2)
        return f"{v}"

print()
membrane_segments = []

shading_list_calculated = [
    0,
    16,
    29.6,
    40.8,
    50.5,
    56.3,
]

for (lower_r, upper_r, num_traces, last) in [
    # 4 traces max
    # (0, -1.25, 4),
    # (-1.25, -3, 3),
    # (-3, -6, 2),
    # (-6, -9, 1),
    # (-9, -12, 1),

    # Hand picked such that I believe they have less combined error
    (shading_list_calculated[0], shading_list_calculated[1], 2, False),
    (shading_list_calculated[1], shading_list_calculated[2], 2, False),
    (shading_list_calculated[2], shading_list_calculated[3], 1, False),
    (shading_list_calculated[3], shading_list_calculated[4], 1, False),
    (shading_list_calculated[4], shading_list_calculated[5], 1, False),
    (shading_list_calculated[5], shading_calculator.Values(db=-12).degree, 1, False),

]:
    # lower_r = shading_calculator.Values(db=lower_db).degree
    # upper_r = shading_calculator.Values(db=upper_db)
    max_v = shading_calculator.Values(db=-12)

    # print(f"{upper_db} = {upper_v.degree}")

    length_mm = geometry_2d.distance_across_circle(upper_r - lower_r, cbt_radius)

    trace_loop_around_top_length_mm, resistor_length_mm, resistor_width_mm = None, None, None

    if num_traces == 1:
        length_to_top_mm = geometry_2d.distance_across_circle(max_v.degree - lower_r,
                                                     cbt_radius) * corrugation_shortening_comp_ratio
        trace_loop_around_top_length_mm = 2 * (length_to_top_mm - traces_top_rounding_offset) + (3.14 / 2) * 10

    membrane_segments.append(MembraneTraces(
        length_mm=geometry_2d.distance_across_circle(upper_r - lower_r,
                                                     cbt_radius) * corrugation_shortening_comp_ratio,
        offset_mm=geometry_2d.distance_across_circle(lower_r, cbt_radius) * corrugation_shortening_comp_ratio,
        num_traces=num_traces,
        last=last,
        trace_loop_around_top_length_mm=trace_loop_around_top_length_mm,
        resistor_length_mm=resistor_length_mm,
        resistor_width_mm=resistor_width_mm,
    ))
    # print(values)

# calc resistor for -12 dB
ms = membrane_segments[-1]

# ratio = math.sqrt(2) / 2
length_ratio = (1 / (math.sqrt(2) / 2)) * (1 - math.sqrt(2) / 2)

main_r = membrane_functions.calc_alu_trace_resistance(ms.trace_width, ms.trace_loop_around_top_length_mm, aluminum_um)
# resistor_length_equal_mm = ms.trace_loop_around_top_length_mm / (ms.trace_width / resistor_width)
# resistor_length_mm = resistor_length_equal_mm / length_ratio
# resistor_r = membrane_functions.calc_alu_trace_resistance(resistor_width, resistor_length_mm, aluminum_um)

resistor_length_mm = ms.trace_loop_around_top_length_mm
resistor_r = membrane_functions.calc_alu_trace_resistance(resistor_width, resistor_length_mm, aluminum_um)
for i_mm in range(1, int(resistor_length_mm * 100)):
    i_mm = i_mm / 100
    i_r = membrane_functions.calc_alu_trace_resistance(resistor_width, i_mm, aluminum_um)
    if abs(length_ratio - (main_r / resistor_r)) > abs(length_ratio - (main_r / i_r)):
        resistor_length_mm = i_mm
        resistor_r = i_r

resistor_length_mm *= resistor_length_factor

last_combined_r = 1 / ((1 / main_r) + (1 / resistor_r))

print()
print(f"-12 dB main_r = {main_r}")
print(f"-12 dB resistor_r = {resistor_r}")
print(f"-12 dB last_combined_r = {last_combined_r}")
print(f"-12 dB trace_loop_around_top_length_mm = {trace_loop_around_top_length_mm}")
print(f"-12 dB resistor_length_mm = {resistor_length_mm}")
assert resistor_r > main_r
assert round(main_r / resistor_r, 4) == round(length_ratio, 4)


membrane_segments[-1] = MembraneTraces(
    length_mm=ms.length_mm,
    offset_mm=ms.offset_mm,
    num_traces=ms.num_traces,
    last=ms.last,
    trace_loop_around_top_length_mm=ms.trace_loop_around_top_length_mm,
    resistor_length_mm=resistor_length_mm,
    resistor_width_mm=resistor_width,
)

# calc resistor for -9 dB
ms = membrane_segments[-2]

# main_r = last_combined_r
main_r = last_combined_r + 2 * membrane_functions.calc_alu_trace_resistance(ms.trace_width, ms.length_mm, aluminum_um)
resistor_length_mm = ms.trace_loop_around_top_length_mm
resistor_r = membrane_functions.calc_alu_trace_resistance(resistor_width, resistor_length_mm, aluminum_um)
for i_mm in range(1, int(resistor_length_mm * 100)):
    i_mm = i_mm / 100
    i_r = membrane_functions.calc_alu_trace_resistance(resistor_width, i_mm, aluminum_um)
    if abs(length_ratio - (main_r / resistor_r)) > abs(length_ratio - (main_r / i_r)):
        resistor_length_mm = i_mm
        resistor_r = i_r

resistor_length_mm *= resistor_length_factor

print()
print(f"-9 dB main_r = {main_r}")
print(f"-9 dB resistor_r = {resistor_r}")
print(f"-9 dB trace_loop_around_top_length_mm = {trace_loop_around_top_length_mm}")
print(f"-9 dB resistor_length_mm = {resistor_length_mm}")
assert resistor_r > main_r
assert round(main_r / resistor_r, 4) == round(length_ratio, 4)

membrane_segments[-2] = MembraneTraces(
    length_mm=ms.length_mm,
    offset_mm=ms.offset_mm,
    num_traces=ms.num_traces,
    last=ms.last,
    trace_loop_around_top_length_mm=ms.trace_loop_around_top_length_mm,
    resistor_length_mm=resistor_length_mm,
    resistor_width_mm=resistor_width,
)

#

print()
for it in membrane_segments:
    print(it)


print()

approx_membrane_resistance = 0

for it in membrane_segments:
    segment_approx_membrane_resistance = 4 * it.num_traces * membrane_functions.calc_alu_trace_resistance(
        width_mm=it.trace_width,
        length_mm=it.length_mm,
        thickness_um=aluminum_um,
    )

    approx_membrane_resistance += segment_approx_membrane_resistance

    print(it)
    print(f"   trace_width = {it.trace_width}")
    print(f"   segment_approx_membrane_resistance = {segment_approx_membrane_resistance}")
    print()

print(f"approx_membrane_resistance = {approx_membrane_resistance}")

print()

import os
if os.environ.get('SKIP_CQ') == 'true':
    sys.exit()

print("Start of CQ")
import cadquery as cq
import wg_functions

t = 0.1

membrane = (
    cq.Workplane("XY")
    .rect(membrane_length_mm, membrane_width)
    .extrude(t)
    .translate((membrane_length_mm / 2, 0, 0))
)
# top
membrane = membrane.union(
    cq.Workplane("XY")
    .rect(membrane_top_length_mm, membrane_width)
    .extrude(t)
    .translate((membrane_top_length_mm / 2 + membrane_length_mm, 0, 0))
)
# bot
membrane = membrane.union(
    cq.Workplane("XY")
    .rect(membrane_bot_length_mm, membrane_width)
    .extrude(t)
    .translate((-membrane_bot_length_mm / 2, 0, 0))
)
membrane = (
    membrane
    .edges("|Z")
    .fillet(4)
)

magnet_row = (
    cq.Workplane("XY")
    .rect(membrane_length_mm, magnet_width)
    .extrude(3)
    .translate((membrane_length_mm / 2, 0, -1.5))
)

magnets = []
for i in range(-math.floor(magnet_rows / 2), math.ceil(magnet_rows / 2)):
    magnets.append(
        magnet_row.translate((0, i * magnet_row_ctc, 0))
    )

magnets = wg_functions.union_shapes_list(magnets)


#

def mirror_traces(shape):
    return (
        shape
        .translate((0, -magnet_width, 0))
        .mirror(mirrorPlane='ZX', union=True)
        .translate((0, -2 * magnet_width, 0))
        .mirror(mirrorPlane='ZX', union=True)
    )


def calc_traces(margin=0):
    all_traces = []

    for it in membrane_segments:
        tw = it.trace_width

        l = it.length_mm

        w = (
            wg_path.WgPath(width=tw + margin, current_p=P(0, 0))
            .walk_straight(it.length_mm)
        )

        trace = (
            w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
            .close()
            .extrude(t)
            .translate((it.offset_mm, 0, 0))
            .translate((0, traces_width / 2 - tw / 2, 0))
        )

        for i in range(0, it.num_traces):
            all_traces.append((
                trace
                .translate((0, -i * calc_trace_width_ctc(it.num_traces), 0))
            ))

        if margin > 0:
            b = (
                cq.Workplane("XY")
                .rect(l, traces_width + margin)
                .extrude(t)
                .translate((l / 2 + it.offset_mm, 0, 0))
                .translate((0, traces_width / 2 - traces_width / 2, 0))
            )
            all_traces.append(b)
        else:
            for i in range(1, it.num_traces + 1):
                all_traces.append((
                    trace
                    .translate((0, - (i - 1) * (tw + trace_gap_width), 0))
                ))

    traces = wg_functions.union_shapes_list(all_traces)

    return traces


#

traces = calc_traces(0)

traces = mirror_traces(traces)

# traces_fill, a, b = calc_traces(2 * trace_gap_width)

# traces_fill = (
#     traces_fill
#     .edges("|Z")
#     .fillet(trace_gap_width * 0.)
# )

# fill = membrane.cut(traces_fill)


# traces = traces.union(fill)

# traces = traces.intersect((
#     cq.Workplane("XY")
#     .rect(membrane_length_mm, membrane_width)
#     .extrude(t)
#     .translate((membrane_length_mm / 2 + membrane_length_mm - 260, 0, 0))
# ))


show_object(magnets, name='magnets', options={"color": (40, 40, 40), "alpha": 0.8})
show_object(membrane, name='membrane', options={"color": (255, 255, 255), "alpha": 0.9})

show_object(traces, name='traces', options={"alpha": 0.0})
# show_object(traces_fill, name='traces_fill', options={"alpha": 0.75})
# show_object(fill, name='fill', options={"alpha": 0.75})

EXPORT_DXF = False
if EXPORT_DXF:
    workplane = traces.faces("-Z")

    cq.exporters.export(
        workplane.section(),
        'full_membrane_traces.dxf'
    )