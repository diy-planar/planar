import cadquery as cq

FILAMENT_PEG_DIAMETER = 2.2  # 1.75 mm filament with 0.4mm nozzle
# FILAMENT_PEG_DIAMETER = 2.3  # 1.75 mm filament with 0.6mm nozzle


def filament_peg(d=FILAMENT_PEG_DIAMETER, height=None, scale=0.1):
    h = height if height else (8 * scale)
    chamfer = 0.5 * scale
    d = d * scale
    b = (
        cq.Workplane("XY")
            .circle(d / 2)
            .extrude(h)
            .translate((0, 0, -h / 2))
    )
    for s in [1, -1]:
        c = (
            cq.Workplane("XY")
                .circle(d / 2 + chamfer * 0.98)
                .extrude(2 * chamfer)
                .translate((0, 0, -chamfer))
                .chamfer(chamfer * 0.99)
        )
        b = b.union(c)
    return b
