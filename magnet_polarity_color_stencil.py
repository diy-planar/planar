import cadquery as cq

import wg_functions

edge_thickness = 3
edge_cut_thickness = 0.2

num_magnets = 20

magnet_length = 12
magnet_width = 5
magnet_thickness = 3

magnet_offset = 0.5

printer_margin = 0.1

magnet = (
    cq.Workplane("XY")
        .rect(magnet_length, magnet_width)
        .extrude(magnet_thickness)
        .translate((0, 0, -magnet_thickness / 2))
)

magnet_polarity_cut = (
    cq.Workplane("XY")
        .rect(magnet_length * 0.8, magnet_width + 2 * edge_thickness)
        .extrude(magnet_thickness / 2)
)

magnet_with_taper = (
    (
        cq.Workplane("XY")
            .moveTo(0, magnet_width * 0.5)
            .lineTo(magnet_length * 0.5, magnet_width * 0.5)
            .lineTo(magnet_length * 0.5, magnet_width * 0.25)
            .lineTo(magnet_length * 0.5 + magnet_offset * 2, -magnet_width * 0.25)
            .lineTo(magnet_length * 0.5 + magnet_offset * 2, -magnet_width * 0.5)
            .lineTo(0, -magnet_width * 0.5)
            .lineTo(-magnet_length * 0.5, -magnet_width * 0.5)
            .lineTo(-magnet_length * 0.5, magnet_width * 0.5)
            .close()
            .extrude(magnet_thickness)
            .translate((0, 0, -magnet_thickness / 2))
    )
)

magnets = []

for i in range(0, num_magnets):
    even = i % 2 == 1
    magnets.append((
        magnet
            .union(magnet_polarity_cut)
            # .rotate((0, 0, 0), (0, 0, 1), 180 if even else 0)
            # .translate((magnet_offset * (1 if even else -1), 0, 0))
            .translate((0, 0, i * magnet_thickness))
    ))

magnets = wg_functions.union_shapes_list(magnets)

magnets = (
    magnets
        .translate((0, 0, magnet_thickness / 2 + edge_thickness))
)

edge_length = magnet_length + 2 * magnet_offset + 2 * edge_thickness
edge_width = magnet_width + 1.5 * edge_thickness
edge_height = num_magnets * magnet_thickness + 2 * edge_thickness

edge_radius = 4

edge_length_incl_tabs = edge_length + 5
edge = (
    (
        cq.Workplane("XY")
            .box(
            edge_length,
            edge_width,
            edge_height
        )
    )
        .fillet(edge_thickness * 0.75)
        .union((
        cq.Workplane("XY")
            .circle(edge_width/2)
            .extrude(edge_height * 2)
            .fillet(edge_thickness * 0.75)
            .translate((0, 0, -0.75 * edge_thickness))
            .translate((0, 0, -edge_height * 1.25))
    ))
        .cut((
        cq.Workplane("XY")
            .box(
            2,
            2,
            2
        )
            .translate((0, 0, -edge_height / 2))
            .translate((0, edge_width/2, 0))
            .translate((0, 0, -edge_height * 0.75))
    ))
    #     .union((
    #     cq.Workplane("XY")
    #         .box(
    #         edge_length_incl_tabs,
    #         edge_thickness * 0.75,
    #         edge_height - 2 * edge_thickness
    #     )
    # ))
    #     .union((
    #     cq.Workplane("XY")
    #         .circle(edge_radius / 2)
    #         .extrude(edge_height - 2 * edge_thickness)
    #         .translate((0, 0, -(edge_height - 2 * edge_thickness) / 2))
    #         .translate(((edge_length_incl_tabs) / 2, 0, 0))
    #         .mirror(mirrorPlane='YZ', union=True)
    # ))
        .translate((0, 0, edge_height / 2))
    # .cut(magnets)
)


edge_tabs = (
    (
        cq.Workplane("XY")
            .circle(edge_radius * 0.8)
            .extrude(edge_height - 2 * edge_thickness)
            .translate((0, 0, -(edge_height - 2 * edge_thickness) / 2))
            .translate(((edge_length_incl_tabs) / 2, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
    )
        .cut((
        cq.Workplane("XY")
            .box(
            edge_length * 2,
            edge_width * 2,
            edge_height - 2 * 20
        )
    ))
        .cut((
        cq.Workplane("XY")
            .box(
            edge_length + 2 * printer_margin,
            edge_width,
            edge_height
        )
    ))
        .translate((0, 0, edge_height / 2))
    .cut(edge)
)

edge_circle = (
    cq.Workplane("XY")
        .circle(edge_width/2 + 1.5)
        .circle(edge_width/2 + printer_margin)
        .extrude(15)
)


edge = (
    edge
        .cut((
        cq.Workplane("XY")
            .box(
            edge_length * 4,
            edge_cut_thickness,
            edge_height * 4
        )
    ))
)

for x in [-printer_margin, 0, printer_margin]:
    for y in [-printer_margin, 0, printer_margin]:
        for z in [0]:
            edge = edge.cut(magnets.translate((x, y, z)))


# show_object(magnet, name=f'magnet', options={"color": (40, 40, 40)})
# show_object(magnet_polarity_cut, name=f'magnet_polarity_cut', options={"color": (40, 40, 40)})
# show_object(magnet_with_taper, name=f'magnet_with_taper', options={"color": (40, 40, 40)})

show_object(magnets, name=f'magnets', options={"color": (40, 40, 40)})

show_object(edge, name=f'edge', options={"color": (125, 90, 0), "alpha": 0.5})
# show_object(edge_tabs, name=f'edge_tabs', options={"color": (125, 40, 40), "alpha": 0.5})
show_object(edge_circle, name=f'edge_circle', options={"color": (125, 40, 40), "alpha": 0.5})

cq.exporters.export(edge, f"magnet_polarity_color_stencil_{num_magnets}x.step")
cq.exporters.export(edge_circle, f"edge_circle.step")


# cq.exporters.export(edge_tabs, f"edge_tabs.step")
