import cadquery as cq

from geometry_2d import P

import wg_path

guide_length = 7 * 12.5 * 1.3
guide_uncompressed_width = 6
guide_compressed_width = 1.5

guide_throat_radius = 10

scissors_blade_width = 2.1


printer_margin = 0.05
target_strip_width = 6

base_thickness = 12
full_guide_thickness = base_thickness + target_strip_width
scissor_cutout_guide_thickness = full_guide_thickness - scissors_blade_width

edge_width = 4

def calc_guide(thickness):
    w = (
        wg_path.WgPath(width=edge_width, current_p=P(0, 0))
        .walk_straight(guide_length / 2)
        .walk_arc(45, guide_throat_radius)
        .walk_straight(guide_throat_radius / 4)
        .walk_arc(-45, 3)
        .walk_straight(10)
    )

    return (
        w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
        .close()
        .extrude(thickness)
    )

b = (
    calc_guide(full_guide_thickness)
    .translate((0, edge_width / 2 + guide_compressed_width / 2, 0))
    .union((
        calc_guide(scissor_cutout_guide_thickness)
        .translate((0, edge_width / 2 + guide_compressed_width / 2, 0))
        .mirror(mirrorPlane='XZ', union=False)
    ))
    .mirror(mirrorPlane='YZ', union=True)
    .edges("|Z")
    .fillet(edge_width * 0.49)
)

base_triangle_xy = guide_throat_radius * 1.05

base_x = guide_length + guide_throat_radius * 2
base_y = guide_compressed_width + edge_width * 2 + base_triangle_xy

def calc_fill_blob(t):
    return (
        cq.Workplane("XY")
        .moveTo(0, guide_compressed_width)
        .lineTo(base_x / 2 - base_triangle_xy, guide_compressed_width)
        .lineTo(base_x / 2, base_y / 2 - 2)
        .lineTo(base_x / 2, base_y / 2)
        .lineTo(0, base_y / 2)
        .close()
        .extrude(t)
        .mirror(mirrorPlane='YZ', union=True)
    )

base = (
    cq.Workplane("XY")
    .rect(base_x, base_y)
    .extrude(base_thickness)
    .union(calc_fill_blob(full_guide_thickness))
    .union(calc_fill_blob(scissor_cutout_guide_thickness).mirror(mirrorPlane='XZ', union=False))
    # .union((
    #     cq.Workplane("XY")
    #     .lineTo(base_triangle_xy, base_triangle_xy)
    #     .lineTo(base_triangle_xy, 0)
    #     .lineTo(0, 0)
    #     .close()
    #     .extrude(base_thickness)
    #     .translate((0, -edge_width/2, 0))
    #     .translate((guide_length / 2, 0, 0))
    # .mirror(mirrorPlane='XZ', union=True)
    # .mirror(mirrorPlane='YZ', union=True)
    # ))
    # .cut((
    #     cq.Workplane("XY")
    #     .rect(guide_length + guide_throat_radius * 2, printer_margin)
    #     .extrude(base_thickness)
    #     .translate((0, guide_compressed_width / 3, 0))
    # ))
)

top_box = (
    cq.Workplane("XY")
    .box(guide_length * 2, 100, 100)
    .translate((0, 50, 0))
    .translate((0, guide_uncompressed_width, 0))
    .union((
        cq.Workplane("XY")
        .box(guide_length * 2, 100, 100)
        .translate((0, 50, 50 + base_thickness))
    ))
)

t = 0.5
shelf_cut = (
    cq.Workplane("XY")
    .box(guide_length * 2, guide_uncompressed_width, t)
    .translate((0, guide_uncompressed_width / 2, base_thickness))
)

b = b.union(base)
b = b.cut(shelf_cut)

b = b.intersect((
    cq.Workplane("XY")
    .box(1000, base_y, 1000)
))

b_top = b.cut((
    top_box
    .translate((0, -printer_margin / 2, 0))
))
b_bot = b.intersect((
    top_box
    .translate((0, printer_margin / 2, 0))
))

def calc_peg(margin=0.0):
    peg_h = base_thickness * 1.4 + 2 * margin
    peg = (
        cq.Workplane("XZ")
        .circle(base_thickness / 3 + margin)
        .extrude(peg_h)
        .chamfer(base_thickness / 8)
        .translate((0, peg_h/2, 0))
        .translate((0, 0, base_thickness/2))
    )
    if margin == 0:
        peg = peg.chamfer(base_thickness / 16)
    return peg

peg = calc_peg()

max_i = 3
# for i in range(0, max_i):
for i in [0, 2]:
    peg = (
        calc_peg()
        .translate((base_x * (i / (max_i + 2)), 0, 0))
        .mirror(mirrorPlane='YZ', union=True)
    )
    peg_cut = (
        calc_peg(margin=printer_margin * 2)
        .translate((base_x * (i / (max_i + 2)), 0, 0))
        .mirror(mirrorPlane='YZ', union=True)
    )
    if i % 2 == 0:
        b_bot = b_bot.union(peg)
        b_top = b_top.cut(peg_cut)
    else:
        b_top = b_top.union(peg)
        b_bot = b_bot.cut(peg_cut)


# show_object(shelf_cut, name=f'shelf_cut')
# show_object(top_box, name=f'top_box')
# show_object(peg, name=f'peg')

show_object(b_top, name=f'foam_strip_cutting_guide_top')
show_object(b_bot, name=f'foam_strip_cutting_guide_bot')

cq.exporters.export(b_top, f"foam_strip_cutting_guide_top.step")
cq.exporters.export(b_bot, f"foam_strip_cutting_guide_bot.step")
