import cadquery as cq

l = 100
h = 10
max_i = 20


b = (
    cq.Workplane("XY")
        .rect(100, h)
        .extrude(3)
        .translate((0, -h/2, 0))
        .translate((0, -h/2, 0))
        .mirror(mirrorPlane='XZ', union=True)
)

for i in range(1, max_i):
    t = i * 0.05

    x = -l / 2 + (i / max_i) * l

    b = b.cut((
        cq.Workplane("XY")
            .text(txt=str(round(t, 2)), fontsize=2.0, distance=4, cut=True, clean=True)
            .translate((0, -h, 0))
            .translate((x, 0, 0))
            .translate((0, 2 if i % 2 == 0 else 0, 0))
    ))

    b = b.union((
        cq.Workplane("XY")
            .rect(t, h)
            .extrude(3)
            .translate((x, 0, 0))
    ))


show_object(b, name=f'b')

cq.exporters.export(
    b,
    'thin_supports_test_04_nozzle.step'
)