import wg_path
import wg_functions
import math

mylar_um = 12
glue_um = 12
# aluminum_um = 20


def calc_alu_trace_resistance(width_mm, length_mm, thickness_um):
    aluminum_ohm_per_mm2_um = 0.000933 * 1.16 * (30 / thickness_um)

    trace_resistance_ohm = length_mm * (
            1 / width_mm) * aluminum_ohm_per_mm2_um

    return trace_resistance_ohm


def calc_path_resistance(path, thickness_um):
    r = 0

    for segment in path.length_segments:
        start_width = segment.start_width
        end_width = segment.end_width
        length = segment.length

        r += calc_alu_trace_resistance(
            width_mm=wg_functions.middle_value(start_width, end_width),
            length_mm=length,
            thickness_um=thickness_um,
        )

    return r

# print(4*3*calc_alu_trace_resistance(1.66666666, 315, 20))
# print(4*2*calc_alu_trace_resistance(3, 1800, 27)* 0.7)
#
#
# r1 = calc_path_resistance((
#     wg_path.WgPath(width=1)
#         .walk_straight(10)
# ), 20)
#
# r2 = calc_path_resistance((
#     wg_path.WgPath(width=1)
#         .walk_straight(10)
#         .walk_straight(10)
# ), 20)
#
# r3 = calc_path_resistance((
#     wg_path.WgPath(width=1)
#         .walk_straight(10)
#         .walk_straight(10, end_width=2)
#         .walk_straight(10)
# ), 20)
#
# r3_est = (
#     calc_alu_trace_resistance(1, 10, 20) +
#     calc_alu_trace_resistance(1.5, 10, 20) +
#     calc_alu_trace_resistance(2, 10, 20)
# )
#
# r4 = calc_path_resistance((
#     wg_path.WgPath(width=1)
#         .walk_straight(10)
#         .walk_arc(90, 100, end_width=2)
#         .walk_straight(10)
# ), 20)
#
# r4_est = (
#     calc_alu_trace_resistance(1, 10, 20) +
#     calc_alu_trace_resistance(1.5, 100 * 0.5 * math.pi, 20) +
#     calc_alu_trace_resistance(2, 10, 20)
# )
#
# print(f"resistance of traces in r1: {r1}")
# print(f"resistance of traces in r2: {r2}")
# print(f"resistance of traces in r1 / r2: {r1 / r2}")
# print(f"resistance of traces in r3: {r3}")
# print(f"resistance of traces in r3_est: {r3_est}")
# print(f"resistance of traces in r4: {r4}")
# print(f"resistance of traces in r4_est: {r4_est}")

