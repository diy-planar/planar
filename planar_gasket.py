from geometry_2d import P
import cadquery as cq


def calc_gasket_sketch(bot_width, height, t, top_width=None):
    inner_height = height - 2 * t

    r = inner_height / 2

    p_bot_right = P(bot_width / 2, 0)

    p0_center = p_bot_right + P(-r, t)

    p0 = p0_center + P(length=r, angle=0)
    p1 = p0_center + P(length=r, angle=45)
    p2 = p0_center + P(length=r, angle=90)

    if top_width:
        p_top_right_with_t = P(top_width / 2, height)
    else:
        p_top_right_with_t = P(bot_width / 2 - 2 * r, height)

    p_top_right = p_top_right_with_t + P(0, -t)
    p_top_mid = P(0, height)

    if top_width:
        p3 = p2 - P(bot_width / 2 - top_width / 2 - 2 * r, 0)
    else:
        p3 = p2

    p4_center = P(p3.x, p_top_right.y)
    p4 = p4_center + P(length=-r, angle=45)

    s = (
        cq.Sketch()
            .segment((0, 0), p_bot_right.values())

            .segment(p_bot_right.values(), p0.values())

            .arc(p0.values(), p1.values(), p2.values(), "p1")
        # .segment(p0.values(), p1.values())
        # .segment(p1.values(), p2.values())
    )

    if p2 != p3:
        s = (
            s
                .segment(p2.values(), p3.values())
        )

    s = (
        s

            .arc(p3.values(), p4.values(), p_top_right.values(), "p2")
            # .segment(p3.values(), p4.values())
            # .segment(p4.values(), p_top_right.values())

            .segment(p_top_right.values(), p_top_right_with_t.values())

            .segment(p_top_right_with_t.values(), p_top_mid.values())

            .segment(p_top_mid.values(), p_top_right_with_t.invert_x().values())

            .arc(p_top_right.invert_x().values(), p4.invert_x().values(), p3.invert_x().values(), "p2")
    )

    if p2 != p3:
        s = (
            s
                .segment(p3.invert_x().values(), p2.invert_x().values())
        )

    s = (
        s
            .arc(p2.invert_x().values(), p1.invert_x().values(), p0.invert_x().values(), "p1")

            .segment(p0.invert_x().values(), p_bot_right.invert_x().values())

            .segment(p_bot_right.invert_x().values(), (0, 0))

            # .close()
            .solve().assemble()
            .clean()
    )

    return s


# def revolve_gasket_sketch(s, center_at=(0, 0, 0)):
#     sketches = []
#
#     sketches.append(
#         (
#             s
#                 .moved(
#                 cq.Location(
#                     cq.Plane.XY().rotated((-planar_height_degrees / 2, 0, 0)),
#                     cq.Vector(center_at[0], center_at[1] - (cbt_radius - p.x), center_at[2] - p.y)
#                 )
#             )
#         )
#     )
#
#     sketches.append(
#         (
#             s
#                 .moved(
#                 cq.Location(
#                     cq.Plane.XY().rotated((0, 0, 0)),
#                     cq.Vector(center_at[0], center_at[1], center_at[2])
#                 )
#             )
#         )
#     )
#
#     sketches.append(
#         (
#             s
#                 .moved(
#                 cq.Location(
#                     cq.Plane.XY().rotated((planar_height_degrees / 2, 0, 0)),
#                     cq.Vector(center_at[0], center_at[1] -(cbt_radius - p.x), center_at[2] + p.y)
#                 )
#             )
#         )
#     )
#
#
#     bb = (
#         cq.Workplane("XY")
#             .placeSketch(*sketches)
#             .loft()
#             .rotate((0, 0, 0), (0, 0, 1), 90)
#             .rotate((0, 0, 0), (0, 1, 0), 90)
#             .translate((0, x_center, 0))
#     )
#
#     return bb


def calc_gasket_workplane(workplane=None, bot_width=None, height=None, middle_height=None, r=None, t=None, top_width=None, x_offset=0.0, y_offset=0.0):
    b = cq.Workplane("XY")
    if workplane:
        b = workplane

    po = P(x_offset, y_offset)

    p00 = po + P(-bot_width / 2, 0)

    p0 = po + P(-bot_width / 2, t)
    p2 = po + P(-top_width / 2, height)
    p1 = p0.middle(p2)
    if middle_height:
        p1 = P(p1.x, middle_height)

    pn = po + P(0, height)

    step = 0.01
    edge_step = 0

    x_delta = p2.x - p0.x
    y_delta = height - t

    if r is None:
        r = height * 0.5 - t
        r = min(x_delta / 2 - t, y_delta / 2 - t, r)

    x_step = x_delta - 2 * r
    y_step = (y_delta - 2 * r) / 2

    pa0 = P(p0.x, p0.y + y_step)
    pa1 = P(p1.x - x_step / 2, p1.y)

    pa01 = P(pa1.x, pa0.y) + P(length=r, angle=45 + 90)

    pa2 = P(p1.x + x_step / 2, p1.y)
    pa3 = P(p2.x, p2.y - y_step)
    pa23 = P(pa2.x, pa3.y) + P(length=-r, angle=45 + 90)


    return (
        b
            .moveTo(po.x, po.y)
            .lineTo(p00.x, p00.y)
            # .lineTo(-bot_width / 2, t)

            .lineTo(p0.x, p0.y)
            # .lineTo(p0.x, p0.y + step)
            #
            # .lineTo(p1.x - step / 2, p1.y)
            # .lineTo(p1.x + step / 2, p1.y)
            #
            # .lineTo(p2.x, p2.y - step)
            # .lineTo(p2.x, p2.y)

            .lineTo(pa0.x, pa0.y)
            # .lineTo(pa01.x, pa01.y)
            # .lineTo(pa1.x, pa1.y)
            .threePointArc(pa01.values(), pa1.values())

            .lineTo(pa2.x, pa2.y)
            # .lineTo(pa23.x, pa23.y)
            # .lineTo(pa3.x, pa3.y)
            .threePointArc(pa23.values(), pa3.values())

            .lineTo(p2.x, p2.y)

            # .spline([
            #     (p0.x, p0.y),
            #     (p0.x + edge_step, p0.y + step),
            #
            #     (p1.x - step / 2, p1.y),
            #     (p1.x + step / 2, p1.y),
            #
            #     (p2.x - edge_step, p2.y - step),
            #     (p2.x, p2.y),
            # ])

            # .lineTo(-top_width / 2, height)
            .lineTo(pn.x, pn.y)
            # .threePointArc((0, 1), (2, 0))
            .close()
        # .mirrorY()
    )


def calc_gasket_workplane_single(workplane=None, bot_width=None, height=None, middle_height=None, r=None, t=None, top_width=None, shell=None, x_offset=0.0, y_offset=0.0):
    b = cq.Workplane("XY")
    if workplane:
        b = workplane

    po = P(x_offset + bot_width / 2, y_offset)

    # p00 = po + P(-bot_width, 0)

    po_0 = po
    pi_0 = po - P(shell, 0)

    po_1 = po + po_0 + P(0, t)
    pi_1 = po + po_0 + P(0, t) - P(shell, 0)

    po_n = po + P(0, height)
    pi_n = po + P(0, height) - P(shell, 0)

    return (
        b
        .moveTo(po_0.x, po_0.y)

        .lineTo(po_1.x, po_1.y)

        .lineTo(pi_1.x, pi_1.y)
        .lineTo(pi_0.x, pi_0.y)

        .close()
    )


    p0 = po + P(-bot_width, t)
    p2 = po + P(-top_width, height)

    p1 = p0.middle(p2)
    if middle_height:
        p1 = P(p1.x, middle_height)

    pn = po + P(0, height)
    pn_2 = po + P(0, height - t)

    x_delta = p2.x - p0.x
    y_delta = height - t

    if r is None:
        r = height * 0.5 - t
        r = min(x_delta / 2 - t, y_delta / 2 - t, r)

    x_step = x_delta - 2 * r
    y_step = (y_delta - 2 * r) / 2

    pa0 = P(p0.x, p0.y + y_step)
    pa1 = P(p1.x - x_step / 2, p1.y)

    pa01 = P(pa1.x, pa0.y) + P(length=r, angle=45 + 90)
    if shell:
        pa01_n = P(pa1.x, pa0.y) + P(length=r - shell, angle=45 + 90)

    pa2 = P(p1.x + x_step / 2, p1.y)
    pa3 = P(p2.x, p2.y - y_step)
    pa23 = P(pa2.x, pa3.y) + P(length=-r, angle=45 + 90)
    if shell:
        pa23_n = P(pa2.x, pa3.y) + P(length=-(r + shell), angle=45 + 90)

    p00_neg1 = p00 + P(top_width, 0)

    if shell:
        b = b.moveTo(p00_neg1.x, p00_neg1.y)
    else:
        b = b.moveTo(po.x, po.y)

    b = (
        b
            .lineTo(p00.x, p00.y)
            # .lineTo(-bot_width / 2, t)

            .lineTo(p0.x, p0.y)
            # .lineTo(p0.x, p0.y + step)
            #
            # .lineTo(p1.x - step / 2, p1.y)
            # .lineTo(p1.x + step / 2, p1.y)
            #
            # .lineTo(p2.x, p2.y - step)
            # .lineTo(p2.x, p2.y)

            .lineTo(pa0.x, pa0.y)
            # .lineTo(pa01.x, pa01.y)
            # .lineTo(pa1.x, pa1.y)
            .threePointArc(pa01.values(), pa1.values())

            .lineTo(pa2.x, pa2.y)
            # .lineTo(pa23.x, pa23.y)
            # .lineTo(pa3.x, pa3.y)
            .threePointArc(pa23.values(), pa3.values())

            .lineTo(p2.x, p2.y)

            # .spline([
            #     (p0.x, p0.y),
            #     (p0.x + edge_step, p0.y + step),
            #
            #     (p1.x - step / 2, p1.y),
            #     (p1.x + step / 2, p1.y),
            #
            #     (p2.x - edge_step, p2.y - step),
            #     (p2.x, p2.y),
            # ])

            # .lineTo(-top_width / 2, height)
            .lineTo(pn.x, pn.y)

        # .mirrorY()
    )

    if shell:
        b = (
            b
                .lineTo(pn_2.x, pn_2.y)

                .threePointArc((pa23_n.x, pa23_n.y), (pa2.x, pa2.y - shell))
                # .lineTo(pa23_n.x, pa23_n.y)
                # .lineTo(pa2.x, pa2.y - shell)

                .lineTo(pa1.x, pa1.y - shell)

                .threePointArc((pa01_n.x, pa01_n.y), (pa0.x + shell, pa0.y))
                # .lineTo(pa01_n.x, pa01_n.y)
                # .lineTo(pa0.x + shell, pa0.y)
        )

    return b.close()


def gasket_revolve(fn, angle, radius):
    p_half = P(-radius, 0) + P(length=radius, angle=angle / 4)
    p_full = P(-radius, 0) + P(length=radius, angle=angle / 2)

    # print(f"p_half={p_half}")
    # print(f"p_full={p_full}")

    b = (
        cq.Workplane("XY")
    )

    b = fn(b)

    b = (
        b
            .workplane()
            .transformed(offset=cq.Vector(0, p_half.x, p_half.y), rotate=cq.Vector(angle / 4, 0, 0))
    )

    b = fn(b)

    b = (
        b
            .workplane()
            .transformed(offset=cq.Vector(0, p_full.x, p_full.y), rotate=cq.Vector(angle / 2, 0, 0))
    )

    b = fn(b)

    b = (
        b
            .loft(combine=True)
            .mirror(mirrorPlane='XY', union=True)
    )

    return b