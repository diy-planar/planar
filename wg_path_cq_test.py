from geometry_2d import P
import geometry_2d
import wg_path

# w = (
#     wg_path.WgPath(width=2, current_p=P(10, 10))
#         .walk_straight(10)
#         .walk_arc(90, 5)
#         .walk_straight(3)
#         .walk_arc(45, 5)
#         .walk_straight(3)
#         .walk_arc(45, 5)
#         .walk_arc(-90, 5)
#         # .walk_straight(10)
# )

# w = (
#     wg_path.WgPath(width=2)
#         .walk_arc(90, 3)
#         .walk_straight(1)
#         .walk_arc(-90, 5)
#         .walk_straight(10)
#         .walk_arc(90, 5)
#         .walk_straight(2)
# )

# w = (
#     wg_path.WgPath(width=2, angle=0)
#         .walk_arc(-90, 5)
# )

t = 2
r = 2 * geometry_2d.degrees_across_circle(12, 750)
l = 2 * 12.25

r *= 10

w = (
    wg_path.WgPath(width=t, current_p=P(0, 0))
        .walk_straight(l)
        # .walk_arc(r, t)C
        .walk_straight(l)
        .walk_arc(r, t)
        .walk_straight(l)
        .walk_arc(r, t)
        .walk_straight(l)
        .walk_arc(r, t)
        .walk_straight(l)
        .walk_arc(r, t)
        .walk_straight(l, end_width=t*1.5, left_offset=0.25 * t)
        .walk_arc(r, t)
        .walk_straight(l, end_width=t * 2, left_offset=0.25 * t)
        .walk_straight(l, end_width=t * 4, left_offset=1 * t)

    # .walk_straight(10)
)

# import  membrane_functions
#
# print(f"resistance of traces in path: {membrane_functions.calc_path_resistance(w, 20)}")

import cadquery as cq

b = (
    w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
    # w.draw_lines_on_workplane(cq.Workplane("XY"))
        .close()
        # .extrude(0.1)
        .extrude(70)
        .translate((0, t/2, 0))
)

show_object(b, name=f'b')

cq.exporters.export(
    b,
    'test-bend.step'
)