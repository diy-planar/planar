import math

import wg_functions
import wg_path
from geometry_2d import P

magnet_length = 12.5
corrugation_shortening_comp_ratio = 1.2

membrane_length = magnet_length * 3 * 7 * corrugation_shortening_comp_ratio
full_membrane_length = magnet_length * 3 * 7 * 6 * corrugation_shortening_comp_ratio
full_membrane_shading = 0.707
# membrane_length = 50

# assume edges are folded
membrane_length_bot_edge = 12
membrane_length_top_edge = 12
membrane_length_including_edges = membrane_length + membrane_length_bot_edge + membrane_length_top_edge
membrane_shift_offset = (membrane_length_bot_edge - membrane_length_top_edge) / 2
membrane_width = 55

magnet_rows = 5

magnet_width = 5
magnet_to_magnet_gap = 5

magnet_row_ctc = magnet_width + magnet_to_magnet_gap

num_traces_rows = magnet_rows - 1

# trace_gap_width = 1.0
# trace_gap_width = 0.75
# trace_gap_width = 0.5
trace_gap_width = 0.5

fill_gap_width = trace_gap_width
suspension_fill_gap = 0.5

SOLID_FILL = False
LADDER_FILL = False
OUTSIDE_FILL = True
SUSPENSION_FILL = True
SQUIGGLY_FILL = False
SEGMENTED_EDGES = False
EXPORT_DXF = False

# traces_row_width = 3
# num_traces_per_row = 2

# traces_row_width = magnet_to_magnet_gap
# num_traces_per_row = 3

# traces_row_width = magnet_to_magnet_gap
# num_traces_per_row = 3
# num_traces_rows = magnet_rows + 1


# traces_row_width = magnet_to_magnet_gap + 0.2 * magnet_width
# num_traces_per_row = 3
# membrane_width = 36


# traces_row_width = magnet_to_magnet_gap + 0.2 * magnet_width
# num_traces_per_row = 3
# num_traces_rows = magnet_rows + 1
# membrane_width += traces_row_width - magnet_to_magnet_gap
# membrane_length_including_edges -= 5

# traces_row_width = 6
# num_traces_per_row = 4
# driven_trace_factor = 20
# fill_trace_factor = 20

traces_row_width = 3
num_traces_per_row = 2
driven_trace_factor = 1 * 51 # um
fill_trace_factor = 14 # um
fill_gap_width = 1.0


suspension_fill_trace_factor = -30


# Solhaga copy
# traces_row_width = 3
# num_traces_per_row = 2
# membrane_length_including_edges -= 15

fill_suffix = ''
if OUTSIDE_FILL:
    fill_suffix += '_fill'
if SUSPENSION_FILL:
    fill_suffix += '_suspension-fill'

def calc_dxf_filename(name):
    from datetime import date
    import os


    today = str(date.today())

    target_dir = f"{os.getcwd()}\\membrane_traces\\{today}"
    os.makedirs(target_dir, exist_ok=True)

    membrane_traces_dxf = f"{target_dir}/membrane_traces_{name}{fill_suffix}.dxf"
    return membrane_traces_dxf

##########################

driven_trace_factor /= 20
fill_trace_factor /= 20
suspension_fill_trace_factor /= 20


mylar_um = 12
glue_um = 12
aluminum_um = 20


trace_width = (traces_row_width - (num_traces_per_row - 1) * trace_gap_width) / num_traces_per_row

trace_ctc = trace_width + trace_gap_width

suspension_fill_width = (membrane_width - magnet_rows * magnet_width - (magnet_rows - 1) * magnet_to_magnet_gap) / 2
suspension_fill_width = -3

non_driven_width = magnet_row_ctc - traces_row_width

aluminum_ohm_per_mm2_um = 0.000933 * 1.16 * (30 / (aluminum_um * driven_trace_factor))

trace_resistance_ohm = num_traces_rows * num_traces_per_row * membrane_length * (1 / trace_width) * aluminum_ohm_per_mm2_um
estimated_full_shaded_membrane_resistance_ohm = num_traces_rows * num_traces_per_row * full_membrane_length * full_membrane_shading * (1 / trace_width) * aluminum_ohm_per_mm2_um


bot_offset = magnet_row_ctc / 2 + trace_gap_width
if num_traces_per_row == 2:
    bot_offset *= 0.6
elif num_traces_per_row == 3:
    bot_offset -= trace_gap_width * 2
elif num_traces_per_row == 4:
    bot_offset -= trace_gap_width * 1.2

print()
print(f'membrane_length = {membrane_length} mm')
print(f'membrane_length_bot_edge = {membrane_length_bot_edge} mm')
print(f'membrane_length_top_edge = {membrane_length_top_edge} mm')
print(f'membrane_length_including_edges = {membrane_length_including_edges} mm')
print(f'membrane_width = {membrane_width} mm')
print(f'traces_row_width = {traces_row_width} mm')
print(f'trace_gap_width = {trace_gap_width} mm')
print(f'num_traces_per_row = {num_traces_per_row}')
print(f'trace_width = {trace_width} mm')
print(f'non_driven_width = {non_driven_width} mm')
print(f'trace_resistance_ohm = {round(trace_resistance_ohm, 2)} ohm')
print(f'estimated_full_shaded_membrane_resistance_ohm = {round(estimated_full_shaded_membrane_resistance_ohm, 2)} ohm')

um_to_mm = 1/1000

mylar_kg_mm3 = 1400 * math.pow(10, -9)
glue_kg_mm3 = 1180 * math.pow(10, -9)
aluminum_kg_mm3 = 2710 * math.pow(10, -9)

cross_driven_width = num_traces_per_row * num_traces_rows * trace_width
if OUTSIDE_FILL:
    cross_undriven_width = membrane_width - 4 * (traces_row_width + 2 * trace_gap_width)
elif SUSPENSION_FILL and num_traces_rows == 4:
    cross_undriven_width = 2 * suspension_fill_width
else:
    cross_undriven_width = 0

mylar_weight = membrane_width * mylar_um * um_to_mm * mylar_kg_mm3
glue_weight = membrane_width * glue_um * um_to_mm * glue_kg_mm3
aluminum_driven_weight = cross_driven_width * driven_trace_factor * aluminum_um * um_to_mm * aluminum_kg_mm3
aluminum_undriven_weight = cross_undriven_width * fill_trace_factor * aluminum_um * um_to_mm * aluminum_kg_mm3

total_membrane_weight = mylar_weight + glue_weight + aluminum_driven_weight + aluminum_undriven_weight

membrane_driven_weight_percent = aluminum_driven_weight / total_membrane_weight

kg_to_mg = 1000 * 1000

print()
print(f"aluminum_driven_thickness = {round(aluminum_um * driven_trace_factor, 2)}")
print(f"aluminum_undriven_thickness = {round(aluminum_um * fill_trace_factor, 2)}")

print(f'mylar_weight = {round(mylar_weight * kg_to_mg, 4)} mg')
print(f'glue_weight = {round(glue_weight * kg_to_mg, 4)} mg')
print(f'aluminum_driven_weight = {round(aluminum_driven_weight * kg_to_mg, 4)} mg')
print(f'aluminum_undriven_weight = {round(aluminum_undriven_weight * kg_to_mg, 4)} mg')
print(f'total_membrane_weight = {round(total_membrane_weight * kg_to_mg, 4)} mg')
print(f'membrane_driven_weight_percent = {round(membrane_driven_weight_percent * 100, 1)} %')

t = 0.1

driven_trace_thickness = t * driven_trace_factor
fill_trace_thickness = t * fill_trace_factor
suspension_fill_trace_thickness = t * suspension_fill_trace_factor

import cadquery as cq

membrane = (
    cq.Workplane("XY")
        .rect(membrane_length_including_edges, membrane_width)
        .extrude(t)
        .translate((membrane_shift_offset, 0, 0))
)

magnet_row = (
    cq.Workplane("XY")
        .rect(membrane_length, magnet_width)
        .extrude(3)
        .translate((0, 0, -1.5))
)

magnets = []
for i in range(-math.floor(magnet_rows / 2), math.ceil(magnet_rows / 2)):
    magnets.append(
        magnet_row.translate((0, i * magnet_row_ctc, 0))
    )

magnets = wg_functions.union_shapes_list(magnets)


def calc_traces(trace_width_margin=0.0, add_neg_con_trace_stub=True, add_pos_con_trace_stub=True):
    trace_width_local = trace_width + trace_width_margin

    single_trace = (
        cq.Workplane("XY")
            .rect(membrane_length, trace_width_local)
            .extrude(driven_trace_thickness)
    )

    traces_row = []
    for i in range(0, num_traces_per_row):
        traces_row.append(
            single_trace.translate((0, i * trace_ctc - (traces_row_width - trace_width) / 2, 0))
        )
    traces_row = wg_functions.union_shapes_list(traces_row)

    traces_rows = []
    for i in range(-math.floor(num_traces_rows / 2), math.floor(num_traces_rows / 2)):
        traces_rows.append(
            traces_row.translate((0, (i) * magnet_row_ctc + magnet_width, 0))
        )
    traces_rows = wg_functions.union_shapes_list(traces_rows)

    def calc_traces_end_cap_w(i, trace_end_r):
        return (
            wg_path.WgPath(width=trace_width_local, current_p=P(0, 0))
                .walk_arc(180, trace_end_r)
        )

    def calc_traces_end_cap(i_range=range(0, num_traces_per_row), w_fn=calc_traces_end_cap_w, inner_r=None):
        traces_end_cap = []

        for i in i_range:
            if inner_r is None:
                inner_r = (non_driven_width + trace_width) / 2
            trace_end_r = inner_r + i * trace_ctc

            # w = (
            #     wg_path.WgPath(width=trace_width, current_p=P(0, 0))
            #         .walk_arc(arc_angle, trace_end_r)
            # )

            w = w_fn(i, trace_end_r)

            trace_end = (
                w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
                    .close()
                    .extrude(driven_trace_thickness)
                    .translate((0, -trace_end_r, 0))
            )

            traces_end_cap.append(trace_end)

        return wg_functions.union_shapes_list(traces_end_cap)

    traces_end_cap = calc_traces_end_cap()

    traces_end_cap_width = traces_row_width * 2 + non_driven_width
    w = num_traces_rows * trace_gap_width + (num_traces_rows - 1) * magnet_width

    traces_end_caps = []
    i_max = math.floor(num_traces_rows / 2)
    for i in range(0, i_max):
        traces_end_caps.append((
            traces_end_cap
                .rotate((0, 0, 0), (0, 0, 1), 180)
                .translate((-membrane_length / 2, (2 * i - i_max / 2) * magnet_row_ctc, 0))
        ))

    traces_end_caps.append((
        calc_traces_end_cap(i_range=[0])
        .translate((membrane_length / 2, 0, 0))
    ))

    if num_traces_per_row > 1:
        traces_end_caps.append((
            calc_traces_end_cap(i_range=range(1, num_traces_per_row), inner_r=(non_driven_width + trace_width) / 2 - trace_ctc / 2)
            .translate((membrane_length / 2, 0, 0))
            .translate((0, -magnet_width * 2 - trace_ctc / 2, 0))
            .mirror(mirrorPlane='XZ', union=True)
        ))

    traces_end_caps = wg_functions.union_shapes_list(traces_end_caps)

    #################

    trace_end_r_min = (non_driven_width + trace_width) / 2
    offset = trace_end_r_min + bot_offset

    def calc_bottom_traces_end_cap_w_1(i, trace_end_r):
        trace_end_big_r = offset + i * trace_ctc
        return (
            wg_path.WgPath(width=trace_width_local, current_p=P(-bot_offset, 0))

                .walk_arc(90, trace_end_big_r)
                .walk_straight(offset)
        )

    def calc_bottom_traces_end_cap_w_2(i, trace_end_r):
        return (
            wg_path.WgPath(width=trace_width_local, current_p=P(-bot_offset, 0))

                .walk_straight(bot_offset)
                .walk_arc(90, trace_end_r)
                .walk_straight(magnet_row_ctc * 2 - trace_ctc)

                .walk_arc(90, trace_end_r)
                .walk_straight(bot_offset)
        )

    bottom_trace_curves = []

    cut_w = 1000

    if num_traces_per_row > 1:

        bottom_trace_curves.append((
            calc_traces_end_cap(i_range=range(1, num_traces_per_row), w_fn=calc_bottom_traces_end_cap_w_1)
                .translate((membrane_length / 2 + bot_offset, -magnet_row_ctc + trace_ctc, 0))
                .cut((
                cq.Workplane("XY")
                    .rect(cut_w, cut_w)
                    .extrude(driven_trace_thickness)
                    .translate((0, cut_w / 2, 0))
            ))
        ))

        bottom_trace_curves.append((
            calc_traces_end_cap(i_range=range(1, num_traces_per_row), w_fn=calc_bottom_traces_end_cap_w_2)
                .translate((membrane_length / 2 + bot_offset, -magnet_row_ctc + trace_ctc, 0))
                .cut((
                cq.Workplane("XY")
                    .rect(cut_w, cut_w)
                    .extrude(driven_trace_thickness)
                    .translate((0, -cut_w / 2, 0))
            ))
        ))

        bottom_trace_curves = wg_functions.union_shapes_list(bottom_trace_curves)

    single_trace.translate((0, i * trace_ctc - (traces_row_width - trace_width) / 2, 0))

    i = 0
    neg_trace_len = 2 * trace_gap_width
    pos_trace_len = 1 * magnet_row_ctc

    if num_traces_per_row == 1:
        neg_trace_len = 13
        pos_trace_len = 13

    # neg_trace_con = (
    #     cq.Workplane("XY")
    #         .rect(neg_trace_len, trace_width_local)
    #         .extrude(driven_trace_thickness)
    #         .translate((0, i * trace_ctc - (traces_row_width - trace_width) / 2, 0))
    #         .translate((0, (i) * magnet_row_ctc + magnet_width, 0))
    #         .translate((0, magnet_row_ctc, 0))
    #         .translate((membrane_length / 2, 0, 0))
    # )
    # pos_trace_con = (
    #     cq.Workplane("XY")
    #         .rect(pos_trace_len, trace_width_local)
    #         .extrude(driven_trace_thickness)
    #         .translate((0, i * trace_ctc - (traces_row_width - trace_width) / 2, 0))
    #         .translate((0, (i) * magnet_row_ctc + magnet_width, 0))
    #         .translate((0, -magnet_row_ctc * 2, 0))
    #         .translate((membrane_length / 2, 0, 0))
    # )

    pad_circle_r = (non_driven_width + trace_width) / 2 - fill_gap_width / 2
    trace_pad_half_circle = (
        cq.Workplane("XY")
        .circle(pad_circle_r)
        .extrude(driven_trace_thickness)
        .cut((
            cq.Workplane("XY")
            .rect(2 * pad_circle_r, 2 * pad_circle_r)
            .extrude(driven_trace_thickness)
            .translate((-pad_circle_r - trace_width_margin / 2, 0, 0))
        ))
    )

    if trace_width_margin > 0:
        trace_pad_half_circle = trace_pad_half_circle.union((
            cq.Workplane("XY")
            .rect(trace_width_margin / 2, 2 * pad_circle_r)
            .extrude(driven_trace_thickness)
            .translate((-trace_width_margin / 4, 0, 0))
        ))
    else:
        top_half_fillet_cut_negative = (
            cq.Workplane("XY")
            .rect(2 * pad_circle_r, 2 * pad_circle_r)
            .extrude(driven_trace_thickness)
            .translate((0, -pad_circle_r, 0))
        )
        bot_half_fillet_cut_negative = (
            cq.Workplane("XY")
            .rect(2 * pad_circle_r, 2 * pad_circle_r)
            .extrude(driven_trace_thickness)
            .translate((0, pad_circle_r, 0))
        )
        trace_pad_half_circle = (
            trace_pad_half_circle.cut(bot_half_fillet_cut_negative)
            .union(
                trace_pad_half_circle
                .edges("|Z")
                .fillet(magnet_width * 0.15)
                .cut(top_half_fillet_cut_negative)
            )
        )

    trace_pad = (
        trace_pad_half_circle
        .translate((membrane_length / 2, 0, 0))
        .translate((0, -magnet_row_ctc - trace_ctc / 2, 0))
        .translate((0, -(fill_gap_width - trace_gap_width) / 2, 0))
        .mirror(mirrorPlane='XZ', union=True)
    )

    inner_curve_r = trace_width_local
    w = (
        wg_path.WgPath(width=trace_width_local, current_p=P(0, 0))
        .walk_arc(90, inner_curve_r)
    )

    trace_pad_inner_curve = (
            w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
            .close()
            .extrude(driven_trace_thickness)
            .translate((0, -magnet_row_ctc * 1, 0))
            .translate((0,  - (non_driven_width + trace_width) / 2, 0))
            .translate((membrane_length / 2, 0, 0))
            .translate((-inner_curve_r + trace_width_local / 2, 0, 0))
            .mirror(mirrorPlane='XZ', union=True)
    )

    traces = (
        traces_rows
            .union(traces_end_caps)
            # .union(bottom_trace_curves)
    )

    if num_traces_per_row > 1:
        traces = (
            traces
                .union(trace_pad)
                .union(trace_pad_inner_curve)
        )

    if num_traces_per_row == 1:
        pad_trace_straight = (
            cq.Workplane("XY")
            .rect(22, trace_width_local)
            .extrude(driven_trace_thickness)
            .translate((0, + magnet_width, 0))
            .translate((0, magnet_row_ctc, 0))
            .translate((membrane_length / 2, 0, 0))
        )

        w = (
            wg_path.WgPath(width=trace_width_local, current_p=P(0, 0))
            .walk_straight(8.5)
            .walk_arc(90, (magnet_row_ctc - traces_row_width))
            .walk_straight(4)
        )

        pad_trace_arc = (
            w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
            .close()
            .extrude(driven_trace_thickness)
            .translate((0, - magnet_width, 0))
            .translate((0, -magnet_row_ctc, 0))
            .translate((membrane_length / 2, 0, 0))
        )

        traces = traces.union(pad_trace_straight)
        traces = traces.union(pad_trace_arc)
        r = trace_width * 1.75
        pad = (
            cq.Workplane("XY")
            .circle(r + trace_width_margin / 2)
            .extrude(driven_trace_thickness)
        )
        if trace_width_margin == 0:
            pad = pad.cut(
                cq.Workplane("XY")
                .circle(r - traces_row_width * 0.5)
                .extrude(driven_trace_thickness)
            )
            pad = pad.union(
                cq.Workplane("XY")
                .circle(r - traces_row_width * 0.5 - trace_gap_width * 2)
                .extrude(driven_trace_thickness)
            )
            pad = pad.union(
                cq.Workplane("XY")
                .rect(r * 1.75, trace_width_local / 3)
                .extrude(driven_trace_thickness)
                .rotate((0, 0, 0), (0, 0, 1), 45)
                .mirror(mirrorPlane='YZ', union=True)
            )
        pad = (
            pad
            .translate((-r + membrane_length_bot_edge - 4 * fill_gap_width, 0, 0))
            .translate((membrane_length / 2, 0, 0))
        )
        traces = traces.union((
            pad
            .translate((0, - (traces_row_width - trace_width) / 2, 0))
            .translate((0, + magnet_width, 0))
            .translate((0, magnet_row_ctc, 0))
            .union(pad)
            # .mirror(mirrorPlane='XZ', union=True)
        ))
    elif num_traces_per_row > 1:
        pass

    # if bottom_trace_curves:
    #     traces = (
    #         traces
    #             # .union(traces_end_caps)
    #             .union(bottom_trace_curves)
    #     )

    # if add_neg_con_trace_stub:
    #     traces = traces.union(neg_trace_con)
    #
    # if add_pos_con_trace_stub:
    #     traces = traces.union(pos_trace_con)

    return traces


traces = calc_traces()
traces_cut_margin = calc_traces(trace_width_margin=2*fill_gap_width)
traces_cut_margin_without_stub = calc_traces(trace_width_margin=2*fill_gap_width, add_neg_con_trace_stub=False, add_pos_con_trace_stub=False)


# add squiggles

squiggly_fill = None

if SQUIGGLY_FILL:
    # tw = 1.1375
    # r = 8.5

    squiggly_cover_width = 5 + 2 - 0 * trace_gap_width

    tw = 0.675
    r = 1.25
    c = 1.5

    x = math.sqrt((c*c) / 2)

    r_x = 2 * x + math.sqrt(2*r*r)

    # tw = 0.8
    # r = 9

    w = (
        wg_path.WgPath(width=tw, current_p=P(-membrane_length / 2 - 20, 0), angle=45)
        # .walk_arc(-90, r)
        # .walk_arc(90, r)
        # .walk_straight(1000)
    )
    for i in range(0, 100):
        sign = (-1 if i % 2 == 0 else 1)
        w = (
            w
            .walk_straight(c if i == 0 else c * 2)
            .walk_arc(sign * 90, r)
        )

    w = (
        w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"))
        .close()
        # .extrude(t)
        # we will route both on front and back, so double for accurate volume
        .extrude(fill_trace_thickness)
    )

    squiggly_row = (
        w.translate((0, squiggly_cover_width / 4 - trace_gap_width, 0))
        .union(w.translate((0, -squiggly_cover_width / 4 + trace_gap_width, 0)))
    )

    # r_x = 12.7
    # r_x = 12.0
    # r_x = 6.85
    squiggly = (
        squiggly_row
        # .union((
        #     squiggly_row.translate((r_x * (1 / 1), 0, 0))
        # ))
        # .union((
        #     w.translate((r_x * (2 / 3), 0, 0))
        # ))
        # .union((
        #     w.translate((r_x * (4 / 3), 0, 0))
        # ))
        # .mirror(mirrorPlane='XZ', union=True)
        # .intersect(
        #     cq.Workplane("XY")
        #     .rect(membrane_length, membrane_width)
        #     .extrude(t)
        # )
        .intersect(
            cq.Workplane("XY")
            .rect(membrane_length_including_edges, membrane_width)
            .extrude(driven_trace_thickness + fill_trace_thickness)
            .translate((membrane_shift_offset, 0, 0))
        )
        # .translate((0, traces_width / 2 - trace_width / 2, 0))
        # .translate((x - transition_length / 2, 0, 0))
    )

    # traces = traces.union(squiggly)
    squiggly_fill = squiggly
    squiggly_fill = squiggly_fill.union(squiggly.translate((0, 10, 0)))
    squiggly_fill = squiggly_fill.union(squiggly.translate((0, 20, 0)))
    squiggly_fill = squiggly_fill.union(squiggly.translate((0, -10, 0)))
    squiggly_fill = squiggly_fill.union(squiggly.translate((0, -20, 0)))

#


def calc_neg_con_trace(width, margin=0.0):
    w_factor = 0
    if num_traces_per_row == 3:
        w_factor = 0.1

    w = (
        wg_path.WgPath(width=trace_width + margin * 2)
            .walk_arc(-60, 2)
            .walk_straight(2)
            .walk_arc(30, 2)
            .walk_straight(1.5)
            .walk_arc(-10, 10)
            .walk_arc(90, 0.75)
            .walk_arc(30, 0.75)
            .walk_straight(6.5)
    )

    r = (
        w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"), walked_points=w.left_walked_points)
            .close()
            .extrude(t)

            .translate((0, 0 * trace_ctc - (traces_row_width - trace_width) / 2, 0))
            .translate((0, 0 * magnet_row_ctc + magnet_width, 0))
            .translate((0, magnet_row_ctc, 0))
            .translate((membrane_length / 2, 0, 0))
    )

    l = (
        cq.Workplane("XY")
            .moveTo(-margin, -trace_width/2)
            .lineTo(0, width / 5 + margin)
            .lineTo(width * 0.3 + margin, width / 5 + margin)
            .lineTo(width * (0.5 + w_factor) + margin, width / 8 + margin)
            .lineTo(width * (0.7 + w_factor) + margin, 0)
            .lineTo(width * (0.8 + w_factor) + margin, -width * 0.25 - margin)
            .lineTo(width * (0.8 + w_factor) + margin, -width * 1.5 - margin)
            .lineTo(width * 0.75 - margin, -width * 1.5 - margin)
            .lineTo(trace_gap_width - margin, -trace_width/2)
            .close()
            # .rect(width * 0.75 + margin, width * 1.2 + margin)
            .extrude(t)
            # .translate((trace_gap_width / 2, width * 0.1, 0))
            # .rotate((0, 0, 0), (0, 0, 1), 45)
            # .translate((membrane_length / 2, 0, 0))
            # .translate((magnet_row_ctc / 2, magnet_row_ctc * 0.8, 0))
            # .translate((trace_gap_width, 0, 0))
            # .translate((trace_gap_width, 0, 0))

            .translate((0, 0 * trace_ctc - (traces_row_width - trace_width) / 2, 0))
            .translate((0, 0 * magnet_row_ctc + magnet_width, 0))
            .translate((0, magnet_row_ctc, 0))
            .translate((membrane_length / 2, 0, 0))
    )

    if margin:
        r = r.union(l)

    b = (
        # l.union(r)
        r
            .cut((
            cq.Workplane("XY")
                .rect(1000, 1000)
                .extrude(t)
                .translate((0, -500, 0))
        ))
    )

    # b = b.fillet(trace_width * 0.1)

    return b


def calc_pos_con_trace(margin=False):
    local_t = t

    w = (
        wg_path.WgPath(width=local_t + (1.9 * trace_gap_width) if margin else 0)
            .walk_arc(-45, 3)
            .walk_straight(2)
            .walk_arc(45, 3)
            .walk_arc(90, 3)
            .walk_straight(2.5)
    )

    if margin:
        w = (
            w
                .walk_straight(4)
                .walk_arc(-25, 5)
        )

    else:
        w = (
            w
                .walk_arc(110, 2)
                .walk_arc(-30, 2)
                .walk_straight(7)
        )

    r = (
        w.draw_lines_and_arc_on_workplane(cq.Workplane("XY"), walked_points=w.left_walked_points)
            .close()
            .extrude(t)

            .translate((0, 0 * trace_ctc - (traces_row_width - trace_width) / 2, 0))
            .translate((0, 0 * magnet_row_ctc + magnet_width, 0))
            .translate((0, -magnet_row_ctc * 2, 0))
            .translate((membrane_length / 2, 0, 0))

            .translate((0, -trace_width/2, 0))
    )

    return (
            r
            .cut((
            cq.Workplane("XY")
                .rect(1000, 1000)
                .extrude(t)
                .translate((0, -500, 0))
                .translate((0, -membrane_width / 2, 0))
        ))
    )


if False and num_traces_per_row > 1:
    neg_con_trace = calc_neg_con_trace(width=magnet_row_ctc, margin=0)
    if num_traces_rows == 4:
        neg_con_trace = (
            neg_con_trace
                .cut((
                cq.Workplane("XY")
                    .rect(magnet_row_ctc * 0.5, trace_gap_width * 2)
                    .extrude(t)
                    .translate((membrane_length / 2 + magnet_row_ctc * (0.65 if num_traces_per_row == 2 else 1), 0, 0))
            ))
        )
    neg_con_trace_margin = calc_neg_con_trace(width=magnet_row_ctc, margin=1.0 * trace_gap_width)
    if num_traces_rows == 4:
        neg_con_trace_margin = (
            neg_con_trace_margin
                .union((
                cq.Workplane("XY")
                    .rect(magnet_row_ctc * 0.5, trace_gap_width * 2)
                    .extrude(t)
                    .translate((membrane_length / 2 + magnet_row_ctc * (0.65 if num_traces_per_row == 2 else 1), 0, 0))
            ))
        )

    neg_con_trace = neg_con_trace.cut(traces_cut_margin_without_stub)

    traces = traces.union(
        neg_con_trace
            .cut(traces_cut_margin_without_stub)
    )
    traces_cut_margin = traces_cut_margin.union(neg_con_trace_margin)

pos_con_trace_width = 0.7
if membrane_width < 40:
    pos_con_trace_width *= 1.5

if num_traces_per_row == 2:
    pos_con_trace_width *= 0.9


pos_con_trace_margin, neg_con_trace_margin, pad_sharp_edge_remover = None, None, None

if False and num_traces_per_row > 1:
    pos_con_trace = calc_pos_con_trace()
    pos_con_trace_margin = calc_pos_con_trace(margin=True)

    pos_con_trace = pos_con_trace.cut(traces_cut_margin_without_stub)

    sharp_remover_width = magnet_row_ctc * 0.087
    if num_traces_rows == 4:
        pad_sharp_edge_remover = (
            cq.Workplane("XY")
                .rect(magnet_row_ctc / 2, magnet_row_ctc * 1.0)
                .extrude(t)
                .translate((magnet_row_ctc / 4, 0, 0))
                .cut((
                    cq.Workplane("XY")
                    .circle(sharp_remover_width)
                    .extrude(t)
                    .union((
                        cq.Workplane("XY")
                        .rect(sharp_remover_width * 1.5, sharp_remover_width * 1.5)
                        .extrude(t)
                        .translate((-sharp_remover_width, 0, 0))
                    ))
                        .translate((1.65 * trace_gap_width, 0, 0))
                        .translate((0, magnet_row_ctc * 1.0 / 2, 0))
                        .mirror(mirrorPlane='XZ', union=True)
                ))
                .translate((membrane_length / 2, 0, 0))
                .translate((magnet_row_ctc / 2, 0, 0))
                .cut(traces_cut_margin)
        )
    else:
        pad_sharp_edge_remover = (
            cq.Workplane("XY")
                .rect(1,1)
                .extrude(t)
                .translate((0, membrane_width, 0))
        )

    traces = (
        traces
            .union(pos_con_trace)
            .cut(pad_sharp_edge_remover)
    )
    traces_cut_margin = traces_cut_margin.union(pos_con_trace_margin)

fill = []

if SOLID_FILL:
    traces_outside_fill = (
        cq.Workplane("XY")
            .rect(membrane_length_including_edges, membrane_width)
            .extrude(fill_trace_thickness)
            .translate((membrane_shift_offset, 0, 0))
            .edges("|Z")
            .fillet(magnet_width * 0.5)
    )
    fill.append(traces_outside_fill)

if LADDER_FILL:
    traces_outside_fill = (
        cq.Workplane("XY")
        .rect(membrane_length_including_edges, membrane_width)
        .extrude(fill_trace_thickness)
        .translate((membrane_shift_offset, 0, 0))
        .edges("|Z")
        .fillet(magnet_width * 0.5)
    )
    w = 1
    gap = w
    for i in range(0, math.floor(membrane_length_including_edges * (1 / (w + gap)) / 2)):
        ladder_cut = (
            cq.Workplane("XY")
            .rect(w, membrane_width - 10)
            .extrude(fill_trace_thickness)
            .edges("|Z")
            .fillet(w * 0.45)
        )
        traces_outside_fill = traces_outside_fill.cut((
            ladder_cut
            .translate((membrane_shift_offset + i * (w + gap), 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
        ))
    fill.append(traces_outside_fill)

if OUTSIDE_FILL:
    traces_outside_fill = (
        cq.Workplane("XY")
            .rect(membrane_length_including_edges, membrane_width)
            .extrude(fill_trace_thickness)
            .translate((membrane_shift_offset, 0, 0))
            .edges("|Z")
            .fillet(magnet_width * 0.5)
            .cut(traces_cut_margin)
            # .edges("|Z")
            # .fillet(magnet_width * 0.125)
            # .cut(pos_con_trace_margin)
            # .cut(neg_con_trace_margin)
            # .cut(pad_sharp_edge_remover)
    )

    if pos_con_trace_margin:
        traces_outside_fill = traces_outside_fill.cut(pos_con_trace_margin)
    if neg_con_trace_margin:
        traces_outside_fill = traces_outside_fill.cut(neg_con_trace_margin)
    if pad_sharp_edge_remover:
        traces_outside_fill = traces_outside_fill.cut(pad_sharp_edge_remover)

    fill.append(traces_outside_fill)

if SUSPENSION_FILL and num_traces_rows == 4:
    a = (
        cq.Workplane("XY")
        .rect(membrane_length_including_edges + 2 * suspension_fill_gap, membrane_width + 2 * suspension_fill_gap)
        .extrude(suspension_fill_trace_thickness)
    )
    b = (
        cq.Workplane("XY")
            .rect(membrane_length_including_edges  - 2 * suspension_fill_width, membrane_width - 2 * suspension_fill_width)
            .extrude(suspension_fill_trace_thickness)
            .edges("|Z")
            .fillet(magnet_width * 0.75)
    )
    if suspension_fill_width > 0:
        traces_outside_fill = a.cut(b)
    else:
        traces_outside_fill = b.cut(a)
    traces_outside_fill = (
            traces_outside_fill
            .translate((membrane_shift_offset, 0, 0))
            .cut(traces_cut_margin.mirror(mirrorPlane='XY', union=True))
            # .cut(pos_con_trace_margin)
            # .cut(neg_con_trace_margin)
            # .cut(pad_sharp_edge_remover)
            # .edges("|Z")
            # .fillet(magnet_width * 0.5)
    )

    if pos_con_trace_margin:
        traces_outside_fill = traces_outside_fill.cut(pos_con_trace_margin)
    if neg_con_trace_margin:
        traces_outside_fill = traces_outside_fill.cut(neg_con_trace_margin)
    if pad_sharp_edge_remover:
        traces_outside_fill = traces_outside_fill.cut(pad_sharp_edge_remover)

    traces_outside_fill = (
        traces_outside_fill
        .edges("|Z")
        .fillet(magnet_width * 0.5)
    )

    fill.append(traces_outside_fill)

if SQUIGGLY_FILL:

    squiggly_fill = squiggly_fill.cut(traces_cut_margin)

    if SUSPENSION_FILL:
        squiggly_fill = squiggly_fill.intersect((
            cq.Workplane("XY")
                .rect(membrane_length_including_edges  - 2 * suspension_fill_width - 2 * trace_gap_width, membrane_width - 2 * suspension_fill_width - 2 * trace_gap_width)
                .extrude(fill_trace_thickness)
                .edges("|Z")
                .fillet(magnet_width * 0.75)
                .translate((membrane_shift_offset, 0, 0))
        ))

    fill.append(traces_outside_fill)

if SEGMENTED_EDGES:
    traces = traces.cut((
        cq.Workplane("XY")
            .rect(trace_gap_width / 2, membrane_width)
            .extrude(t)
            .translate((membrane_length / 2 - magnet_row_ctc / 4, 0, 0))
            .mirror(mirrorPlane='YZ', union=True)
    ))

fill = wg_functions.union_shapes_list(fill)

volume_intersect = (
    cq.Workplane("XY")
    .rect(membrane_length, membrane_width)
    .extrude(driven_trace_thickness * 2)
    .translate((0, 0, -driven_trace_thickness))
)

traces_vol = traces.intersect(volume_intersect).val().Volume()
print(f"traces_vol = {traces_vol}")
fill_vol = fill.intersect(volume_intersect).val().Volume()
print(f"fill_vol = {fill_vol}")
traces_and_fill_vol = traces_vol + fill_vol
print(f"traces_and_fill_vol = {traces_and_fill_vol}")
traces_driven_undriven_vol_ratio = traces_vol / traces_and_fill_vol
print(f"traces_driven_undriven_vol_ratio = {round(traces_driven_undriven_vol_ratio * 100, 1)} %")

membrane = membrane.cut(magnets)
# membrane = membrane.cut(traces)
# magnets = magnets.cut(traces)


show_object(magnets, name='magnets', options={"color": (40, 40, 40), "alpha": 0.9})
# show_object(membrane, name='membrane', options={"color": (255, 255, 255), "alpha": 0.9})

show_object(traces, name='traces', options={"alpha": 0.75})
show_object(fill, name='fill', options={"alpha": 0.75})
# show_object(traces_cut_margin, name='traces_cut_margin')
# show_object(neg_con_trace, name='neg_con_trace')
# show_object(neg_con_trace_margin, name='neg_con_trace_margin')
# show_object(pos_con_trace, name='pos_con_trace')
# show_object(pad_sharp_edge_remover, name='pad_sharp_edge_remover')
# show_object(traces_row, name='traces_row')
# show_object(traces_rows, name='traces_rows')

# show_object(traces_end_cap, name='traces_end')
# show_object(traces_end_caps, name='traces_end_caps')
# show_object(bottom_trace_curves, name='bottom_trace_curves')

if EXPORT_DXF:
    cq.exporters.export(
        traces.faces("-Z").section(),
        calc_dxf_filename('traces')
    )
    cq.exporters.export(
        fill.faces("-Z").section(),
        calc_dxf_filename('fill')
    )
    cq.exporters.export(
        traces.union(fill).faces("-Z").section(),
        calc_dxf_filename('traces-with-fill')
    )
    # cq.exporters.export(
    #     traces_cut_margin.union(fill).faces("-Z").section(),
    #     calc_dxf_filename('traces-cut-margin-with-fill')
    # )
