import unittest

import wg_path
from geometry_2d import P


class TestWgPath(unittest.TestCase):

    def test_straight(self):
        w = wg_path.WgPath(width=2)
        w = w.walk_straight(length=10)

        self.assertEqual(P(10.0, 0.0).round(), w.current_p.round())
        self.assertEqual([P(0.0, 1.0), P(10.0, 1.0)], round_walks(w.right_walked_points))
        self.assertEqual([P(10.0, -1.0), P(0.0, -1.0)], round_walks(w.left_walked_points))

    def test_straight_twice(self):
        w = wg_path.WgPath(width=2)
        w = w.walk_straight(length=10)
        w = w.walk_straight(length=15)

        self.assertEqual(P(25.0, 0.0).round(), w.current_p.round())
        self.assertEqual([P(0.0, 1.0), P(10.0, 1.0), P(25.0, 1.0)], round_walks(w.right_walked_points))
        self.assertEqual([P(25.0, -1.0), P(10.0, -1.0), P(0.0, -1.0)], round_walks(w.left_walked_points))

    def test_right_arc(self):
        w = wg_path.WgPath(width=2)
        w = w.walk_arc(relative_angle=90, radius=5)

        p_right = P(0, 5) + P(length=4, angle=-45)
        p_left = P(0, 5) + P(length=6, angle=-45)

        self.assertEqual(P(5.0, 5.0).round(), w.current_p.round())
        self.assertEqual(round_walks([P(0.0, 1.0), (p_right, P(4.0, 5.0))], decimals=3), round_walks(w.right_walked_points, decimals=3))
        self.assertEqual(round_walks([(P(6.0, 5.0), p_left), P(0.0, -1.0)], decimals=3), round_walks(w.left_walked_points, decimals=3))

    def test_right_arc_then_straight(self):
        w = wg_path.WgPath(width=2)
        w = w.walk_arc(relative_angle=90, radius=5)
        w = w.walk_straight(10)

        p_right = P(0, 5) + P(length=4, angle=-45)
        p_left = P(0, 5) + P(length=6, angle=-45)

        self.assertEqual(P(5.0, 15.0).round(), w.current_p.round())
        self.assertEqual(round_walks([P(0.0, 1.0), (p_right, P(4.0, 5.0)), P(4.0, 15.0)], decimals=3), round_walks(w.right_walked_points, decimals=3))
        self.assertEqual(round_walks([P(6.0, 15.0), (P(6.0, 5.0), p_left), P(0.0, -1.0)], decimals=3), round_walks(w.left_walked_points, decimals=3))

    def test_left_arc(self):
        w = wg_path.WgPath(width=2)
        w = w.walk_arc(relative_angle=-90, radius=5)

        p_right = P(0, -5) + P(length=6, angle=45)
        p_left = P(0, -5) + P(length=4, angle=45)

        self.assertEqual(P(5.0, -5.0).round(), w.current_p.round())
        self.assertEqual(round_walks([P(0.0, 1.0), (p_right, P(6.0, -5.0))], decimals=3), round_walks(w.right_walked_points, decimals=3))
        self.assertEqual(round_walks([(P(4.0, -5.0), p_left), P(0.0, -1.0)], decimals=3), round_walks(w.left_walked_points, decimals=3))

    def test_left_arc_2(self):
        w = wg_path.WgPath(width=2, angle=90)
        w = w.walk_arc(relative_angle=-90, radius=5)

        p_right = P(5, 0) + P(length=6, angle=135)
        p_left = P(5, 0) + P(length=4, angle=135)

        self.assertEqual(P(5.0, 5.0).round(), w.current_p.round())
        self.assertEqual(round_walks([P(-1.0, 0.0), (p_right, P(5.0, 6.0))], decimals=3), round_walks(w.right_walked_points, decimals=3))
        self.assertEqual(round_walks([(P(5.0, 4.0), p_left), P(1.0, 0.0)], decimals=3), round_walks(w.left_walked_points, decimals=3))


    def test_right_arc_then_straight_then_left_arc(self):
        w = wg_path.WgPath(width=2)
        w = w.walk_arc(relative_angle=90, radius=5)
        w = w.walk_straight(10)
        w = w.walk_arc(relative_angle=-90, radius=5)

        p_right = P(0, 5) + P(length=4, angle=-45)
        p_left = P(0, 5) + P(length=6, angle=-45)

        p_right_2 = P(10, 15) + P(length=6, angle=135)
        p_left_2 = P(10, 15) + P(length=4, angle=135)

        self.assertEqual(P(10.0, 20.0).round(), w.current_p.round())
        self.assertEqual(round_walks([P(0.0, 1.0), (p_right, P(4.0, 5.0)), P(4.0, 15.0), (p_right_2, P(10.0, 21.0))], decimals=3), round_walks(w.right_walked_points, decimals=3))
        self.assertEqual(round_walks([(P(10.0, 19.0), p_left_2), P(6.0, 15.0), (P(6.0, 5.0), p_left), P(0.0, -1.0)], decimals=3), round_walks(w.left_walked_points, decimals=3))


    def test_round_p(self):
        p = P(0.12345, 0.12345)

        self.assertEqual(P(round(p.x, 3), round(p.y, 3)), p.round(3))


def round_walks(walk, decimals=0):
    out = []
    for p in walk:
        if type(p) is tuple:
            a, b = p

            out += [
                a.round(decimals=decimals),
                b.round(decimals=decimals)
            ]
        else:
            out += [
                p.round(decimals=decimals)
            ]
    return out


if __name__ == '__main__':
    unittest.main()
