import math

arc_degrees = 70


def calc_shading_dec(degree):
    if degree < arc_degrees:
        return math.cos(math.radians(degree * (90 / arc_degrees)))
    else:
        return 0


def dec_to_db(dec):
    if dec > 0:
        return 20 * math.log10(dec)
    else:
        return 0


def db_to_dec(db):
    return 10 ** (db / 20)


def get_degree_with_shading(dec):
    degrees = math.degrees(math.acos(dec))
    return degrees / (90 / arc_degrees)


def debug_calc(degree, debug_print=True):
    dec = calc_shading_dec(degree)
    db = dec_to_db(dec)
    if debug_print:
        print()
        print(f"calc_shading_dec({degree})={dec}")
        print(f"dec_to_db(calc_shading_dec({degree}))={db}")
    dec_from_db = db_to_dec(db)
    degree_from_dec = get_degree_with_shading(dec_from_db)

    if round(dec, 10) != round(dec_from_db, 10):
        raise Exception(f"Something wrong with calculation! {degree}")

    if round(degree, 10) != round(degree_from_dec, 10):
        raise Exception(f"Something wrong with calculation! {degree}")


print()

for r in range(0, arc_degrees * 10):
    debug_calc(r / 10, debug_print=False)


def calc_power_accumulated_until_degree(degree):
    power = 0
    for r in range(0, arc_degrees * 10):
        if r > degree:
            return power

        dec = calc_shading_dec(r)
        watt = dec * dec
        power += watt

    return power


class Values:
    def __init__(self, watt=None, dec=None, db=None):
        if watt is not None:
            self.watt = watt
            self.dec = math.sqrt(watt)
            self.degree = get_degree_with_shading(self.dec)
            self.db = dec_to_db(self.dec)
        elif dec is not None:
            self.dec = dec
            self.watt = dec * dec
            self.degree = get_degree_with_shading(dec)
            self.db = dec_to_db(dec)
        elif db is not None:
            self.db = db
            self.dec = db_to_dec(db)
            self.degree = get_degree_with_shading(self.dec)
            self.watt = self.dec * self.dec
        else:
            raise Exception("all can not be None!")

        self.power_left_above = 1 - (calc_power_accumulated_until_degree(self.degree) / calc_power_accumulated_until_degree(arc_degrees))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        v = {
            "degree": round(self.degree, 2),
            "db": round(self.db, 2),
            "dec": round(self.dec, 2),
            "watt": round(self.watt, 2),
            "power_left_above": round(self.power_left_above, 3),
        }
        return f"{v}"


# for watt in [
#     # 0.75,
#     # 0.5,
#     # 0.25,
#
#     0.66,
#     0.33,
# ]:
#     print(Values(watt=watt))
#
# # print()
# # print(Values(db=-2))
#
# step_db = 1.5

# print()
# values_shaded_0 = Values(watt=watt)
# values_shaded_1 = Values(db=values_shaded_0.db + step_db)
# print(f"radius parallel shaded = {Values(watt=watt).degree - values_shaded_1.degree} degrees")
# print(values_shaded_1)
#
# power_lost_when_padding_0_15_db = 1 - (db_to_dec(-step_db) * db_to_dec(-step_db))
# print(f"wasted power: {(values_shaded_1.power_left_above - values_shaded_0.power_left_above) * power_lost_when_padding_0_15_db}")
#
# print(Values(db=values_shaded_0.db - step_db))
#
# print()

# for db in [
#     0,
#     -1.5,
#     -3,
#     -4.5,
#     -6,
#     -7.5,
#     -9,
#     -10.5,
#     -12,
# ]:
#     print(Values(db=db))
