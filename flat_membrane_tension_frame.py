import cadquery as cq

import wg_functions

holes = [

]

for i in range(1, 160, 2):
    holes.append((i - 80, -4))
    holes.append((i - 80, 4))

flat_membrane_tension_frame = (
    cq.Workplane("XY")
        .rect(160, 9)
        .rect(158, 7)
        .pushPoints(holes)
        .circle(0.32/2)
        .extrude(0.3)
)

show_object(flat_membrane_tension_frame, name=f'flat_membrane_tension_frame', options={"color": (120, 120, 120)})
cq.exporters.export(wg_functions.scale_cm_to_mm(flat_membrane_tension_frame), f"flat_membrane_tension_frame.step")
