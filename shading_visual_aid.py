import shading_calculator
from geometry_2d import P

shading_fixed = [
    (0, shading_calculator.Values(db=-1.25).degree, 0, -1.25),
    (shading_calculator.Values(db=-1.25).degree, shading_calculator.Values(db=-3).degree, -1.25, -3),
    (shading_calculator.Values(db=-3).degree, shading_calculator.Values(db=-6).degree, -3, -6),
    (shading_calculator.Values(db=-6).degree, shading_calculator.Values(db=-9).degree, -6, -9),
    (shading_calculator.Values(db=-9).degree, shading_calculator.Values(db=-12).degree, -9, -12),
    # (shading_calculator.Values(db=-12), shading_calculator.Values(db=-12).degree, 0.25 / 4),
]

shading_list_calculated = [
    0,
    16,
    29.5,
    40.8,
    50.1,
    56.3,
]

shading_custom = [
    (shading_list_calculated[0], shading_list_calculated[1], 0, -1.25),
    (shading_list_calculated[1], shading_list_calculated[2], -1.25, -3),
    (shading_list_calculated[2], shading_list_calculated[3], -3, -6),
    (shading_list_calculated[3], shading_list_calculated[4], -6, -9),
    (shading_list_calculated[4], shading_list_calculated[5], -9, -12),
    (shading_list_calculated[5], shading_calculator.Values(db=-12).degree, -12, None),
]

# shading_list_calculated = [
#     0,
#     14.3,
#     29.6,
#     40.6,
#     50.1,
#     56.3,
# ]

# 3 dB steps
# shading_list_calculated = [
#     0,
#     16,
#     29.6,
#     40.8,
#     50.5,
#     56.3,
# ]
#
# shading_calculated = [
#     (shading_list_calculated[0], shading_list_calculated[1], 0, -1.25),
#     (shading_list_calculated[1], shading_list_calculated[2], -1.25, -3),
#     (shading_list_calculated[2], shading_list_calculated[3], -3, -6),
#     (shading_list_calculated[3], shading_list_calculated[4], -6, -9),
#     (shading_list_calculated[4], shading_list_calculated[5], -9, -12),
#     (shading_list_calculated[5], shading_calculator.Values(db=-12).degree, -12, None),
# ]


# 12 dB steps
shading_list_calculated = [
    0,
    shading_calculator.Values(db=-1).degree - 6.2,
    shading_calculator.Values(db=-2).degree - 3.8,
    shading_calculator.Values(db=-3).degree - 2.8,
    shading_calculator.Values(db=-4).degree - 2.2,
    shading_calculator.Values(db=-5).degree - 1.8,
    shading_calculator.Values(db=-6).degree - 1.55,
    shading_calculator.Values(db=-7).degree - 1.3,
    shading_calculator.Values(db=-8).degree - 1.15,
    shading_calculator.Values(db=-9).degree - 1.0,
    shading_calculator.Values(db=-10).degree - 0.85,
    shading_calculator.Values(db=-11).degree - 0.75,
    shading_calculator.Values(db=-12).degree - 0.65,
]

shading_calculated = []

for i in range(0, len(shading_list_calculated) - 1):
    shading_calculated.append((shading_list_calculated[i], shading_list_calculated[i+1], -i, -i -1))

shading_calculated.append((shading_list_calculated[-1], shading_calculator.Values(db=-12).degree, -12, None))

x = 0

# calculated shading with 2 dB steps
# shading_list_calculated = [
#     0,
#     16,
#     29.6,
#     38.8,
#     44.4,
#     49.4,
#     53.8,
#     57.3,
# ]
#
# shading_calculated = [
#     (shading_list_calculated[0], shading_list_calculated[1], 0, -1.25),
#     (shading_list_calculated[1], shading_list_calculated[2], -1.25, -3),
#     (shading_list_calculated[2], shading_list_calculated[3], -3, -4.6),
#     (shading_list_calculated[2], shading_list_calculated[4], -4.6, -6),
#     (shading_list_calculated[3], shading_list_calculated[5], -6, -8),
#     (shading_list_calculated[3], shading_list_calculated[6], -8, -10),
#     (shading_list_calculated[4], shading_list_calculated[7], -10, -12),
#     (shading_list_calculated[5], shading_calculator.Values(db=-12).degree, -12, None),
# ]

# calculated shading with 1.5 dB steps
# shading_list_calculated = [
#     0,
#     16,
#     29.6,
#     38.4,
#     44.1,
#     48.6,
#     52.3,
#     55.3,
#     57.6,
# ]
#
# shading_calculated = [
#     (shading_list_calculated[0], shading_list_calculated[1], 0, -1.25),
#     (shading_list_calculated[1], shading_list_calculated[2], -1.25, -3),
#     (shading_list_calculated[1], shading_list_calculated[3], -3, -4.5),
#     (shading_list_calculated[2], shading_list_calculated[4], -4.5, -6),
#     (shading_list_calculated[3], shading_list_calculated[5], -6, -7.5),
#     (shading_list_calculated[3], shading_list_calculated[6], -7.5, -9),
#     (shading_list_calculated[4], shading_list_calculated[7], -9, -10.5),
#     (shading_list_calculated[4], shading_list_calculated[8], -10.5, -12),
#     (shading_list_calculated[5], shading_calculator.Values(db=-12).degree, -12, None),
# ]

# shading_list_visual = [
#     0,
#     16,
#     29.5,
#     40.8,
#     50.1,
#     56.3,
# ]
#
# shading_visual = [
#     (shading_list_visual[0], shading_list_visual[1], 0, -1.25),
#     (shading_list_visual[1], shading_list_visual[2], -1.25, -3),
#     (shading_list_visual[2], shading_list_visual[3], -3, -6),
#     (shading_list_visual[3], shading_list_visual[4], -6, -9),
#     (shading_list_visual[4], shading_list_visual[5], -9, -12),
#     (shading_list_visual[5], shading_calculator.Values(db=-12).degree, -12, None),
# ]

shading_list_visual = [
    0,
    10,
    15,
    40.8,
    50.1,
    56.3,
]

shading_visual = [
    (shading_list_visual[0], shading_list_visual[1], 0, -1.25),
    (shading_list_visual[1], shading_list_visual[2], -1.25, -3),
    (shading_list_visual[2], shading_list_visual[3], -3, -6),
    (shading_list_visual[3], shading_list_visual[4], -6, -9),
    (shading_list_visual[4], shading_list_visual[5], -9, -12),
    (shading_list_visual[5], shading_calculator.Values(db=-12).degree, -12, None),
]

import cadquery as cq
import wg_functions

cbt_radius = 1500.0


def calc_slices(list):
    slices = []
    diffs = {}
    for (r1, r2, shading, end_shading) in list:
        # print(f"r1 = {r1}, r2 = {r2}, shading = {shading}")
        p1 = P(angle=r1, length=cbt_radius)
        p2 = P(angle=r2, length=cbt_radius)
        p_shading = P(angle=shading_calculator.Values(db=shading).degree, length=cbt_radius)


        # diffs[f"{shading}"] = abs()
        s = (
            cq.Workplane("YX")
            .moveTo(0, p1.y)
            .lineTo(p_shading.x, p1.y)
            .lineTo(p_shading.x, p2.y)
            .lineTo(0, p2.y)

            # .lineTo(p1.x, p1.y)
            # .lineTo(p2.x, p2.y)
            # .lineTo(0, 0)
            .close()
            .extrude(1)
        )
        slices.append(s)

    return wg_functions.union_shapes_list(slices)


c = (
    cq.Workplane("XY")
    .circle(cbt_radius)
    .extrude(-1)
    .intersect((
        cq.Workplane("XY")
        .rect(cbt_radius, cbt_radius)
        .extrude(-1)
        .translate((cbt_radius / 2, cbt_radius / 2, 0))
    ))
)

p_shaded_max = P(angle=shading_calculator.Values(db=-12).degree, length=cbt_radius)
cutoff_12_db = (
    cq.Workplane("XY")
    .rect(p_shaded_max.y, cbt_radius)
    .extrude(-1)
    .translate((p_shaded_max.y / 2, cbt_radius / 2, 0))
)

fixed_shaded_circle = calc_slices(shading_fixed)
visual_shaded_circle = calc_slices(shading_visual)
calculated_shaded_circle = calc_slices(shading_calculated)

diff_add = calculated_shaded_circle.cut(c).intersect(cutoff_12_db)
diff_sub = c.cut(calculated_shaded_circle).intersect(cutoff_12_db)

diff_add_vol = diff_add.val().Volume()
diff_sub_vol = diff_sub.val().Volume()
diff_error_vol = diff_add_vol - diff_sub_vol
print(f"diff_add_vol = {diff_add_vol}")
print(f"diff_sub_vol = {diff_sub_vol}")
print(f"diff_error_vol = {diff_error_vol}")

show_object(c, name='c', options={"alpha": 0.5})
show_object(fixed_shaded_circle, name='fixed_shaded_circle', options={"color": (200, 40, 40), "alpha": 0.5})
# show_object(visual_shaded_circle, name='visual_shaded_circle', options={"color": (40, 40, 200), "alpha": 0.5})
show_object(calculated_shaded_circle, name='calculated_shaded_circle', options={"color": (40, 200, 40), "alpha": 0.5})

show_object(diff_add, name='diff_add', options={"color": (30, 150, 30), "alpha": 0.5})
show_object(diff_sub, name='diff_sub', options={"color": (20, 100, 20), "alpha": 0.5})
# show_object(cutoff_12_db, name='cutoff_12_db', options={"color": (20, 100, 20), "alpha": 0.5})
